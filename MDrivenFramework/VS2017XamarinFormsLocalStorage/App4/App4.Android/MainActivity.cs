﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using App1;
using App1.ViewModels;
using Eco.ViewModel.Runtime;
using ProjectTemplate11;

namespace App4.Droid
{
  [Activity(Label = "@string/app_name", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
  public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
  {
    private EcoProject1EcoSpace _es;
    protected override void OnCreate(Bundle bundle)
    {
      TabLayoutResource = Resource.Layout.Tabbar;
      ToolbarResource = Resource.Layout.Toolbar;

      _es=new EcoProject1EcoSpace();
      _es.PMapperXml.OnLoadAsXML += Pm_OnLoadAsXML;
      _es.PMapperXml.OnSaveAsXML += Pm_OnSaveAsXML;

      ViewModelDefinitionsInApplication.Init(_es);

      _es.Active = true;
      BaseViewModel.DataStore.SetEcoSpace(_es);

      base.OnCreate(bundle);

      global::Xamarin.Forms.Forms.Init(this, bundle);

      LoadApplication(new App());
    }

    private void Pm_OnSaveAsXML(object sender, Eco.Persistence.OnSaveAsXMLArgs e)
    {
      var path = global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
      var filename = System.IO.Path.Combine(path.ToString(), e.FileName);
      e.XDocumentToSave.Save(filename);
      e.ContinueWithDefault = false;
    }

    private void Pm_OnLoadAsXML(object sender, Eco.Persistence.OnLoadAsXMLArgs e)
    {
      var path = global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
      var filename = System.IO.Path.Combine(path.ToString(), e.FileName);
      if (System.IO.File.Exists(filename))
      {
        using (var fileStream = System.IO.File.OpenRead(filename))
        {
          using (var streamReader = new System.IO.StreamReader(fileStream))
          {
            e.LoadedDataAsXml = streamReader.ReadToEnd();
            e.ContinueWithDefault = false;
          }
        }
      }
    }


  }
}
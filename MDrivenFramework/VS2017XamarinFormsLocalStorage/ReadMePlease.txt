This sample has a video and article found here:
http://www.new.capableobjects.com/2017/04/02/getting-mdriven-benefits-on-devices/

It has come to our attention that weh you nuget MDriven EcoCore it looks like VS2017 picks the wrong assemblies.

It seems to pick these:
VS2017XamarinFormsLocalStorage\packages\EcoCore.7.0.0.xxxx\lib\Unity-Full-v35

when in fact it should use these:
VS2017XamarinFormsLocalStorage\packages\EcoCore.7.0.0.xxxx\lib\portable-windows8+net45

If you get this issue - remove the Eco assemblies and find the ones in lib\portable-windows8+net45 instead
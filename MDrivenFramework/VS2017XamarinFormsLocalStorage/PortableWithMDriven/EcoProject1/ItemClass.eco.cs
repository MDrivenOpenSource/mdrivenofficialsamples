//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EcoProject1 {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using System.CodeDom.Compiler;
  using Eco.ObjectImplementation;
  using Eco.ObjectRepresentation;
  using Eco.Services;
  using Eco.Subscription;
  using Eco.UmlCodeAttributes;
  using Eco.UmlRt;
  
  
  [UmlElement(Id="4f9f3cd8-9892-439a-9faa-e34698e129a1")]
  public partial class ItemClass : EcoProject1.BaseDataClass {
    
    #region *** Constructors ***
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public ItemClass(Eco.ObjectRepresentation.IEcoServiceProvider serviceProvider) : 
        base(serviceProvider) {
      try {
        this.ObjectCreated();
      }
      catch (System.Exception ) {
        this.Deinitialize(serviceProvider);
        throw;
      }
    }
    
    /// <summary>
    /// For framework internal use only
    /// Constructor public for one reason only; to avoid need for ReflectionPermission in reduced trust
    /// </summary>
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public ItemClass(Eco.ObjectImplementation.IContent content) : 
        base(content) {
      this.ObjectFetched();
    }
    
    /// <summary>
    /// Creates an Offline object. Can for example be used by MVC runtime to pass values
    /// This is type equivalent to the eco object in everyway - but has none of the eco services
    /// It does however implement INotifyPropertyChanged and INotifyCollectionChanged
    /// </summary>
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public ItemClass() {
      this.eco_Content = new EcoOfflineValues();
      this.ObjectCreated();
    }
    
    /// <summary>This method is called when the object has been created the first time (not when loaded from a db)</summary>
    partial void ObjectCreated();
    
    /// <summary>This method is called when the object has been loaded from a db (not when it is created the first time)</summary>
    partial void ObjectFetched();
    
    /// <summary>This method is called before the object is deleted. You can perform custom clean up or prevent the deletion by returning false or throw an exception (preferably EcoObjectDeleteException)</summary>
    partial void PreDelete(ref System.Boolean canDelete);
    
    #endregion *** Constructors ***
    
    #region *** ILoopback implementation ***
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public override void set_MemberByIndex(int index, object value) {
      base.set_MemberByIndex(index, value);
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public override object get_MemberByIndex(int index) {
      return base.get_MemberByIndex(index);
    }
    
    #endregion *** ILoopback implementation ***
    
    #region *** LoopbackIndex declarations ***
    
    new public struct Eco_LoopbackIndices {
      
      public const int Eco_FirstMember = EcoProject1.BaseDataClass.Eco_LoopbackIndices.Eco_MemberCount;
      
      public const int Description = Eco_FirstMember;
      
      public const int Text = (Description + 1);
      
      public const int Eco_MemberCount = (Text + 1);
    }
    
    #endregion *** LoopbackIndex declarations ***
    
    [UmlTaggedValue("Eco.AllowNULL", "True")]
    [UmlElement(Id="caf2ecac-d378-487f-bcb2-4bf01cba809f", Index=Eco_LoopbackIndices.Description)]
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public string Description {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex_OfflineAware(Eco_LoopbackIndices.Description, typeof(string))));
      }
      set {
        this.eco_Content.set_MemberByIndex_OfflineAware(Eco_LoopbackIndices.Description, ((object)(value)), "Description");
      }
    }
    
    [UmlTaggedValue("Eco.AllowNULL", "True")]
    [UmlElement(Id="eeb66cf8-ac5e-4da7-b309-1275def0e589", Index=Eco_LoopbackIndices.Text)]
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public string Text {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex_OfflineAware(Eco_LoopbackIndices.Text, typeof(string))));
      }
      set {
        this.eco_Content.set_MemberByIndex_OfflineAware(Eco_LoopbackIndices.Text, ((object)(value)), "Text");
      }
    }
  }
}

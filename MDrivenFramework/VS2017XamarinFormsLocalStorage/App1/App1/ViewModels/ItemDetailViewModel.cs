﻿
using EcoProject1;

namespace App1.ViewModels
{
  public class ItemDetailViewModel : BaseViewModel
  {
    public ItemClass Item { get; set; }
    public ItemDetailViewModel(ItemClass item = null)
    {
      Title = item.Text;
      Item = item;
    }

    int quantity = 1;
    public int Quantity
    {
      get { return quantity; }
      set { SetProperty(ref quantity, value); }
    }
  }
}
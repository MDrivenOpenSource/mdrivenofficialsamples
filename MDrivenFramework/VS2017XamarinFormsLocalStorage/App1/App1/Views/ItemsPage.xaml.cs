﻿using System;

using App1.ViewModels;

using Xamarin.Forms;
using EcoProject1;
using PortableWithMDriven.ViewModelCodeGen_DMItemsViewModel;
using App1.Services;

namespace App1.Views
{
  public partial class ItemsPage : ContentPage
  {
    DMItemsViewModel viewModel;

    public ItemsPage()
    {
      InitializeComponent();
      viewModel = DMItemsViewModel.Create(BaseViewModel.DataStore.GetEcoSpace(), null);
    }

    private void AssignBindindContext()
    {
      Dispatcher.CurrentDispatcher.BeginInvoke(() => {
        BindingContext = viewModel;
      });
      
    }

    async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
    {
      var item = args.SelectedItem as Item;
      if (item == null)
        return;

      await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(item.Element.AsObject as ItemClass)));

      // Manually deselect item
      ItemsListView.SelectedItem = null;
    }

    async void AddItem_Clicked(object sender, EventArgs e)
    {
      await Navigation.PushAsync(new NewItemPage());
    }

    protected override async void OnAppearing()
    {
      base.OnAppearing();
      AssignBindindContext();
      
    }

    protected override void OnDisappearing()
    {
      base.OnDisappearing();
      BindingContext = null;
    }
  }
}

namespace WPFWithViewModel
{
    public partial class WPFWithViewModelEcoSpace
    {
        protected WPFWithViewModel.WPFWithViewModelPackage IncludeEcoPackage_WPFWithViewModel_WPFWithViewModelPackage;
        /// <summary>
        /// Required designer variable
        /// </summary>
        private System.ComponentModel.Container components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Active = false;
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {

        }
    }
}

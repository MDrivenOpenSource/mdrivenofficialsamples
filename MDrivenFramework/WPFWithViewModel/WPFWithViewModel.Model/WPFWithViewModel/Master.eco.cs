//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WPFWithViewModel {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;
  
  
  [UmlElement(Id="655b211d-d049-4fb3-8610-791cda9eba23")]
  public partial class Master : Eco.ObjectImplementation.ILoopBack2, System.ComponentModel.INotifyPropertyChanged {
    
    #region *** Constructors ***
    
    public Master(Eco.ObjectRepresentation.IEcoServiceProvider serviceProvider) {
      this.Initialize(serviceProvider);
      try {
        this.ObjectCreated();
      }
      catch (System.Exception ) {
        this.Deinitialize(serviceProvider);
        throw;
      }
    }
    
    // For framework internal use only
    protected Master(Eco.ObjectImplementation.IContent content) {
      this.eco_Content = content;
      content.AssertLoopbackUnassigned();
    }
    
    /// <summary>This method is called when the object has been created the first time (not when loaded from a db)</summary>
    partial void ObjectCreated();
    
    /// <summary>This method is before the object is deleted. You can perform custom clean up or prevent the deletion by returning false or throw an exception (preferably EcoObjectDeleteException)</summary>
    partial void PreDelete(ref System.Boolean canDelete);
    
    #endregion *** Constructors ***
    
    #region *** ILoopback implementation ***
    
    public virtual void set_MemberByIndex(int index, object value) {
      throw new System.IndexOutOfRangeException();
    }
    
    public virtual object get_MemberByIndex(int index) {
      throw new System.IndexOutOfRangeException();
    }
    
    Eco.ObjectRepresentation.IObject Eco.ObjectRepresentation.IObjectProvider.AsIObject() {
      return this.eco_Content.AsIObject();
    }
    
    void Eco.ObjectImplementation.ILoopBack2.SetContent(Eco.ObjectImplementation.IContent content) {
      if ((this.eco_Content != null)) {
        throw new System.InvalidOperationException();
      }
      this.eco_Content = content;
    }
    
    #endregion *** ILoopback implementation ***
    
    #region *** LoopbackIndex declarations ***
    
    public class Eco_LoopbackIndices {
      
      public const int Eco_FirstMember = 0;
      
      public const int Attribute2 = Eco_FirstMember;
      
      public const int Attribute3 = (Attribute2 + 1);
      
      public const int Attribute4 = (Attribute3 + 1);
      
      public const int Attribute5 = (Attribute4 + 1);
      
      public const int Attribute1 = (Attribute5 + 1);
      
      public const int TypicalComboChoose = (Attribute1 + 1);
      
      public const int Detail = (TypicalComboChoose + 1);
      
      public const int Eco_MemberCount = (Detail + 1);
    }
    
    #endregion *** LoopbackIndex declarations ***
    
    #region *** IObjectProvider implementation ***
    
    public virtual Eco.ObjectRepresentation.IObjectInstance AsIObject() {
      return this.eco_Content.AsIObject();
    }
    
    #endregion *** IObjectProvider implementation ***
    
    #region *** INotifyPropertyChanged implementation ***
    
    event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged {
    
      add { eco_Content.PropertyChanged += value; }
    
      remove { eco_Content.PropertyChanged -= value; }
    
    }
    
    #endregion *** INotifyPropertyChanged implementation ***
    
    #region *** ECO framework implementations ***
    
    protected Eco.ObjectImplementation.IContent eco_Content;
    
    protected virtual void Initialize(Eco.ObjectRepresentation.IEcoServiceProvider serviceProvider) {
      if ((this.eco_Content == null)) {
        Eco.ObjectImplementation.IInternalObjectContentFactory factory = serviceProvider.GetEcoService<Eco.ObjectImplementation.IInternalObjectContentFactory>();
        factory.CreateContent(this);
        this.eco_Content.LoopbackValid();
      }
    }
    
    protected virtual void Deinitialize(Eco.ObjectRepresentation.IEcoServiceProvider serviceProvider) {
      if ((this.eco_Content == null)) {
        Eco.ObjectImplementation.IInternalObjectContentFactory factory = serviceProvider.GetEcoService<Eco.ObjectImplementation.IInternalObjectContentFactory>();
        factory.CreateContentFailed(this.eco_Content, this);
        this.eco_Content = null;
      }
    }
    
    #endregion *** ECO framework implementations ***
    
    [UmlElement(Id="0ef77350-1d74-44dc-965c-1f79bf1958ea", Index=Eco_LoopbackIndices.Attribute2)]
    public System.DateTime Attribute2 {
      get {
        return ((System.DateTime)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.Attribute2)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.Attribute2, ((object)(value)));
      }
    }
    
    [UmlElement(Id="154b093f-c628-4852-9a20-19935af65121", Index=Eco_LoopbackIndices.Attribute3)]
    public bool Attribute3 {
      get {
        return ((bool)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.Attribute3)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.Attribute3, ((object)(value)));
      }
    }
    
    [UmlElement(Id="36d6f3ca-3b8e-49c7-8168-9f2ed8b5521e", Index=Eco_LoopbackIndices.Attribute4)]
    public double Attribute4 {
      get {
        return ((double)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.Attribute4)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.Attribute4, ((object)(value)));
      }
    }
    
    [UmlElement(Id="ae8a6320-5970-48f4-ab48-112aa0a052f8", Index=Eco_LoopbackIndices.Attribute5)]
    public string Attribute5 {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.Attribute5)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.Attribute5, ((object)(value)));
      }
    }
    
    [UmlElement(Id="b57a203b-553a-4d70-8bcf-310d8c40732f", Index=Eco_LoopbackIndices.Attribute1)]
    public string Attribute1 {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.Attribute1)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.Attribute1, ((object)(value)));
      }
    }
    
    [UmlElement("AssociationEnd", Id="533b3a07-0fa6-40fe-885d-8e99f2c4ea37", Index=Eco_LoopbackIndices.TypicalComboChoose)]
    [UmlMetaAttribute("association", typeof(WPFWithViewModelPackage.MasterMasterTypicalComboChooseTypicalComboChoose), Index=0)]
    [UmlMetaAttribute("multiplicity", "0..1")]
    public TypicalComboChoose TypicalComboChoose {
      get {
        return ((TypicalComboChoose)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.TypicalComboChoose)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.TypicalComboChoose, ((object)(value)));
      }
    }
    
    [UmlElement("AssociationEnd", Id="e72f8c0d-50c2-4ac8-a30d-050b7e17d9b9", Index=Eco_LoopbackIndices.Detail)]
    [UmlMetaAttribute("association", typeof(WPFWithViewModelPackage.DetailDetailMasterMaster), Index=1)]
    [UmlMetaAttribute("multiplicity", "0..*")]
    public IEcoList<Detail> Detail {
      get {
        return ((IEcoList<Detail>)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.Detail)));
      }
    }
  }
}

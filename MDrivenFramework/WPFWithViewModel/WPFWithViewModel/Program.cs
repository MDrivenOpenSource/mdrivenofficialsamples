namespace WPFWithViewModel
{
    using System;
    using System.Windows.Forms;
    using Eco.Windows.Forms;

    public static class Program
    {
        /// <summary>
        /// Stores the global EcoSpace instance.
        /// </summary>
        private static WPFWithViewModel.WPFWithViewModelEcoSpace ecoSpace;

        /// <summary>
        /// Gets the global EcoSpace instance.
        /// </summary>
        public static WPFWithViewModel.WPFWithViewModelEcoSpace EcoSpace
        {
            get { return ecoSpace; }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            WinFormDequeuer.Active = true;
            Form mainForm = null;
            try
            {
                ecoSpace = new WPFWithViewModel.WPFWithViewModelEcoSpace();
                ecoSpace.Active = true;
                mainForm = new Form1(ecoSpace);
            }
            catch (Exception e)
            {
                new ThreadExceptionDialog(e).ShowDialog();
                throw;
            }
            
            Application.Run(mainForm);
            ecoSpace.Dispose();
        }
    }
}

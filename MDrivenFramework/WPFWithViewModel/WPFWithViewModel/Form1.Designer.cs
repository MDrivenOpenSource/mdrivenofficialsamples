namespace WPFWithViewModel
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Windows.Forms;
    using Eco.Handles;
    using Eco.ObjectRepresentation;
    using Eco.Services;
    using Eco.Windows.Forms;
    using Eco.WinForm;

    public partial class Form1
    {
        private Eco.Handles.ReferenceHandle rhRoot;
        private Eco.WinForm.EcoActionExtender ecoGlobalActions;
        private Eco.WinForm.EcoDragDropExtender ecoDragDrop;
        private Eco.WinForm.EcoAutoFormExtender ecoAutoForms;
        private Eco.WinForm.EcoListActionExtender ecoListActions;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.rhRoot = new Eco.Handles.ReferenceHandle();
          this.ecoListActions = new Eco.WinForm.EcoListActionExtender();
          this.ecoDragDrop = new Eco.WinForm.EcoDragDropExtender();
          this.ecoGlobalActions = new Eco.WinForm.EcoActionExtender();
          this.ecoAutoForms = new Eco.WinForm.EcoAutoFormExtender();
          this.viewModelUserControl1 = new Eco.ViewModel.WinForms.ViewModelUserControl();
          this.btnAutoForm = new System.Windows.Forms.Button();
          this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
          this.label2 = new System.Windows.Forms.Label();
          this.label1 = new System.Windows.Forms.Label();
          this.button2 = new System.Windows.Forms.Button();
          this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
          this.userControl11 = new WPFWithViewModel.UserControl1();
          ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).BeginInit();
          this.tableLayoutPanel1.SuspendLayout();
          this.SuspendLayout();
          // 
          // rhRoot
          // 
          this.rhRoot.EcoSpaceTypeName = "WPFWithViewModel.WPFWithViewModelEcoSpace";
          // 
          // ecoListActions
          // 
          this.ecoListActions.DeleteQuestion = "Delete object(s)?";
          // 
          // ecoGlobalActions
          // 
          this.ecoGlobalActions.RootHandle = this.rhRoot;
          // 
          // viewModelUserControl1
          // 
          this.viewModelUserControl1.DefaultDataSourceUpdateMode = System.Windows.Forms.DataSourceUpdateMode.OnValidation;
          this.viewModelUserControl1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.viewModelUserControl1.EcoSpaceTypeName = "WPFWithViewModel.WPFWithViewModelEcoSpace";
          this.viewModelUserControl1.Location = new System.Drawing.Point(3, 41);
          this.viewModelUserControl1.Name = "viewModelUserControl1";
          this.viewModelUserControl1.Preview = false;
          this.viewModelUserControl1.ReadOnly = false;
          this.viewModelUserControl1.Size = new System.Drawing.Size(426, 434);
          this.viewModelUserControl1.TabIndex = 0;
          this.viewModelUserControl1.ViewModelName = "FirstViewModel";
          // 
          // btnAutoForm
          // 
          this.btnAutoForm.Location = new System.Drawing.Point(435, 489);
          this.btnAutoForm.Name = "btnAutoForm";
          this.btnAutoForm.Size = new System.Drawing.Size(122, 27);
          this.btnAutoForm.TabIndex = 2;
          this.btnAutoForm.Text = "AutoForm";
          this.btnAutoForm.UseVisualStyleBackColor = true;
          this.btnAutoForm.Click += new System.EventHandler(this.btnAutoForm_Click);
          // 
          // tableLayoutPanel1
          // 
          this.tableLayoutPanel1.ColumnCount = 2;
          this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
          this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
          this.tableLayoutPanel1.Controls.Add(this.button2, 0, 3);
          this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
          this.tableLayoutPanel1.Controls.Add(this.btnAutoForm, 0, 3);
          this.tableLayoutPanel1.Controls.Add(this.elementHost1, 1, 1);
          this.tableLayoutPanel1.Controls.Add(this.viewModelUserControl1, 0, 1);
          this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
          this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 2);
          this.tableLayoutPanel1.Name = "tableLayoutPanel1";
          this.tableLayoutPanel1.RowCount = 4;
          this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.949791F));
          this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.05021F));
          this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
          this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
          this.tableLayoutPanel1.Size = new System.Drawing.Size(864, 519);
          this.tableLayoutPanel1.TabIndex = 3;
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.label2.Location = new System.Drawing.Point(435, 0);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(419, 29);
          this.label2.TabIndex = 4;
          this.label2.Text = "Below is WPF viewmodel user control";
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.label1.Location = new System.Drawing.Point(3, 0);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(378, 38);
          this.label1.TabIndex = 3;
          this.label1.Text = "Below is winform viewmodel user control";
          // 
          // button2
          // 
          this.button2.Location = new System.Drawing.Point(3, 489);
          this.button2.Name = "button2";
          this.button2.Size = new System.Drawing.Size(122, 27);
          this.button2.TabIndex = 5;
          this.button2.Text = "Create new master";
          this.button2.UseVisualStyleBackColor = true;
          // 
          // elementHost1
          // 
          this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.elementHost1.Location = new System.Drawing.Point(435, 41);
          this.elementHost1.Name = "elementHost1";
          this.elementHost1.Size = new System.Drawing.Size(426, 434);
          this.elementHost1.TabIndex = 1;
          this.elementHost1.Text = "elementHost1";
          this.elementHost1.Child = this.userControl11;
          // 
          // Form1
          // 
          this.ClientSize = new System.Drawing.Size(875, 533);
          this.Controls.Add(this.tableLayoutPanel1);
          this.Name = "Form1";
          this.Text = "Form1";
          this.Load += new System.EventHandler(this.Form1_Load);
          ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).EndInit();
          this.tableLayoutPanel1.ResumeLayout(false);
          this.tableLayoutPanel1.PerformLayout();
          this.ResumeLayout(false);

        }
        #endregion

        private Eco.ViewModel.WinForms.ViewModelUserControl viewModelUserControl1;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private UserControl1 userControl11;
        private Button btnAutoForm;
        private TableLayoutPanel tableLayoutPanel1;
        private Label label2;
        private Label label1;
        private Button button2;
    }
}

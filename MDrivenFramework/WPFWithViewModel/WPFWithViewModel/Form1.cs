namespace WPFWithViewModel
{
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using System.Data;
  using System.Drawing;
  using System.Linq;
  using System.Windows.Forms;
  using Eco.Handles;
  using Eco.Linq;
  using Eco.ObjectRepresentation;
  using Eco.Services;
  using Eco.Windows.Forms;
  using Eco.WinForm;
  using Eco.ViewModel.Runtime;

  /// <summary>
  /// Summary description for Form1.
  /// </summary>
  public partial class Form1 : System.Windows.Forms.Form
  {
    public Form1(WPFWithViewModel.WPFWithViewModelEcoSpace ecoSpace)
    {
      // Required for Windows Form Designer support
      this.InitializeComponent();

      // Set EcoSpace (actually stores it in rhRoot.EcoSpace)
      // Note that you need to set the EcoSpace property for 
      // each ReferenceHandle and VariableHandle you add to 
      // the form.
      this.rhRoot.EcoSpace = ecoSpace;
      viewModelUserControl1.SetEcoSpace(ecoSpace);
      this.userControl11.viewModelWPFUserControl1.SetEcoSpace(ecoSpace);
      ViewDefinitionsInApplication.Init(ecoSpace);
      InitData();
    }

    private void InitData()
    {
      for (int i = 0; i < 5; i++)
        new TypicalComboChoose(EcoSpace) { Attribute1 = "value" + i.ToString() };

      button1_Click(null, null);
    }

    public WPFWithViewModel.WPFWithViewModelEcoSpace EcoSpace
    {
      get
      {
        return (WPFWithViewModel.WPFWithViewModelEcoSpace)this.rhRoot.EcoSpace;
      }
    }

    private void Form1_Load(object sender, EventArgs e)
    {

    }

    Master m = null;
    private void button1_Click(object sender, EventArgs e)
    {
      m = new Master(EcoSpace);
      this.viewModelUserControl1.ViewModel.SetElement(m);
      this.userControl11.viewModelWPFUserControl1.ViewModel.SetElement(m);
    }

    private void btnAutoForm_Click(object sender, EventArgs e)
    {
      var args = new Eco.AutoContainers.AutoContainerArgs(EcoSpace);
      Eco.AutoContainers.AutoContainerService.Instance.CreateContainer(m.AsIObject(), args).Show(args);
    }

  }
}

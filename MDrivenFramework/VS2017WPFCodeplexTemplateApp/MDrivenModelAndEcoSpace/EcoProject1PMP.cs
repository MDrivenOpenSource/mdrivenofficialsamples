namespace EcoProject1.EcoSpaceAndModel1
{
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Handles;
  using Eco.Services;
  using Eco.Persistence;
  using Eco.Wcf.Server;

  public partial class EcoProject1PMP : Eco.Persistence.PersistenceMapperProvider
  {
    public EcoProject1PMP()
      : base()
    {
      this.InitializeComponent();
      if (this.PersistenceMapper==this.persistenceMapperWCFClient1 &&  this.persistenceMapperWCFClient1.Uri == "https://YOURPLACE.azurewebsites.net/APP_A0/A0_PMPWCF.svc")
        throw new Exception(
          "This exception is to point your attention to that you need to set up YOUR MDrivenServer\r\n"+
          "Once you have it you need to set the Uri, The User and The Password for it\r\n"+
          "Setting up a MDriven Server is really painless - http://www.new.capableobjects.com/2014/05/19/new-video-on-mdrivenserver/"
      );
    }

    /// <summary>
    /// Gets the singleton instance of the PersistenceMapperProvider.
    /// </summary>
    public static EcoProject1PMP Instance
    {
      get
      {
        return GetInstance<EcoProject1PMP>();
      }
    }

    /// <summary>
    /// Regenerates the database schema, no questions asked.
    /// </summary>
    public static void GenerateDB()
    {
      Instance.DoGenerateDB();
    }
    #region Eco Managed Code
    private void DoGenerateDB()
    {
      if (PersistenceMapper is PersistenceMapperDb)
      {
        (PersistenceMapper as PersistenceMapperDb).CreateDatabaseSchema(GetTypeSystemService(true), new DefaultCleanPsConfig(true));
      }
      else
      {
        throw new InvalidOperationException("The PersistenceMapper is not a PersistenceMapperDb");
      }
    }
    #endregion
  }

}

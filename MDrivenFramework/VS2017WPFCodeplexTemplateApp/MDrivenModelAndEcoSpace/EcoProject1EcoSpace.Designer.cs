namespace EcoProject1
{
    public partial class EcoProject1EcoSpace
    {
        /// <summary>
        /// Required designer variable
        /// </summary>
        private System.ComponentModel.Container components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Active = false;
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
      this.persistenceMapperSharer1 = new Eco.Persistence.PersistenceMapperSharer();
      // 
      // persistenceMapperSharer1
      // 
      this.persistenceMapperSharer1.MapperProviderTypeName = "EcoProject1.EcoSpaceAndModel1.EcoProject1PMP";
      // 
      // EcoProject1EcoSpace
      // 
      this.PersistenceMapper = this.persistenceMapperSharer1;

        }
        protected Eco.Web.AspProviders.EcoAspProviderPackage IncludeEcoPackage_Eco_Web_AspProviders_EcoAspProviderPackage;
        protected EcoProject1Package IncludeEcoPackage_EcoProject1_EcoProject1Package;
        private Eco.Persistence.PersistenceMapperSharer persistenceMapperSharer1;
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EcoProject1.ViewModelCodeGen_Ticket {
  using System;
  using System.ComponentModel;
  using System.Collections.Generic;
  using System.CodeDom.Compiler;
  using Eco.Handles;
  using Eco.ObjectRepresentation;
  using Eco.ViewModel.Runtime;
  
  
  public partial class Ticket : Eco.ViewModel.Runtime.VMClass {
    
    /// <summary>
    /// This constructor creates an Offline ViewModel class
    /// </summary>
    public Ticket() {
    }
    
    protected static System.Type GetVMClassType(Eco.ViewModel.Runtime.VMClassDescriptor desc) {
      if ((desc.Name == "Ticket")) {
        return typeof(Ticket);
      }
      if ((desc.Name == "VM_Validations")) {
        return typeof(VM_Validations);
      }
      if ((desc.Name == "VM_Status")) {
        return typeof(VM_Status);
      }
      if ((desc.Name == "VM_Variables")) {
        return typeof(VM_Variables);
      }
      if ((desc.Name == "TicketAttachments")) {
        return typeof(TicketAttachments);
      }
      if ((desc.Name == "TicketLogRows")) {
        return typeof(TicketLogRows);
      }
      if ((desc.Name == "self")) {
        return typeof(self);
      }
      throw new System.Exception("Generated ViewModel code out of data");
    }
    
    public static Ticket Create(Eco.Handles.EcoSpace es, EcoProject1.Ticket obj) {
      return ((Ticket)(ViewModelHelper.CreateFromViewModel("Ticket", es, obj, new GetVMClassTypeDelegate(Ticket.GetVMClassType))));
    }
    
    /// <summary>
    /// Setting this will make the ViewModel reset to Content=null, and become online
    /// </summary>
    public void SetEcoSpace(Eco.Handles.EcoSpace es) {
      this.SetEcoSpace("Ticket", es, new GetVMClassTypeDelegate(Ticket.GetVMClassType));
    }
    
    /// <summary>
    /// Constructor public for technical reasons, use static Create on ViewModel root to create online ViewModel
    /// </summary>
    public Ticket(Eco.ViewModel.Runtime.VMClassDescriptor vMClassDescriptor, Eco.ObjectRepresentation.IEcoServiceProvider sp, Eco.ViewModel.Runtime.VMClass ownedby, Eco.ObjectRepresentation.IElement content, Eco.ObjectRepresentation.IExternalVariableList variables) : 
        base(vMClassDescriptor, sp, ownedby, content, variables) {
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public string Creator_Name {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Creator_Name", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Creator_Name", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public string Created {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Created", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
    }
    
    [System.ComponentModel.DataAnnotations.Required()]
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public string ShortDescription {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("ShortDescription", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("ShortDescription", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public string State {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("State", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
    }
    
    [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "LongDescription is required.")]
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public string LongDescription {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("LongDescription", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("LongDescription", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    public VM_Validations VM_Validations {
      get {
        return ((VM_Validations)(((Eco.ViewModel.Runtime.VMSingleAssociation)(this.GetMemberOfflineAware("VM_Validations", typeof(Eco.ViewModel.Runtime.VMSingleAssociation)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMSingleAssociation)(this.GetMemberOfflineAware("VM_Validations", typeof(Eco.ViewModel.Runtime.VMSingleAssociation)))).Value = value;
      }
    }
    
    public VM_Status VM_Status {
      get {
        return ((VM_Status)(((Eco.ViewModel.Runtime.VMSingleAssociation)(this.GetMemberOfflineAware("VM_Status", typeof(Eco.ViewModel.Runtime.VMSingleAssociation)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMSingleAssociation)(this.GetMemberOfflineAware("VM_Status", typeof(Eco.ViewModel.Runtime.VMSingleAssociation)))).Value = value;
      }
    }
    
    public VM_Variables VM_Variables {
      get {
        return ((VM_Variables)(((Eco.ViewModel.Runtime.VMSingleAssociation)(this.GetMemberOfflineAware("VM_Variables", typeof(Eco.ViewModel.Runtime.VMSingleAssociation)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMSingleAssociation)(this.GetMemberOfflineAware("VM_Variables", typeof(Eco.ViewModel.Runtime.VMSingleAssociation)))).Value = value;
      }
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public IList<TicketAttachments> TicketAttachments {
      get {
        return ((VMManyAssociation<TicketAttachments>)(this.GetMemberOfflineAware("TicketAttachments", typeof(VMManyAssociation<TicketAttachments>))));
      }
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public IList<TicketLogRows> TicketLogRows {
      get {
        return ((VMManyAssociation<TicketLogRows>)(this.GetMemberOfflineAware("TicketLogRows", typeof(VMManyAssociation<TicketLogRows>))));
      }
    }
  }
  
  public class VM_Validations : Eco.ViewModel.Runtime.VMClass {
    
    /// <summary>
    /// This constructor creates an Offline ViewModel class
    /// </summary>
    public VM_Validations() {
    }
    
    /// <summary>
    /// Constructor public for technical reasons, use static Create on ViewModel root to create online ViewModel
    /// </summary>
    public VM_Validations(Eco.ViewModel.Runtime.VMClassDescriptor vMClassDescriptor, Eco.ObjectRepresentation.IEcoServiceProvider sp, Eco.ViewModel.Runtime.VMClass ownedby, Eco.ObjectRepresentation.IElement content, Eco.ObjectRepresentation.IExternalVariableList variables) : 
        base(vMClassDescriptor, sp, ownedby, content, variables) {
    }
  }
  
  public class VM_Status : Eco.ViewModel.Runtime.VMClass {
    
    /// <summary>
    /// This constructor creates an Offline ViewModel class
    /// </summary>
    public VM_Status() {
    }
    
    /// <summary>
    /// Constructor public for technical reasons, use static Create on ViewModel root to create online ViewModel
    /// </summary>
    public VM_Status(Eco.ViewModel.Runtime.VMClassDescriptor vMClassDescriptor, Eco.ObjectRepresentation.IEcoServiceProvider sp, Eco.ViewModel.Runtime.VMClass ownedby, Eco.ObjectRepresentation.IElement content, Eco.ObjectRepresentation.IExternalVariableList variables) : 
        base(vMClassDescriptor, sp, ownedby, content, variables) {
    }
    
    public bool Ticket_Creator_Name_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_Creator_Name_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_Creator_Name_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_Creator_Name_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_Creator_Name_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_Creator_Name_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_Creator_Name_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_Creator_Name_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_Created_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_Created_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_Created_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_Created_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_Created_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_Created_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_Created_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_Created_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_ShortDescription_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_ShortDescription_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_ShortDescription_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_ShortDescription_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_ShortDescription_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_ShortDescription_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_ShortDescription_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_ShortDescription_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_State_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_State_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_State_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_State_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_State_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_State_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_State_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_State_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_LongDescription_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_LongDescription_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_LongDescription_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_LongDescription_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_LongDescription_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_LongDescription_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_LongDescription_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_LongDescription_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_TicketAttachments_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_TicketAttachments_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_TicketAttachments_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_TicketAttachments_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_TicketAttachments_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_TicketAttachments_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_TicketAttachments_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_TicketAttachments_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_TicketLogRows_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_TicketLogRows_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_TicketLogRows_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_TicketLogRows_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_TicketLogRows_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_TicketLogRows_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool Ticket_TicketLogRows_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Ticket_TicketLogRows_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketAttachments_Name_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketAttachments_Name_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketAttachments_Name_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketAttachments_Name_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketAttachments_Name_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketAttachments_Name_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketAttachments_Name_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketAttachments_Name_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_TicketStateChange_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_TicketStateChange_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_TicketStateChange_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_TicketStateChange_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_TicketStateChange_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_TicketStateChange_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_TicketStateChange_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_TicketStateChange_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_Message_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_Message_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_Message_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_Message_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_Message_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_Message_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_Message_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_Message_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_self_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_self_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_self_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_self_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_self_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_self_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool TicketLogRows_self_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketLogRows_self_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool self_Message_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("self_Message_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool self_Message_Enabled {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("self_Message_Enabled", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool self_Message_Visible {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("self_Message_Visible", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool self_Message_Style {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("self_Message_Style", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
    
    public bool CompleteVM_ReadOnly {
      get {
        return ((bool)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("CompleteVM_ReadOnly", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute), false))).Value));
      }
    }
  }
  
  public class VM_Variables : Eco.ViewModel.Runtime.VMClass {
    
    /// <summary>
    /// This constructor creates an Offline ViewModel class
    /// </summary>
    public VM_Variables() {
    }
    
    /// <summary>
    /// Constructor public for technical reasons, use static Create on ViewModel root to create online ViewModel
    /// </summary>
    public VM_Variables(Eco.ViewModel.Runtime.VMClassDescriptor vMClassDescriptor, Eco.ObjectRepresentation.IEcoServiceProvider sp, Eco.ViewModel.Runtime.VMClass ownedby, Eco.ObjectRepresentation.IElement content, Eco.ObjectRepresentation.IExternalVariableList variables) : 
        base(vMClassDescriptor, sp, ownedby, content, variables) {
    }
    
    public EcoProject1.Ticket vCurrent_Ticket {
      get {
        return ((EcoProject1.Ticket)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_Ticket", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_Ticket", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    public string vCurrent_Ticket_AsExternalId {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_Ticket_AsExternalId", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_Ticket_AsExternalId", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    public IEnumerable<EcoProject1.Ticket> vSelected_Ticket {
      get {
        return ((Eco.ViewModel.Runtime.VMNativeTypeCollection<EcoProject1.Ticket>)(this.GetMemberOfflineAware("vSelected_Ticket", typeof(Eco.ViewModel.Runtime.VMNativeTypeCollection<EcoProject1.Ticket>)))).TypedList;
      }
    }
    
    public EcoProject1.TicketAttachment vCurrent_TicketAttachments {
      get {
        return ((EcoProject1.TicketAttachment)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_TicketAttachments", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_TicketAttachments", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    public string vCurrent_TicketAttachments_AsExternalId {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_TicketAttachments_AsExternalId", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_TicketAttachments_AsExternalId", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    public IEnumerable<EcoProject1.TicketAttachment> vSelected_TicketAttachments {
      get {
        return ((Eco.ViewModel.Runtime.VMNativeTypeCollection<EcoProject1.TicketAttachment>)(this.GetMemberOfflineAware("vSelected_TicketAttachments", typeof(Eco.ViewModel.Runtime.VMNativeTypeCollection<EcoProject1.TicketAttachment>)))).TypedList;
      }
    }
    
    public EcoProject1.TicketLogRow vCurrent_TicketLogRows {
      get {
        return ((EcoProject1.TicketLogRow)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_TicketLogRows", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_TicketLogRows", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    public string vCurrent_TicketLogRows_AsExternalId {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_TicketLogRows_AsExternalId", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_TicketLogRows_AsExternalId", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    public IEnumerable<EcoProject1.TicketLogRow> vSelected_TicketLogRows {
      get {
        return ((Eco.ViewModel.Runtime.VMNativeTypeCollection<EcoProject1.TicketLogRow>)(this.GetMemberOfflineAware("vSelected_TicketLogRows", typeof(Eco.ViewModel.Runtime.VMNativeTypeCollection<EcoProject1.TicketLogRow>)))).TypedList;
      }
    }
    
    public EcoProject1.TicketLogRow vCurrent_self {
      get {
        return ((EcoProject1.TicketLogRow)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_self", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_self", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    public string vCurrent_self_AsExternalId {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_self_AsExternalId", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vCurrent_self_AsExternalId", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    public IEnumerable<EcoProject1.TicketLogRow> vSelected_self {
      get {
        return ((Eco.ViewModel.Runtime.VMNativeTypeCollection<EcoProject1.TicketLogRow>)(this.GetMemberOfflineAware("vSelected_self", typeof(Eco.ViewModel.Runtime.VMNativeTypeCollection<EcoProject1.TicketLogRow>)))).TypedList;
      }
    }
    
    public string vGridActionArgument {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vGridActionArgument", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("vGridActionArgument", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
  }
  
  public class TicketAttachments : Eco.ViewModel.Runtime.VMClass {
    
    /// <summary>
    /// This constructor creates an Offline ViewModel class
    /// </summary>
    public TicketAttachments() {
    }
    
    /// <summary>
    /// Constructor public for technical reasons, use static Create on ViewModel root to create online ViewModel
    /// </summary>
    public TicketAttachments(Eco.ViewModel.Runtime.VMClassDescriptor vMClassDescriptor, Eco.ObjectRepresentation.IEcoServiceProvider sp, Eco.ViewModel.Runtime.VMClass ownedby, Eco.ObjectRepresentation.IElement content, Eco.ObjectRepresentation.IExternalVariableList variables) : 
        base(vMClassDescriptor, sp, ownedby, content, variables) {
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public string Name {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Name", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Name", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
  }
  
  public class TicketLogRows : Eco.ViewModel.Runtime.VMClass {
    
    /// <summary>
    /// This constructor creates an Offline ViewModel class
    /// </summary>
    public TicketLogRows() {
    }
    
    /// <summary>
    /// Constructor public for technical reasons, use static Create on ViewModel root to create online ViewModel
    /// </summary>
    public TicketLogRows(Eco.ViewModel.Runtime.VMClassDescriptor vMClassDescriptor, Eco.ObjectRepresentation.IEcoServiceProvider sp, Eco.ViewModel.Runtime.VMClass ownedby, Eco.ObjectRepresentation.IElement content, Eco.ObjectRepresentation.IExternalVariableList variables) : 
        base(vMClassDescriptor, sp, ownedby, content, variables) {
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public string TicketStateChange {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketStateChange", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("TicketStateChange", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public string Message {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Message", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Message", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public self self {
      get {
        return ((self)(((Eco.ViewModel.Runtime.VMSingleAssociation)(this.GetMemberOfflineAware("self", typeof(Eco.ViewModel.Runtime.VMSingleAssociation)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMSingleAssociation)(this.GetMemberOfflineAware("self", typeof(Eco.ViewModel.Runtime.VMSingleAssociation)))).Value = value;
      }
    }
  }
  
  public class self : Eco.ViewModel.Runtime.VMClass {
    
    /// <summary>
    /// This constructor creates an Offline ViewModel class
    /// </summary>
    public self() {
    }
    
    /// <summary>
    /// Constructor public for technical reasons, use static Create on ViewModel root to create online ViewModel
    /// </summary>
    public self(Eco.ViewModel.Runtime.VMClassDescriptor vMClassDescriptor, Eco.ObjectRepresentation.IEcoServiceProvider sp, Eco.ViewModel.Runtime.VMClass ownedby, Eco.ObjectRepresentation.IElement content, Eco.ObjectRepresentation.IExternalVariableList variables) : 
        base(vMClassDescriptor, sp, ownedby, content, variables) {
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public string Message {
      get {
        return ((string)(((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Message", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value));
      }
      set {
        ((Eco.ViewModel.Runtime.VMNativeTypeAttribute)(this.GetMemberOfflineAware("Message", typeof(Eco.ViewModel.Runtime.VMNativeTypeAttribute)))).Value = value;
      }
    }
  }
}

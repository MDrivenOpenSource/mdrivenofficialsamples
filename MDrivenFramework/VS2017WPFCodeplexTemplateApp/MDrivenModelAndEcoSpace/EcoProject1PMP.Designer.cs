namespace EcoProject1.EcoSpaceAndModel1
{
  public partial class EcoProject1PMP
  {
    #region Component Designer generated code
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.Container components = null;

    private Eco.Persistence.SyncHandler syncHandler1;

    private void InitializeComponent()
    {
      this.syncHandler1 = new Eco.Persistence.SyncHandler();
      this.persistenceMapperWCFClient1 = new Eco.Wcf.Client.PersistenceMapperWCFClient();
      this.persistenceMapperXml1 = new Eco.Persistence.PersistenceMapperXml();
      // 
      // syncHandler1
      // 
      this.syncHandler1.HistoryLength = 10000;
      // 
      // persistenceMapperWCFClient1
      // 
      this.persistenceMapperWCFClient1.ClientCredentialType = System.ServiceModel.HttpClientCredentialType.None;
      this.persistenceMapperWCFClient1.EndpointConfigurationName = null;
      this.persistenceMapperWCFClient1.MaxReceivedMessageSize = ((long)(2147483647));
      this.persistenceMapperWCFClient1.Uri = "https://YOURPLACE.azurewebsites.net/APP_A0/A0_PMPWCF.svc";
      this.persistenceMapperWCFClient1.UseWSHttp = true;
      this.persistenceMapperWCFClient1.WCFServerPassword = "a";
      this.persistenceMapperWCFClient1.WCFServerUserName = "a";
      // 
      // persistenceMapperXml1
      // 
      this.persistenceMapperXml1.FileName = "XMLFileToStoreData.xml";
      // 
      // EcoProject1PMP
      // 
      this.EcoSpaceTypeName = "EcoProject1.EcoProject1EcoSpace";
      this.PersistenceMapper = this.persistenceMapperXml1;

    }
    #endregion

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (this.components != null)
        {
          this.components.Dispose();
        }
      }

      base.Dispose(disposing);
    }

    private Eco.Wcf.Client.PersistenceMapperWCFClient persistenceMapperWCFClient1;
    private Eco.Persistence.PersistenceMapperXml persistenceMapperXml1;
  }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EcoProject1 {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using System.CodeDom.Compiler;
  using Eco.ObjectImplementation;
  using Eco.ObjectRepresentation;
  using Eco.Services;
  using Eco.Subscription;
  using Eco.UmlCodeAttributes;
  using Eco.UmlRt;
  
  
  [UmlElement(Id="ab14c3a6-42dc-4bf9-97c7-4394e8e3eb95")]
  [UmlTaggedValue("Eco.IsSingleton", "True")]
  public partial class GlobalSingleton : Eco.ObjectImplementation.ILoopBack3, System.ComponentModel.INotifyPropertyChanged {
    
    #region *** Constructors ***
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public GlobalSingleton(Eco.ObjectRepresentation.IEcoServiceProvider serviceProvider) {
      this.Initialize(serviceProvider);
      try {
        this.ObjectCreated();
      }
      catch (System.Exception ) {
        this.Deinitialize(serviceProvider);
        throw;
      }
    }
    
    /// <summary>
    /// For framework internal use only
    /// Constructor public for one reason only; to avoid need for ReflectionPermission in reduced trust
    /// </summary>
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public GlobalSingleton(Eco.ObjectImplementation.IContent content) {
      this.eco_Content = content;
      content.AssertLoopbackUnassigned();
      this.ObjectFetched();
    }
    
    /// <summary>
    /// Creates an Offline object. Can for example be used by MVC runtime to pass values
    /// This is type equivalent to the eco object in everyway - but has none of the eco services
    /// It does however implement INotifyPropertyChanged and INotifyCollectionChanged
    /// </summary>
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public GlobalSingleton() {
      this.eco_Content = new EcoOfflineValues();
      this.ObjectCreated();
    }
    
    /// <summary>This method is called when the object has been created the first time (not when loaded from a db)</summary>
    partial void ObjectCreated();
    
    /// <summary>This method is called when the object has been loaded from a db (not when it is created the first time)</summary>
    partial void ObjectFetched();
    
    /// <summary>This method is called before the object is deleted. You can perform custom clean up or prevent the deletion by returning false or throw an exception (preferably EcoObjectDeleteException)</summary>
    partial void PreDelete(ref System.Boolean canDelete);
    
    #endregion *** Constructors ***
    
    #region *** ILoopback implementation ***
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public virtual void set_MemberByIndex(int index, object value) {
      throw new System.IndexOutOfRangeException();
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public virtual object get_MemberByIndex(int index) {
      throw new System.IndexOutOfRangeException();
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    Eco.ObjectRepresentation.IObject Eco.ObjectRepresentation.IObjectProvider.AsIObject() {
      return this.eco_Content.AsIObject();
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    void Eco.ObjectImplementation.ILoopBack2.SetContent(Eco.ObjectImplementation.IContent content) {
      if ((this.eco_Content != null)) {
        throw new System.InvalidOperationException();
      }
      this.eco_Content = content;
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    Eco.ObjectImplementation.IContent Eco.ObjectImplementation.ILoopBack3.GetContent() {
      return this.eco_Content;
    }
    
    #endregion *** ILoopback implementation ***
    
    #region *** LoopbackIndex declarations ***
    
    public struct Eco_LoopbackIndices {
      
      public const int Eco_FirstMember = 0;
      
      public const int CurrentUser = Eco_FirstMember;
      
      public const int Eco_MemberCount = (CurrentUser + 1);
    }
    
    #endregion *** LoopbackIndex declarations ***
    
    #region *** IObjectProvider implementation ***
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public virtual Eco.ObjectRepresentation.IObjectInstance AsIObject() {
      return this.eco_Content.AsIObject();
    }
    
    #endregion *** IObjectProvider implementation ***
    
    #region *** INotifyPropertyChanged implementation ***
    
    event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged {
    
      add { eco_Content.PropertyChanged += value; }
    
      remove { eco_Content.PropertyChanged -= value; }
    
    }
    
    #endregion *** INotifyPropertyChanged implementation ***
    
    #region *** ECO framework implementations ***
    
    protected Eco.ObjectImplementation.IContent eco_Content;
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    protected virtual void Initialize(Eco.ObjectRepresentation.IEcoServiceProvider serviceProvider) {
      if ((this.eco_Content == null)) {
        Eco.ObjectImplementation.IInternalObjectContentFactory factory = serviceProvider.GetEcoService<Eco.ObjectImplementation.IInternalObjectContentFactory>();
        factory.CreateContent(this);
        this.eco_Content.LoopbackValid();
      }
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    protected virtual void Deinitialize(Eco.ObjectRepresentation.IEcoServiceProvider serviceProvider) {
      if ((this.eco_Content == null)) {
        Eco.ObjectImplementation.IInternalObjectContentFactory factory = serviceProvider.GetEcoService<Eco.ObjectImplementation.IInternalObjectContentFactory>();
        factory.CreateContentFailed(this.eco_Content, this);
        this.eco_Content = null;
      }
    }
    
    #endregion *** ECO framework implementations ***
    
    [UmlElement("AssociationEnd", Id="bac4ce7b-6efd-483a-93ae-6040770e597f", Index=Eco_LoopbackIndices.CurrentUser)]
    [UmlMetaAttribute("association", typeof(EcoProject1Package.UserCurrentUserGlobalSingletonGlobalSingletonsCurrentUser), Index=0)]
    [UmlMetaAttribute("multiplicity", "0..1")]
    [UmlTaggedValue("Eco.DerivationOCL", "self.GetCurrentUser")]
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public User CurrentUser {
      get {
        return ((User)(this.eco_Content.get_MemberByIndex_OfflineAware(Eco_LoopbackIndices.CurrentUser, typeof(User))));
      }
    }
  }
}

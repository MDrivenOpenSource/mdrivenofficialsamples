namespace EcoProject1 {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using System.CodeDom.Compiler;
  using Eco.ObjectImplementation;
  using Eco.ObjectRepresentation;
  using Eco.Services;
  using Eco.Subscription;
  using Eco.UmlCodeAttributes;
  using Eco.UmlRt;
  
  
  public partial class GlobalSingleton {
    [UmlElement(Id = "51b32d15-9409-412a-856a-500182147c67"), UmlMetaAttribute("isQuery", true)]
    public User GetCurrentUser()
    {
      return _currentUser;
    }

    private User _currentUser;
    public static void InstallCurrentUserFromStringIdentifier(IEcoServiceProvider sp, string id)
    {
      var gs = EcoServiceHelper.GetEcoService<IOclService>(sp).Evaluate("GlobalSingleton.oclSingleton").GetValue<GlobalSingleton>();      
      var userlist = EcoServiceHelper.GetEcoService<IOclPsService>(sp).Execute("User.allinstances->select(name='" + id + "')").GetAsIList<User>();      
      if (userlist.Count == 0)
      {
        var user = new User(sp);
        user.Name = id;
        EcoServiceHelper.GetEcoService<IPersistenceService>(sp).UpdateDatabase(user);
        gs._currentUser = user;
      }
      else
        gs._currentUser = userlist[0];

    }


    public static bool IsLoggedIn(IEcoServiceProvider sp)
    {
      var gs = EcoServiceHelper.GetEcoService<IOclService>(sp).Evaluate("GlobalSingleton.oclSingleton").GetValue<GlobalSingleton>();
      return gs.CurrentUser != null;
    }
  }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Eco.Wcf.Common;
using Eco.Wcf.Server;

namespace EcoSolutionShowcase
{
    // NOTE: If you change the class name "EcoSolutionShowcasePMPWCF" here, you must also update the reference to "EcoSolutionShowcasePMPWCF" in Web.config.
    public class EcoSolutionShowcasePMPWCF : PersistenceMapperProviderWCF<EcoSolutionShowcase.EcoSolutionShowcasePMP>, IPersistenceMapperWCF
    {
    }
}

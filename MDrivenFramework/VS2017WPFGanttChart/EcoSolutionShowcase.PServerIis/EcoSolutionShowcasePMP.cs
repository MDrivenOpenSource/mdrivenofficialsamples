namespace EcoSolutionShowcase
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Handles;
    using Eco.Services;
    using Eco.Persistence;
    using Eco.Wcf.Server;
    
    public partial class EcoSolutionShowcasePMP : Eco.Persistence.PersistenceMapperProvider
    {
        public EcoSolutionShowcasePMP() : base()
        {
            this.InitializeComponent();
          // IMPORTANT CONFIGURATION SETTINGS START
#if DEBUG_DEVSERVER
            // I choose to use DevServer and an XML file for persistence for shortest development loop
            PersistenceMapper = persistenceMapperXml1;
            persistenceMapperXml1.FileName = @"Data3.xml"; 
#endif
#if DEBUG_IIS
            // ... then I use SQLServer and IIS for a bit more real life debugging/development
            persistenceMapperSqlServer1.ConnectionString = "Data Source=localhost;Initial Catalog=EcoShowcase;Integrated Security=True";
            PersistenceMapper = persistenceMapperSqlServer1;
#endif

          // IMPORTANT CONFIGURATION SETTINGS END
        }

        /// <summary>
        /// Gets the singleton instance of the PersistenceMapperProvider.
        /// </summary>
        public static EcoSolutionShowcasePMP Instance
        {
            get
            {
                return GetInstance<EcoSolutionShowcasePMP>();
            }
        }

        /// <summary>
        /// Regenerates the database schema, no questions asked.
        /// </summary>
        public static void GenerateDB()
        {
            Instance.DoGenerateDB();
        }

        #region EcoSpace type reference
        static EcoSolutionShowcasePMP()
        {
            // this registration is here to ensure that the 
            // ecospace type is not removed by the optimization in the compiler
            // if you rename your ecospace, please update this reference too.
            EcoSpace.RegisterEcoSpaceType(typeof(EcoSolutionShowcase.EcoSolutionShowcaseEcoSpace));
        }
        #endregion
        #region Eco Managed Code
        private void DoGenerateDB()
        {
            if (PersistenceMapper is PersistenceMapperDb)
            {
                (PersistenceMapper as PersistenceMapperDb).CreateDatabaseSchema(GetTypeSystemService(true), new DefaultCleanPsConfig(true));
            }
            else
            {
                throw new InvalidOperationException("The PersistenceMapper is not a PersistenceMapperDb");
            }
        }
        #endregion
    }
    
}

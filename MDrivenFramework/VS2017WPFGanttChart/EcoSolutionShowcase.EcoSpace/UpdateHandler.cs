﻿using System;
using System.Collections.Generic;
using Eco.Persistence;
using Eco.Cache;
using Eco.UmlRt;
using Eco.Framework;
using Eco.ObjectRepresentation;

namespace EcoSolutionShowcase
{
  public class UpdateHandler : ChainedPersistenceHandlerBase
  {
    private readonly IObjectRepresentationProvider orp;
    public UpdateHandler(IObjectRepresentationProvider orp, IEcoTypeSystem typeSystem)
      : base(typeSystem)
    {
      this.orp = orp;
    }
    public override void UpdateDatabaseWithList(ICollection<Locator> locators)
    {
      DateTime timestamp = DateTime.Now;
      foreach (Locator loc in locators)
      {
        IObject obj = orp.IObjectForLocator(loc);
        if (obj.AsObject is ITrackUpdate)
        {
          (obj.AsObject as ITrackUpdate).PreUpdate(timestamp);
        }
      }
      base.UpdateDatabaseWithList(locators);
    }
  }
}

namespace EcoSolutionShowcase
{
    public partial class EcoSolutionShowcaseEcoSpace
    {
        protected EcoSolutionShowcase.EcoSolutionShowcasePackage IncludeEcoPackage_EcoSolutionShowcase_EcoSolutionShowcasePackage;
        /// <summary>
        /// Required designer variable
        /// </summary>
        private System.ComponentModel.Container components = null;

        private Eco.Wcf.Client.PersistenceMapperWCFClient persistenceMapperClient1;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Active = false;
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
      this.persistenceMapperClient1 = new Eco.Wcf.Client.PersistenceMapperWCFClient();
      this.persistenceMapperXml1 = new Eco.Persistence.PersistenceMapperXml();
      // 
      // persistenceMapperClient1
      // 
      this.persistenceMapperClient1.ClientCredentialType = System.ServiceModel.HttpClientCredentialType.None;
      this.persistenceMapperClient1.EndpointConfigurationName = null;
      this.persistenceMapperClient1.MaxReceivedMessageSize = ((long)(2147483647));
      this.persistenceMapperClient1.Uri = "http://localhost:12345/EcoSolutionShowcasePMPWCF.svc";
      this.persistenceMapperClient1.UseWSHttp = false;
      this.persistenceMapperClient1.WCFServerPassword = null;
      this.persistenceMapperClient1.WCFServerUserName = null;
      // 
      // persistenceMapperXml1
      // 
      this.persistenceMapperXml1.FileName = "TheData.xml";
      // 
      // EcoSolutionShowcaseEcoSpace
      // 
      this.PersistenceMapper = this.persistenceMapperXml1;

        }

    private Eco.Persistence.PersistenceMapperXml persistenceMapperXml1;
  }
}

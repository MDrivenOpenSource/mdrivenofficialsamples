﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WECPOFLogic;
using PlexityHide.GTP;
using Eco.Services;
using System.Windows.Markup;
using System.Globalization;

namespace EcoSolutionShowcase
{
  /// <summary>
  /// Interaction logic for WindowWECPOF2.xaml
  /// </summary>
  public partial class WindowWECPOF2 : Window
  {
    private EcoSolutionShowcase.EcoSolutionShowcaseEcoSpace ecoSpace;
    private MenuHandling _MenuHandling;

    public WindowWECPOF2()
    {
      InitializeComponent();

    
    }

    public void InitEcoSpace(EcoSolutionShowcaseEcoSpace ecoSpace)
    {
      this.ecoSpace = ecoSpace;
      // EcoServiceHelper.GetAsyncSupportService(ecoSpace).TurnOnAsyncHandling();


              ResourceDictionary dictionary = new ResourceDictionary();
      dictionary.Source = new Uri("/EcoSolutionShowcase.WPF;component/Look1.xaml", UriKind.RelativeOrAbsolute);
      WECPOFTabBased1.WECPOFSkinHandling.SkinDictionaries.Add("Look1", dictionary);
      WECPOFTabBased1.ScreenCreatedBeforePopulate += new EventHandler<ScreenCreatedArgs>(WECPOFTabBased1_ScreenCreatedBeforePopulate);
      
      dictionary = new ResourceDictionary();
      dictionary.Source = new Uri("/EcoSolutionShowcase.WPF;component/MDrivenDesignerDefault.xaml", UriKind.RelativeOrAbsolute);
      WECPOFTabBased1.WECPOFSkinHandling.SkinDictionaries.Add("MDrivenDesignerDefault", dictionary);
      WECPOFTabBased1.WECPOFSkinHandling.InitSkins();

      _MenuHandling = new MenuHandling(ecoSpace);

      _MenuHandling.InitMainMenu(WECPOFTabBased1.MainMenu, WECPOFTabBased1, WECPOFTabBased1, this, true, true);
      WECPOFTabBased1.InstallMenuHandling(_MenuHandling);
      MenuHandling.OnExit += new EventHandler<EventArgs>(MenuHandling_OnExit);
      WECPOFTabBased1.WECPOFSkinHandling.ApplyFirstStyle();

    }

    void WECPOFTabBased1_ScreenCreatedBeforePopulate(object sender, ScreenCreatedArgs e)
    {
      e.Screen.UserControl.OnColumnUIOverride += new EventHandler<Eco.ViewModel.Runtime.OnColumnUIOverrideArgs>(UserControl_OnColumnUIOverride);
    }

    void UserControl_OnColumnUIOverride(object sender, Eco.ViewModel.Runtime.OnColumnUIOverrideArgs e)
    {
      if (e.ViewModelColumn.Name == "GanttPlaceHolder")
      {
        GanttDisplay g = new GanttDisplay();
        Grid grid=e.ViewModelColumn.SpawnedArtifacts["control"] as Grid;
        grid.Children.Add(g);
        
      }
    }

    void MenuHandling_OnExit(object sender, EventArgs e)
    {
      Close();
    }
  }
}

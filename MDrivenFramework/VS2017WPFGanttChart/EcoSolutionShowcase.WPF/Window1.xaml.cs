using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Eco.ViewModel.Runtime;
using WECPOFLogic;
using System.Windows.Markup;
using System.Globalization;

namespace EcoSolutionShowcase
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private EcoSolutionShowcase.EcoSolutionShowcaseEcoSpace ecoSpace;
        private Eco.WinForm.EcoAutoFormExtender ecoAutoForms;
        public Window1()
        {
            ecoSpace = new EcoSolutionShowcase.EcoSolutionShowcaseEcoSpace();
            ecoSpace.Active = true;
            Eco.WPF.WPFDequeuer.Active = true;
            ViewDefinitionsInApplication.Init(ecoSpace);
            InitializeComponent();
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));


            ecoAutoForms = new Eco.WinForm.EcoAutoFormExtender(); // optional
        }


       

      

        private void AutoViewbutton_Click(object sender, RoutedEventArgs e)
        {
          WindowAutoViewViewModel w = new WindowAutoViewViewModel();
          w.InitEcoSpace(ecoSpace);
          w.Show();
        }

        private void CodegenViewModelsbutton_Click(object sender, RoutedEventArgs e)
        {
          WindowCodeGenViewModel w = new WindowCodeGenViewModel();
          w.InitEcoSpace(ecoSpace);
          w.Show();
        }


        private void Wecpof2button_Click(object sender, RoutedEventArgs e)
        {
          WindowWECPOF2 w = new WindowWECPOF2();
          w.InitEcoSpace(ecoSpace);
          w.Show();
        }
    }
}

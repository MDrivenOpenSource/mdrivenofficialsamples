﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EcoSolutionShowcase
{
  /// <summary>
  /// Interaction logic for GanttDisplay.xaml
  /// </summary>
  public partial class GanttDisplay : UserControl
  {
    public GanttDisplay()
    {
      InitializeComponent();
      DataContextChanged += new DependencyPropertyChangedEventHandler(GanttDisplay_DataContextChanged);
    }

    void GanttDisplay_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      if ((e.NewValue is Project))
        treeview1.ItemsSource = (e.NewValue as Project).Tasks;
      else
        treeview1.ItemsSource = null; 
    }
  }
}

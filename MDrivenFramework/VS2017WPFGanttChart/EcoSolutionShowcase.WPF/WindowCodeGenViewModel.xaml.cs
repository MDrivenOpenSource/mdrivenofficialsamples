﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using EcoSolutionShowcase.ViewModelCodeGen_ViewModelGroupsAndProjects;

namespace EcoSolutionShowcase
{
  /// <summary>
  /// Interaction logic for WindowCodeGenViewModel.xaml
  /// </summary>
  public partial class WindowCodeGenViewModel : Window
  {
    private EcoSolutionShowcase.EcoSolutionShowcaseEcoSpace ecoSpace;
    public WindowCodeGenViewModel()
    {
      InitializeComponent();
    }

    private ViewModelGroupsAndProjects _thecodegeneratedViewModel;
    internal void InitEcoSpace(EcoSolutionShowcaseEcoSpace ecoSpace)
    {
      this.ecoSpace = ecoSpace;
      _thecodegeneratedViewModel= Resources["codegenVM1"] as ViewModelGroupsAndProjects;
      _thecodegeneratedViewModel.SetEcoSpace(ecoSpace); // This particular ViewModel does not need a root object
    }

    private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
    {

    }

    private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
    {

    }

   

   
  }

  public class MyOwnRoutedCommand : RoutedCommand
  { 
  
  }
}

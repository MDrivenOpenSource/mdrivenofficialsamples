﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EcoSolutionShowcase
{
  /// <summary>
  /// Interaction logic for WindowAutoViewViewModel.xaml
  /// </summary>
  public partial class WindowAutoViewViewModel : Window
  {
    private EcoSolutionShowcase.EcoSolutionShowcaseEcoSpace ecoSpace;
    public WindowAutoViewViewModel()
    {
      InitializeComponent();
    }

    internal void InitEcoSpace(EcoSolutionShowcaseEcoSpace ecoSpace)
    {
      this.ecoSpace = ecoSpace;
      viewModelWPFUserControl1.SetEcoSpace(ecoSpace);
     

      // sample code
      // Project c = new Project(ecoSpace);
      // viewModelWPFUserControl1.RootObject = c; This particular ViewModel does not need a RootObject
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      ProjectGroup pg = new ProjectGroup(ecoSpace);
      pg.Name = "Added this thru codebehind";
      pg.Projects.Add(new Project(ecoSpace) { Name = "And associated this project too" });
      pg.Projects.Add(new Project(ecoSpace) { Name = "and this too" });

    }
  }
}

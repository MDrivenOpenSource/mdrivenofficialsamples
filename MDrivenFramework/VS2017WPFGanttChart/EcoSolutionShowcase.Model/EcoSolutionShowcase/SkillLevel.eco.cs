//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EcoSolutionShowcase {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using System.CodeDom.Compiler;
  using Eco.ObjectImplementation;
  using Eco.ObjectRepresentation;
  using Eco.Services;
  using Eco.Subscription;
  using Eco.UmlCodeAttributes;
  using Eco.UmlRt;
  
  
  [UmlElement(Id="d41f3886-8744-4265-81e5-79e40fa12c1f")]
  [UmlMetaAttribute("association", typeof(EcoSolutionShowcasePackage.CoWorkerCoWorkersSkillSkills))]
  public partial class SkillLevel : EcoSolutionShowcase.CommonSuper {
    
    #region *** Constructors ***
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public SkillLevel(Eco.ObjectRepresentation.IEcoServiceProvider serviceProvider) : 
        base(serviceProvider) {
      try {
        this.ObjectCreated();
      }
      catch (System.Exception ) {
        this.Deinitialize(serviceProvider);
        throw;
      }
    }
    
    /// <summary>
    /// For framework internal use only
    /// Constructor public for one reason only; to avoid need for ReflectionPermission in reduced trust
    /// </summary>
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public SkillLevel(Eco.ObjectImplementation.IContent content) : 
        base(content) {
      this.ObjectFetched();
    }
    
    /// <summary>
    /// Creates an Offline object. Can for example be used by MVC runtime to pass values
    /// This is type equivalent to the eco object in everyway - but has none of the eco services
    /// It does however implement INotifyPropertyChanged and INotifyCollectionChanged
    /// </summary>
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public SkillLevel() {
      this.eco_Content = new EcoOfflineValues();
      this.ObjectCreated();
    }
    
    /// <summary>This method is called when the object has been created the first time (not when loaded from a db)</summary>
    partial void ObjectCreated();
    
    /// <summary>This method is called when the object has been loaded from a db (not when it is created the first time)</summary>
    partial void ObjectFetched();
    
    /// <summary>This method is called before the object is deleted. You can perform custom clean up or prevent the deletion by returning false or throw an exception (preferably EcoObjectDeleteException)</summary>
    partial void PreDelete(ref System.Boolean canDelete);
    
    #endregion *** Constructors ***
    
    #region *** ILoopback implementation ***
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public override void set_MemberByIndex(int index, object value) {
      base.set_MemberByIndex(index, value);
    }
    
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public override object get_MemberByIndex(int index) {
      return base.get_MemberByIndex(index);
    }
    
    #endregion *** ILoopback implementation ***
    
    #region *** LoopbackIndex declarations ***
    
    new public struct Eco_LoopbackIndices {
      
      public const int Eco_FirstMember = EcoSolutionShowcase.CommonSuper.Eco_LoopbackIndices.Eco_MemberCount;
      
      public const int LevelPercent = Eco_FirstMember;
      
      public const int Skills = (LevelPercent + 1);
      
      public const int CoWorkers = (Skills + 1);
      
      public const int Eco_MemberCount = (CoWorkers + 1);
    }
    
    #endregion *** LoopbackIndex declarations ***
    
    [UmlElement(Id="b817f03d-19d0-4113-b67d-be3f6096902a", Index=Eco_LoopbackIndices.LevelPercent)]
    [GeneratedCodeAttribute("ECO", "7.0.0.0")]
    public int LevelPercent {
      get {
        return ((int)(this.eco_Content.get_MemberByIndex_OfflineAware(Eco_LoopbackIndices.LevelPercent, typeof(int))));
      }
      set {
        this.eco_Content.set_MemberByIndex_OfflineAware(Eco_LoopbackIndices.LevelPercent, ((object)(value)), "LevelPercent");
      }
    }
    
    public Skill Skills {
      get {
        return ((Skill)(this.eco_Content.get_MemberByIndex_OfflineAware(Eco_LoopbackIndices.Skills, typeof(Skill))));
      }
    }
    
    public CoWorker CoWorkers {
      get {
        return ((CoWorker)(this.eco_Content.get_MemberByIndex_OfflineAware(Eco_LoopbackIndices.CoWorkers, typeof(CoWorker))));
      }
    }
  }
}

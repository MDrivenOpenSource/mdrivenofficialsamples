// This file contains all user-written code for the class

namespace EcoSolutionShowcase {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;

  public partial class Project
  {
    partial void NumberOfTasksDerive(ref int res)
    {
      // just to test code derive in SL
      res=this.Tasks.Count;
    }

  }
}

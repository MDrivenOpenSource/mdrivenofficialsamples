namespace EcoSolutionShowcase {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;


  public interface ITrackUpdate
  {
    void PreUpdate(DateTime timestamp);
  }

  public partial class CommonSuper : ITrackUpdate
  {
    partial void ObjectCreated()
    {
      CreateTime = DateTime.Now;
      this.Guid = System.Guid.NewGuid().ToString();
    }
    public void PreUpdate(DateTime timestamp)
    {
      ChangeTime = DateTime.Now;
    }
  }
}
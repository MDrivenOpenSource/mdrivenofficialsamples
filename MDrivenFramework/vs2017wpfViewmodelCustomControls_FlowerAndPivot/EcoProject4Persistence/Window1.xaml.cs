using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Eco.ViewModel.Runtime;
using Eco.Services;
using WECPOFLogic;
using System.IO;

namespace EcoProject4Persistence
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private EcoProject4Persistence.EcoProject4PersistenceEcoSpace _ecoSpace;
        public Window1()
        {
          
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
          _ecoSpace = new EcoProject4Persistence.EcoProject4PersistenceEcoSpace();
          _ecoSpace.Active = true;          
          
          InitializeComponent();
          wecpof.WECPOFSkinHandling.PathToStyles = Directory.GetCurrentDirectory();
          wecpof.WECPOFSkinHandling.ResourceDictionaryTargetForStyles(wecpof);
          wecpof.EasyInit(_ecoSpace);
          
        }

      
    }
}

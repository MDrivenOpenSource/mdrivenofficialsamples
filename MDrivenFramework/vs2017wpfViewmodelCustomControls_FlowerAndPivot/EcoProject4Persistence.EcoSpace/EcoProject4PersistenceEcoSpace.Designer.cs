namespace EcoProject4Persistence
{
    public partial class EcoProject4PersistenceEcoSpace
    {
        /// <summary>
        /// Required designer variable
        /// </summary>
        private System.ComponentModel.Container components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Active = false;
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
      this.persistenceMapperWCFClient1 = new Eco.Wcf.Client.PersistenceMapperWCFClient();
      this.persistenceMapperXml1 = new Eco.Persistence.PersistenceMapperXml();
      // 
      // persistenceMapperWCFClient1
      // 
      this.persistenceMapperWCFClient1.EndpointConfigurationName = "CONFIG1";
      this.persistenceMapperWCFClient1.MaxReceivedMessageSize = ((long)(2147483647));
      this.persistenceMapperWCFClient1.Uri = "";
      this.persistenceMapperWCFClient1.WCFFactoryCreated += new System.EventHandler<Eco.Wcf.Client.WCFFactoryCreatedArgs>(this.persistenceMapperWCFClient1_WCFFactoryCreated);
      // 
      // persistenceMapperXml1
      // 
      this.persistenceMapperXml1.FileName = "data.xml";
      // 
      // EcoProject4PersistenceEcoSpace
      // 
      this.PersistenceMapper = this.persistenceMapperXml1;

        }

        private Eco.Wcf.Client.PersistenceMapperWCFClient persistenceMapperWCFClient1;
        protected EcoProject4PersistencePackage IncludeEcoPackage_EcoProject4Persistence_EcoProject4PersistencePackage;
        private Eco.Persistence.PersistenceMapperXml persistenceMapperXml1;
    }
}

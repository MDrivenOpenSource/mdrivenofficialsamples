namespace StateDemo
{
	public partial class StateMachinePMP
	{
		#region Component Designer generated code
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private Eco.Persistence.SyncHandler syncHandler1;

		private Eco.Persistence.PersistenceMapperXml persistenceMapperXml1;
		private void InitializeComponent()
		{
            this.syncHandler1 = new Eco.Persistence.SyncHandler();
            this.persistenceMapperXml1 = new Eco.Persistence.PersistenceMapperXml();
            // 
            // syncHandler1
            // 
            this.syncHandler1.HistoryLength = 10000;
            // 
            // persistenceMapperXml1
            // 
            this.persistenceMapperXml1.FileName = "Data.xml";
            this.persistenceMapperXml1.SyncHandler = this.syncHandler1;
            // 
            // StateMachinePMP
            // 
            this.EcoSpaceTypeName = "StateDemo.StateMachineEcoSpace";
            this.PersistenceMapper = this.persistenceMapperXml1;

		}
		#endregion
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}

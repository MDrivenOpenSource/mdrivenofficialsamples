namespace StateDemo
{
	public partial class StateMachineEcoSpace
	{
		/// <summary>
		/// Required designer variable
		/// </summary>
		private System.ComponentModel.Container components = null;

		private Eco.Persistence.PersistenceMapperSharer persistenceMapperSharer1;
		private void InitializeComponent()
		{
            this.persistenceMapperSharer1 = new Eco.Persistence.PersistenceMapperSharer();
            // 
            // persistenceMapperSharer1
            // 
            this.persistenceMapperSharer1.MapperProviderTypeName = "StateDemo.StateMachinePMP";
            // 
            // StateMachineEcoSpace
            // 
            this.PersistenceMapper = this.persistenceMapperSharer1;

		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose (bool disposing)
		{
			if (disposing)
			{
				Active = false;
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
        }

        protected StateDemoPackage IncludeEcoPackage_StateDemo_StateDemoPackage;
	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using Eco.Services;
using Eco.UmlCodeAttributes;

namespace StateDemo
{
	[EcoSpace]
	public partial class StateMachineEcoSpace: Eco.Handles.DefaultEcoSpace
	{
		#region Eco Managed code
		private static ITypeSystemService c_TypeSystemProvider;
		public static new ITypeSystemService GetTypeSystemService()
		{
			if (c_TypeSystemProvider == null)
				lock(typeof(StateMachineEcoSpace))
				{
					if (c_TypeSystemProvider == null)
						c_TypeSystemProvider = MakeTypeService(typeof(StateMachineEcoSpace));
				}
			return c_TypeSystemProvider;
		}
		protected override ITypeSystemService GetTypeSystemProvider()
		{
			return StateMachineEcoSpace.GetTypeSystemService();
		}
		#endregion

		public StateMachineEcoSpace(): base()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Persist all changes to the domain objects.
		/// </summary>
		/// <remarks>
		/// This function persists all changes to the eco space, including object creation,
		/// object manipulation, changed associations and object deletions. After invoking this method
		/// all undo information is removed.
		/// If the application does not have any persistence layer defined the operation does nothing.
		/// </remarks>
		public void UpdateDatabase()
		{
			if ((Persistence != null) && (DirtyList != null))
			{
				Persistence.UpdateDatabaseWithList(DirtyList.AllDirtyObjects());
			}
		}

		//
		// Add user written methods here
		//
	}
}

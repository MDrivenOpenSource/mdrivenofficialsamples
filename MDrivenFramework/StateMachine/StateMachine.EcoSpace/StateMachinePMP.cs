using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using Eco.Persistence;
using Eco.Handles;


namespace StateDemo
{
	public partial class StateMachinePMP : Eco.Persistence.PersistenceMapperProvider
	{
		#region Eco Managed Code
		private void DoGenerateDB()
		{
			if (PersistenceMapper is PersistenceMapperDb)
				(PersistenceMapper as PersistenceMapperDb).CreateDatabaseSchema(GetTypeSystemService(true), new DefaultCleanPsConfig(true));
			else
				throw new InvalidOperationException("The PersistenceMapper is not a PersistenceMapperDb");
		}
		/// <summary>
		/// Returns the singleton instance of the PersistenceMapperProvider.
		/// </summary>
		public static StateMachinePMP Instance
		{
			get
			{
				return GetInstance<StateMachinePMP>();
			}
		}
		#endregion

		public StateMachinePMP(): base()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Regenerates the database schema, no questions asked.
		/// </summary>
		public static void GenerateDB()
		{
			Instance.DoGenerateDB();
		}
	}
}

using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Eco.Handles;
using Eco.Windows.Forms;
using Eco.WinForm;
using Eco.ObjectRepresentation;
using Eco.Services;

namespace StateDemo
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public partial class Form1 : System.Windows.Forms.Form
	{

		public Form1(StateDemo.StateMachineEcoSpace ecoSpace)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// Set EcoSpace (actually stores it in rhRoot.EcoSpace)
			// Note that you need to set the EcoSpace property for 
			// each ReferenceHandle and VariableHandle you add to 
			// the form.
			rhRoot.EcoSpace = ecoSpace;
            vhWaybillNo.EcoSpace = ecoSpace;
        }

		public StateDemo.StateMachineEcoSpace EcoSpace
		{
			get { return (StateDemo.StateMachineEcoSpace)rhRoot.EcoSpace; }
		}

        private void btnCreateTestData_Click(object sender, EventArgs e)
        {
            Customer c1 = new Customer(EcoSpace);
            c1.Name = "Jack";

            Customer c2 = new Customer(EcoSpace);
            c2.Name = "Jill";

            Order o1 = new Order(EcoSpace);
            o1.Customer = c2;
            Order o2 = new Order(EcoSpace);

        }

        private void btnNewOrder_Click(object sender, EventArgs e)
        {
            new Order(EcoSpace);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            EcoSpace.UpdateDatabase();
        }
	}
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Eco.Handles;
using Eco.Windows.Forms;
using Eco.WinForm;
using Eco.ObjectRepresentation;
using Eco.Services;

namespace StateDemo
{
	public partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose (bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Eco.Handles.ReferenceHandle rhRoot;
        private Eco.WinForm.EcoActionExtender EcoGlobalActions;
        private Eco.WinForm.EcoDragDropExtender EcoDragDrop;
        private Eco.WinForm.EcoAutoFormExtender EcoAutoForms;
        private Eco.WinForm.EcoListActionExtender EcoListActions;
        private System.Windows.Forms.Button btnCreateTestData;
        private System.Windows.Forms.DataGrid dgCustomers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGrid dgOrders;
        private System.Windows.Forms.Button btnNewOrder;
        private Eco.Handles.ExpressionHandle ehAllCusttomers;
        private Eco.Handles.ExpressionHandle ehAllOrders;
        private System.Windows.Forms.ComboBox comboBox1;
        private Eco.Handles.ExpressionHandle ehCustomerCombo;
        private System.Windows.Forms.DataGridTableStyle dataGridTableStyle1;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn1;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn3;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnShip;
        private System.Windows.Forms.Button btnCancel;
        private Eco.Handles.VariableHandle vhWaybillNo;
        private System.Windows.Forms.TextBox tbWaybillNo;
        private System.Windows.Forms.Label label4;
        private Eco.Handles.OclVariables oclVariables1;


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      Eco.Handles.OclColumn oclColumn1 = new Eco.Handles.OclColumn();
      Eco.Handles.OclColumn oclColumn2 = new Eco.Handles.OclColumn();
      Eco.Handles.OclColumn oclColumn3 = new Eco.Handles.OclColumn();
      Eco.Handles.OclColumn oclColumn4 = new Eco.Handles.OclColumn();
      Eco.Handles.OclColumn oclColumn5 = new Eco.Handles.OclColumn();
      Eco.Handles.OclVariable oclVariable1 = new Eco.Handles.OclVariable("WaybillNo", this.vhWaybillNo);
      Eco.Handles.OclColumn oclColumn6 = new Eco.Handles.OclColumn();
      this.vhWaybillNo = new Eco.Handles.VariableHandle();
      this.rhRoot = new Eco.Handles.ReferenceHandle();
      this.EcoListActions = new Eco.WinForm.EcoListActionExtender();
      this.btnAccept = new System.Windows.Forms.Button();
      this.dgCustomers = new System.Windows.Forms.DataGrid();
      this.ehAllCusttomers = new Eco.Handles.ExpressionHandle();
      this.ehAllOrders = new Eco.Handles.ExpressionHandle();
      this.oclVariables1 = new Eco.Handles.OclVariables();
      this.btnShip = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.dgOrders = new System.Windows.Forms.DataGrid();
      this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
      this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
      this.dataGridTextBoxColumn2 = new System.Windows.Forms.DataGridTextBoxColumn();
      this.dataGridTextBoxColumn3 = new System.Windows.Forms.DataGridTextBoxColumn();
      this.EcoDragDrop = new Eco.WinForm.EcoDragDropExtender();
      this.EcoGlobalActions = new Eco.WinForm.EcoActionExtender();
      this.EcoAutoForms = new Eco.WinForm.EcoAutoFormExtender();
      this.btnCreateTestData = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.btnNewOrder = new System.Windows.Forms.Button();
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.ehCustomerCombo = new Eco.Handles.ExpressionHandle();
      this.label3 = new System.Windows.Forms.Label();
      this.tbWaybillNo = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.vhWaybillNo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgCustomers)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ehAllCusttomers)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ehAllOrders)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgOrders)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ehCustomerCombo)).BeginInit();
      this.SuspendLayout();
      // 
      // vhWaybillNo
      // 
      this.vhWaybillNo.EcoSpaceTypeName = "StateDemo.StateMachineEcoSpace";
      this.vhWaybillNo.FilterPredicateCallback = null;
      this.vhWaybillNo.InitialValues = " ";
      this.vhWaybillNo.RefName = null;
      this.vhWaybillNo.StaticValueTypeName = "System.String";
      // 
      // rhRoot
      // 
      this.rhRoot.EcoSpaceTypeName = "StateDemo.StateMachineEcoSpace";
      this.rhRoot.FilterPredicateCallback = null;
      this.rhRoot.RefName = null;
      // 
      // EcoListActions
      // 
      this.EcoListActions.DeleteQuestion = "Delete object(s)?";
      // 
      // btnAccept
      // 
      this.EcoListActions.SetActionExpression(this.btnAccept, "self.Accept");
      this.EcoListActions.SetBindingContext(this.btnAccept, this.dgCustomers);
      this.EcoListActions.SetEcoListAction(this.btnAccept, Eco.WinForm.ListAction.ExecuteAction);
      this.EcoListActions.SetEnabledOcl(this.btnAccept, "self.Accept?");
      this.btnAccept.ImageIndex = 12;
      this.btnAccept.Location = new System.Drawing.Point(224, 144);
      this.btnAccept.Name = "btnAccept";
      this.EcoListActions.SetRootHandle(this.btnAccept, this.ehAllOrders);
      this.btnAccept.Size = new System.Drawing.Size(75, 23);
      this.btnAccept.TabIndex = 10;
      this.btnAccept.Text = "Accept";
      this.btnAccept.UseCompatibleTextRendering = true;
      // 
      // dgCustomers
      // 
      this.dgCustomers.DataMember = "";
      this.dgCustomers.DataSource = this.ehAllCusttomers;
      this.dgCustomers.HeaderForeColor = System.Drawing.SystemColors.ControlText;
      this.dgCustomers.Location = new System.Drawing.Point(8, 32);
      this.dgCustomers.Name = "dgCustomers";
      this.dgCustomers.Size = new System.Drawing.Size(120, 104);
      this.dgCustomers.TabIndex = 1;
      // 
      // ehAllCusttomers
      // 
      this.ehAllCusttomers.AddDefaultProperties = false;
      oclColumn1.Expression = "self.Name";
      oclColumn1.ForceColumnToSignalNotReadOnlyInOrderToWorkAroundWPFBindingLimitation = false;
      oclColumn1.Name = "Name";
      oclColumn1.TypeName = "n/a";
      this.ehAllCusttomers.Columns.AddRange(new Eco.Handles.AbstractColumn[] {
            oclColumn1});
      this.ehAllCusttomers.Expression = "Customer.allInstances";
      this.ehAllCusttomers.FilterPredicateCallback = null;
      this.ehAllCusttomers.RefName = null;
      this.ehAllCusttomers.RootHandle = this.rhRoot;
      // 
      // ehAllOrders
      // 
      this.ehAllOrders.AddDefaultProperties = false;
      oclColumn2.Expression = "self.Customer.AsString";
      oclColumn2.ForceColumnToSignalNotReadOnlyInOrderToWorkAroundWPFBindingLimitation = false;
      oclColumn2.Name = "Customer";
      oclColumn2.TypeName = "n/a";
      oclColumn3.Expression = "self.Reference";
      oclColumn3.ForceColumnToSignalNotReadOnlyInOrderToWorkAroundWPFBindingLimitation = false;
      oclColumn3.Name = "Reference";
      oclColumn3.TypeName = "n/a";
      oclColumn4.Expression = "self.Customer";
      oclColumn4.ForceColumnToSignalNotReadOnlyInOrderToWorkAroundWPFBindingLimitation = false;
      oclColumn4.Name = "CustomerObject";
      oclColumn4.TypeName = "n/a";
      oclColumn5.Expression = "self.State";
      oclColumn5.ForceColumnToSignalNotReadOnlyInOrderToWorkAroundWPFBindingLimitation = false;
      oclColumn5.Name = "State";
      oclColumn5.TypeName = "n/a";
      this.ehAllOrders.Columns.AddRange(new Eco.Handles.AbstractColumn[] {
            oclColumn2,
            oclColumn3,
            oclColumn4,
            oclColumn5});
      this.ehAllOrders.Expression = "Order.allInstances";
      this.ehAllOrders.FilterPredicateCallback = null;
      this.ehAllOrders.RefName = null;
      this.ehAllOrders.RootHandle = this.rhRoot;
      this.ehAllOrders.Variables = this.oclVariables1;
      // 
      // oclVariables1
      // 
      oclVariable1.ElementHandle = this.vhWaybillNo;
      oclVariable1.VariableName = "WaybillNo";
      this.oclVariables1.OclVariableCollection.AddRange(new Eco.Handles.OclVariable[] {
            oclVariable1});
      // 
      // btnShip
      // 
      this.EcoListActions.SetActionExpression(this.btnShip, "Ship(WaybillNo)");
      this.EcoListActions.SetBindingContext(this.btnShip, this.dgCustomers);
      this.EcoListActions.SetEcoListAction(this.btnShip, Eco.WinForm.ListAction.ExecuteAction);
      this.EcoListActions.SetEnabledOcl(this.btnShip, "Ship?(WaybillNo)");
      this.btnShip.ImageIndex = 12;
      this.btnShip.Location = new System.Drawing.Point(193, 184);
      this.btnShip.Name = "btnShip";
      this.EcoListActions.SetRootHandle(this.btnShip, this.ehAllOrders);
      this.btnShip.Size = new System.Drawing.Size(98, 23);
      this.btnShip.TabIndex = 11;
      this.btnShip.Text = "Ship";
      this.btnShip.UseCompatibleTextRendering = true;
      // 
      // btnCancel
      // 
      this.EcoListActions.SetActionExpression(this.btnCancel, "self.Cancel");
      this.EcoListActions.SetBindingContext(this.btnCancel, this.dgOrders);
      this.EcoListActions.SetEcoListAction(this.btnCancel, Eco.WinForm.ListAction.ExecuteAction);
      this.EcoListActions.SetEnabledOcl(this.btnCancel, "self.Cancel?");
      this.btnCancel.ImageIndex = 12;
      this.btnCancel.Location = new System.Drawing.Point(312, 144);
      this.btnCancel.Name = "btnCancel";
      this.EcoListActions.SetRootHandle(this.btnCancel, this.ehAllOrders);
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 12;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseCompatibleTextRendering = true;
      // 
      // dgOrders
      // 
      this.dgOrders.DataMember = "";
      this.dgOrders.DataSource = this.ehAllOrders;
      this.EcoAutoForms.SetEcoAutoForm(this.dgOrders, true);
      this.dgOrders.HeaderForeColor = System.Drawing.SystemColors.ControlText;
      this.dgOrders.Location = new System.Drawing.Point(136, 32);
      this.dgOrders.Name = "dgOrders";
      this.dgOrders.Size = new System.Drawing.Size(280, 104);
      this.dgOrders.TabIndex = 3;
      this.dgOrders.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.dataGridTableStyle1});
      // 
      // dataGridTableStyle1
      // 
      this.dataGridTableStyle1.DataGrid = this.dgOrders;
      this.dataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.dataGridTextBoxColumn1,
            this.dataGridTextBoxColumn2,
            this.dataGridTextBoxColumn3});
      this.dataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
      // 
      // dataGridTextBoxColumn1
      // 
      this.dataGridTextBoxColumn1.Format = "";
      this.dataGridTextBoxColumn1.FormatInfo = null;
      this.dataGridTextBoxColumn1.HeaderText = "Customer";
      this.dataGridTextBoxColumn1.MappingName = "Customer";
      this.dataGridTextBoxColumn1.Width = 75;
      // 
      // dataGridTextBoxColumn2
      // 
      this.dataGridTextBoxColumn2.Format = "";
      this.dataGridTextBoxColumn2.FormatInfo = null;
      this.dataGridTextBoxColumn2.HeaderText = "Reference";
      this.dataGridTextBoxColumn2.MappingName = "Reference";
      this.dataGridTextBoxColumn2.Width = 75;
      // 
      // dataGridTextBoxColumn3
      // 
      this.dataGridTextBoxColumn3.Format = "";
      this.dataGridTextBoxColumn3.FormatInfo = null;
      this.dataGridTextBoxColumn3.HeaderText = "State";
      this.dataGridTextBoxColumn3.MappingName = "State";
      this.dataGridTextBoxColumn3.Width = 75;
      // 
      // EcoGlobalActions
      // 
      this.EcoGlobalActions.RootHandle = this.rhRoot;
      // 
      // btnCreateTestData
      // 
      this.btnCreateTestData.Location = new System.Drawing.Point(8, 144);
      this.btnCreateTestData.Name = "btnCreateTestData";
      this.btnCreateTestData.Size = new System.Drawing.Size(104, 23);
      this.btnCreateTestData.TabIndex = 0;
      this.btnCreateTestData.Text = "Create Test Data";
      this.btnCreateTestData.UseCompatibleTextRendering = true;
      this.btnCreateTestData.Click += new System.EventHandler(this.btnCreateTestData_Click);
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(8, 8);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(100, 23);
      this.label1.TabIndex = 2;
      this.label1.Text = "Customers";
      this.label1.UseCompatibleTextRendering = true;
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(136, 8);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(52, 23);
      this.label2.TabIndex = 4;
      this.label2.Text = "Orders";
      this.label2.UseCompatibleTextRendering = true;
      // 
      // btnNewOrder
      // 
      this.btnNewOrder.Location = new System.Drawing.Point(136, 144);
      this.btnNewOrder.Name = "btnNewOrder";
      this.btnNewOrder.Size = new System.Drawing.Size(72, 23);
      this.btnNewOrder.TabIndex = 7;
      this.btnNewOrder.Text = "New Order";
      this.btnNewOrder.UseCompatibleTextRendering = true;
      this.btnNewOrder.Click += new System.EventHandler(this.btnNewOrder_Click);
      // 
      // comboBox1
      // 
      this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.ehAllOrders, "CustomerObject", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
      this.comboBox1.DataSource = this.ehCustomerCombo;
      this.comboBox1.DisplayMember = "Name";
      this.comboBox1.Location = new System.Drawing.Point(424, 72);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size(80, 21);
      this.comboBox1.TabIndex = 8;
      this.comboBox1.ValueMember = "Myself";
      // 
      // ehCustomerCombo
      // 
      oclColumn6.Expression = "self";
      oclColumn6.ForceColumnToSignalNotReadOnlyInOrderToWorkAroundWPFBindingLimitation = false;
      oclColumn6.Name = "Myself";
      oclColumn6.TypeName = "n/a";
      this.ehCustomerCombo.Columns.AddRange(new Eco.Handles.AbstractColumn[] {
            oclColumn6});
      this.ehCustomerCombo.Expression = "Customer.allInstances";
      this.ehCustomerCombo.FilterPredicateCallback = null;
      this.ehCustomerCombo.NullRowMode = Eco.Handles.NullRowMode.First;
      this.ehCustomerCombo.RefName = null;
      this.ehCustomerCombo.RootHandle = this.rhRoot;
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(416, 32);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(100, 23);
      this.label3.TabIndex = 9;
      this.label3.Text = "Customer";
      this.label3.UseCompatibleTextRendering = true;
      // 
      // tbWaybillNo
      // 
      this.tbWaybillNo.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vhWaybillNo, "self", true));
      this.tbWaybillNo.Location = new System.Drawing.Point(304, 187);
      this.tbWaybillNo.Name = "tbWaybillNo";
      this.tbWaybillNo.Size = new System.Drawing.Size(100, 20);
      this.tbWaybillNo.TabIndex = 13;
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(304, 170);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(100, 14);
      this.label4.TabIndex = 14;
      this.label4.Text = "Waybill Number";
      this.label4.UseCompatibleTextRendering = true;
      // 
      // Form1
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(520, 221);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.tbWaybillNo);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnShip);
      this.Controls.Add(this.btnAccept);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.comboBox1);
      this.Controls.Add(this.btnNewOrder);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.dgOrders);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.dgCustomers);
      this.Controls.Add(this.btnCreateTestData);
      this.Location = new System.Drawing.Point(510, 410);
      this.Name = "Form1";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "WinForm";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
      ((System.ComponentModel.ISupportInitialize)(this.vhWaybillNo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgCustomers)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ehAllCusttomers)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ehAllOrders)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgOrders)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ehCustomerCombo)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

        }
		#endregion

	}
}

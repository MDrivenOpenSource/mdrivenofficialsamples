using System;
using System.Windows.Forms;
using Eco.Windows.Forms;

namespace StateDemo
{
	static class Program
	{
		private static StateDemo.StateMachineEcoSpace ecoSpace;

		/// <summary>
		/// The global EcoSpace instance.
		/// </summary>
		public static StateDemo.StateMachineEcoSpace EcoSpace
		{
			get { return ecoSpace; }
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			WinFormDequeuer.Active = true;
			Form mainForm = null;
			try
			{
				ecoSpace = new StateDemo.StateMachineEcoSpace();
				ecoSpace.Active = true;
				mainForm = new Form1(ecoSpace);

			}
			catch (Exception e)
			{
				new ThreadExceptionDialog(e).ShowDialog();
				throw;
			}
			Application.Run(mainForm);
			ecoSpace.Dispose();
		}
	}
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Eco.ViewModel.Runtime;
using Eco.Services;
using Eco.Handles;
using WECPOFLogic;

namespace EcoProject1
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
  public partial class Window1 : Window
  {
    private EcoSpace _ecoSpace;

    public Window1()
    {
      InitializeComponent();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      // The EcoSpace
      _ecoSpace = new EcoProject1EcoSpace();
      _ecoSpace.Active = true;

      // AsyncHandling
      EcoServiceHelper.GetAsyncSupportService(_ecoSpace).TurnOnAsyncHandling();
      Application.Current.Exit += new ExitEventHandler(Current_Exit);

      
      // WPF UI Settings
      System.Windows.FrameworkElement.LanguageProperty.OverrideMetadata(typeof(System.Windows.FrameworkElement), new System.Windows.FrameworkPropertyMetadata(System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentCulture.IetfLanguageTag)));//http://stackoverflow.com/questions/3630967/datetime-not-showing-with-currentculture-format-in-datagrid-listview
      System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture; // Set the user interface to display in the same culture as that set in Control Panel.

      // WECPOF Initialize
      WECPOFTabBased wecpof = new WECPOFTabBased();
      MainGrid.Children.Add(wecpof);
      wecpof.EasyInit(_ecoSpace, true, Environment.CurrentDirectory + "\\Styles", "Look2014.xaml",this);

      this.PreviewKeyDown += (keysender, keye) => 
      {
            if (Keyboard.FocusedElement==this)   // if the keyboard focused is lost - return it to something usefull
              Keyboard.Focus(wecpof.PredictFocus(FocusNavigationDirection.Left) as IInputElement); 
      };

      wecpof.OnFullScreenModeChanged += (snd, args) =>
      {
        if (wecpof.IsFullScreen)
        {
          this.WindowState = System.Windows.WindowState.Maximized;
          this.WindowStyle = System.Windows.WindowStyle.None;
        }
        else
        {
          this.WindowState = System.Windows.WindowState.Normal;
          this.WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
        }
      };
     
    }

   


    void Current_Exit(object sender, ExitEventArgs e)
    {
      EcoServiceHelper.GetAsyncSupportService(_ecoSpace).ThreadForAsyncOperations.Abort();
    }

  }
}

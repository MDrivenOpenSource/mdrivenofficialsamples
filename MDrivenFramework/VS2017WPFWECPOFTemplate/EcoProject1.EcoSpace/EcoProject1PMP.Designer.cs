namespace EcoProject1
{
    public partial class EcoProject1PMP
    {
        #region Component Designer generated code
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        private Eco.Persistence.SyncHandler syncHandler1;

        private Eco.Persistence.PersistenceMapperXml persistenceMapperXml1;
        
        private void InitializeComponent()
        {
            this.syncHandler1 = new Eco.Persistence.SyncHandler();

            this.persistenceMapperXml1 = new Eco.Persistence.PersistenceMapperXml();
            this.persistenceMapperXml1.FileName = "Data.xml";
            this.persistenceMapperXml1.SyncHandler = this.syncHandler1;
            this.PersistenceMapper = this.persistenceMapperXml1;

            //
            // PersistenceMapperProvider
            //
            this.EcoSpaceTypeName = "EcoProject1.EcoProject1EcoSpace";
        }
        #endregion
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }
    }
}

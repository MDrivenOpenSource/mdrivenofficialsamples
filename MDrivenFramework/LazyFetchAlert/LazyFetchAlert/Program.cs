namespace LazyFetchAlert
{
    using System;
    using System.Windows.Forms;
    using Eco.Windows.Forms;

    public static class Program
    {
        /// <summary>
        /// Stores the global EcoSpace instance.
        /// </summary>
        private static LazyFetchAlert.LazyFetchAlertEcoSpace ecoSpace;

        /// <summary>
        /// Gets the global EcoSpace instance.
        /// </summary>
        public static LazyFetchAlert.LazyFetchAlertEcoSpace EcoSpace
        {
            get { return ecoSpace; }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            WinFormDequeuer.Active = true;
            Form mainForm = null;
            try
            {
                ecoSpace = new LazyFetchAlert.LazyFetchAlertEcoSpace();
                ecoSpace.Active = true;
                mainForm = new Form1(ecoSpace);
            }
            catch (Exception e)
            {
                new ThreadExceptionDialog(e).ShowDialog();
                throw;
            }
            
            Application.Run(mainForm);
            ecoSpace.Dispose();
        }
    }
}

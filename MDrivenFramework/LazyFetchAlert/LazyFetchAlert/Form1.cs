namespace LazyFetchAlert
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Windows.Forms;
    using Eco.Handles;
    using Eco.ObjectRepresentation;
    using Eco.Services;
    using Eco.Windows.Forms;
    using Eco.WinForm;

    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public partial class Form1 : System.Windows.Forms.Form
    {
        public Form1(LazyFetchAlert.LazyFetchAlertEcoSpace ecoSpace)
        {
            // Required for Windows Form Designer support
            this.InitializeComponent();

            // Set EcoSpace (actually stores it in rhRoot.EcoSpace)
            // Note that you need to set the EcoSpace property for 
            // each ReferenceHandle and VariableHandle you add to 
            // the form.
            this.rhRoot.EcoSpace = ecoSpace;
            EcoSpace.FetchDetector.OnSingleFetch += new FetchAlertDelegate(WriteToLog);

        }

        public LazyFetchAlert.LazyFetchAlertEcoSpace EcoSpace
        {
            get 
            { 
                return (LazyFetchAlert.LazyFetchAlertEcoSpace)this.rhRoot.EcoSpace; 
            }
        }

        private void btnNormalForm_Click(object sender, EventArgs e)
        {
            new EcoWinForm(EcoSpace, false).Show();

        }

        private void btnOptimizedForm_Click(object sender, EventArgs e)
        {
            new EcoWinForm(EcoSpace, true).Show();
        }

        private void WriteToLog(string message)
        {
            tbxFetchLog.AppendText(message + "\r\n");
        }

    }
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Eco.Handles;
using Eco.Windows.Forms;
using Eco.WinForm;
using Eco.ObjectRepresentation;
using Eco.Services;
using Eco.AutoContainers;
using CoreClasses;

namespace LazyFetchAlert
{
	/// <summary>
	/// Summary description for EcoWinForm.
	/// </summary>
	public class EcoWinForm : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private Eco.Handles.ReferenceHandle rhRoot;
		private Eco.Handles.ExpressionHandle ehRoot;
		private Eco.WinForm.EcoActionExtender EcoGlobalActions;
		private Eco.WinForm.EcoDragDropExtender EcoDragDrop;
		private Eco.WinForm.EcoAutoFormExtender EcoAutoForms;
		private Eco.WinForm.EcoListActionExtender EcoListActions;
		private System.Windows.Forms.DataGrid dataGrid1;
		private System.Windows.Forms.Button button1;

		public EcoWinForm(Eco.Handles.EcoSpace ecoSpace, bool prefetch)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// Set EcoSpace (actually stores it in rhRoot.EcoSpace)
			this.EcoSpace = ecoSpace;

			if (prefetch)
			{
				IElement el = ehRoot.Element;
				IObjectList ol = el as IObjectList;
				IPersistenceService ps = EcoSpace.GetEcoService<IPersistenceService>();
				if (ol != null)
					ps.EnsureRelatedObjects(ol, "Home");
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose (bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.rhRoot = new Eco.Handles.ReferenceHandle();
			this.ehRoot = new Eco.Handles.ExpressionHandle();
			this.EcoListActions = new Eco.WinForm.EcoListActionExtender();
			this.EcoDragDrop = new Eco.WinForm.EcoDragDropExtender();
			this.EcoGlobalActions = new Eco.WinForm.EcoActionExtender();
			this.EcoAutoForms = new Eco.WinForm.EcoAutoFormExtender();
			this.dataGrid1 = new System.Windows.Forms.DataGrid();
			this.button1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
			this.SuspendLayout();
			// 
			// rhRoot
			// 
			this.rhRoot.EcoSpaceTypeName = "LazyFetchAlert.LazyFetchAlertEcoSpace";
			// 
			// ehRoot
			// 
			this.ehRoot.Expression = "Person.allInstances";
			this.ehRoot.RootHandle = this.rhRoot;
			// 
			// EcoGlobalActions
			// 
			this.EcoGlobalActions.RootHandle = this.rhRoot;
			// 
			// dataGrid1
			// 
			this.dataGrid1.DataMember = "";
			this.dataGrid1.DataSource = this.ehRoot;
			this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid1.Location = new System.Drawing.Point(16, 16);
			this.dataGrid1.Name = "dataGrid1";
			this.dataGrid1.Size = new System.Drawing.Size(256, 192);
			this.dataGrid1.TabIndex = 0;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(104, 232);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "New";
			this.button1.UseCompatibleTextRendering = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// EcoWinForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.dataGrid1);
			this.Name = "EcoWinForm";
			this.Text = "All persons";
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public Eco.Handles.EcoSpace EcoSpace
		{
			get { return (Eco.Handles.EcoSpace)rhRoot.EcoSpace; }
			set { rhRoot.EcoSpace = value; }
		}
		
		private void button1_Click(object sender, System.EventArgs e)
		{
			Person p = new Person(EcoSpace);
			p.Home = new Building(EcoSpace);
		}

	}
}

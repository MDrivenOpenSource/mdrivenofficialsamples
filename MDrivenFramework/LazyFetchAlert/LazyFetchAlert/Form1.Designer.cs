namespace LazyFetchAlert
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Windows.Forms;
    using Eco.Handles;
    using Eco.ObjectRepresentation;
    using Eco.Services;
    using Eco.Windows.Forms;
    using Eco.WinForm;

    public partial class Form1
    {
        private Eco.Handles.ReferenceHandle rhRoot;
        private Eco.WinForm.EcoActionExtender ecoGlobalActions;
        private Eco.WinForm.EcoDragDropExtender ecoDragDrop;
        private Eco.WinForm.EcoAutoFormExtender ecoAutoForms;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rhRoot = new Eco.Handles.ReferenceHandle();
            this.ecoDragDrop = new Eco.WinForm.EcoDragDropExtender();
            this.ecoGlobalActions = new Eco.WinForm.EcoActionExtender();
            this.ecoAutoForms = new Eco.WinForm.EcoAutoFormExtender();
            this.btnOptimizedForm = new System.Windows.Forms.Button();
            this.btnNormalForm = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxFetchLog = new System.Windows.Forms.TextBox();
            this.btnUpdateDB = new System.Windows.Forms.Button();
            this.btnActivate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).BeginInit();
            this.SuspendLayout();
            // 
            // rhRoot
            // 
            this.rhRoot.EcoSpaceTypeName = "LazyFetchAlert.LazyFetchAlertEcoSpace";
            // 
            // ecoGlobalActions
            // 
            this.ecoGlobalActions.RootHandle = this.rhRoot;
            // 
            // btnOptimizedForm
            // 
            this.btnOptimizedForm.Location = new System.Drawing.Point(150, 41);
            this.btnOptimizedForm.Name = "btnOptimizedForm";
            this.btnOptimizedForm.Size = new System.Drawing.Size(120, 23);
            this.btnOptimizedForm.TabIndex = 13;
            this.btnOptimizedForm.Text = "Open optimized form";
            this.btnOptimizedForm.UseCompatibleTextRendering = true;
            this.btnOptimizedForm.Click += new System.EventHandler(this.btnOptimizedForm_Click);
            // 
            // btnNormalForm
            // 
            this.btnNormalForm.Location = new System.Drawing.Point(150, 12);
            this.btnNormalForm.Name = "btnNormalForm";
            this.btnNormalForm.Size = new System.Drawing.Size(120, 23);
            this.btnNormalForm.TabIndex = 12;
            this.btnNormalForm.Text = "Open normal form";
            this.btnNormalForm.UseCompatibleTextRendering = true;
            this.btnNormalForm.Click += new System.EventHandler(this.btnNormalForm_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 23);
            this.label1.TabIndex = 11;
            this.label1.Text = "Log:";
            this.label1.UseCompatibleTextRendering = true;
            // 
            // tbxFetchLog
            // 
            this.tbxFetchLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxFetchLog.Location = new System.Drawing.Point(6, 116);
            this.tbxFetchLog.Multiline = true;
            this.tbxFetchLog.Name = "tbxFetchLog";
            this.tbxFetchLog.ReadOnly = true;
            this.tbxFetchLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxFetchLog.Size = new System.Drawing.Size(272, 152);
            this.tbxFetchLog.TabIndex = 10;
            // 
            // btnUpdateDB
            // 
            this.ecoGlobalActions.SetEcoAction(this.btnUpdateDB, Eco.WinForm.EcoAction.UpdateDatabase);
            this.btnUpdateDB.Location = new System.Drawing.Point(22, 41);
            this.btnUpdateDB.Name = "btnUpdateDB";
            this.btnUpdateDB.Size = new System.Drawing.Size(96, 23);
            this.btnUpdateDB.TabIndex = 9;
            this.btnUpdateDB.Text = "Update DB";
            this.btnUpdateDB.UseCompatibleTextRendering = true;
            // 
            // btnActivate
            // 
            this.btnActivate.Location = new System.Drawing.Point(22, 12);
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.Size = new System.Drawing.Size(96, 23);
            this.btnActivate.TabIndex = 8;
            this.btnActivate.Text = "Activate";
            this.btnActivate.UseCompatibleTextRendering = true;
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(284, 264);
            this.Controls.Add(this.btnOptimizedForm);
            this.Controls.Add(this.btnNormalForm);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxFetchLog);
            this.Controls.Add(this.btnUpdateDB);
            this.Controls.Add(this.btnActivate);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private Button btnOptimizedForm;
        private Button btnNormalForm;
        private Label label1;
        private TextBox tbxFetchLog;
        private Button btnUpdateDB;
        private Button btnActivate;
    }
}

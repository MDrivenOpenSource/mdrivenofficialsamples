namespace LazyFetchAlert
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Eco.Services;
    using Eco.UmlCodeAttributes;
    
    [EcoSpace]
    public partial class LazyFetchAlertEcoSpace : Eco.Handles.DefaultEcoSpace
    {
        #region Eco Managed code
        private static ITypeSystemService typeSystemProvider;
        #endregion Eco Managed code
        
        public LazyFetchAlertEcoSpace() : base()
        {
            this.InitializeComponent();

            m_FetchDetector = new FetchDetector(TypeSystem);
            m_FetchDetector.NextPersistenceHandler = FrontsidePolicy.PersistenceHandler;
            m_FetchDetector.ObjectRepresentationProvider = FrontsidePolicy.ObjectRepresentationProvider;
            FrontsidePolicy.PersistenceHandler = m_FetchDetector;
        }

        private readonly FetchDetector m_FetchDetector;
        public FetchDetector FetchDetector { get { return m_FetchDetector; } }

        /// <summary>
        /// Persist all changes to the domain objects.
        /// </summary>
        /// <remarks>
        /// This function persists all changes to the eco space, including object creation,
        /// object manipulation, changed associations and object deletions. After invoking this method
        /// all undo information is removed.
        /// If the application does not have any persistence layer defined the operation does nothing.
        /// </remarks>
        public void UpdateDatabase()
        {
            if ((Persistence != null) && (DirtyList != null))
            {
                Persistence.UpdateDatabaseWithList(DirtyList.AllDirtyObjects());
            }
        }

        #region Eco Managed code
        public static new ITypeSystemService GetTypeSystemService()
        {
            if (typeSystemProvider == null)
            {
                lock (typeof(LazyFetchAlertEcoSpace))
                {
                    if (typeSystemProvider == null)
                        typeSystemProvider = MakeTypeService(typeof(LazyFetchAlertEcoSpace));
                }
            }
            
            return typeSystemProvider;
        }
        
        protected override ITypeSystemService GetTypeSystemProvider()
        {
            return LazyFetchAlertEcoSpace.GetTypeSystemService();
        }
        #endregion

        // Add user written methods here
    }
}

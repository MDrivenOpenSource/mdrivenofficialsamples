using System;
using System.Collections;
using Eco.Framework;
using Eco.Cache;
using Eco.UmlRt;
using Eco.ObjectRepresentation;
using Eco.Persistence;
using System.Collections.Generic;

namespace LazyFetchAlert
{

  public delegate void FetchAlertDelegate(string message);


  /// <summary>
  /// Intercepts calls to an IPersistenceHandler, and logs situations where objects are
  /// fetched one at a time.
  /// </summary>
  public class FetchDetector : ChainedPersistenceHandlerBase
  {
    private IObjectRepresentationProvider m_objRepProv;
    public FetchDetector(IEcoTypeSystem typeSystem) : base(typeSystem)
    {
    }

    public override void Fetch(ICollection<Locator> objects, IStructuralFeature[] members, Eco.Framework.FetchStrategy linkFetchStrategy)
    {
      base.Fetch(objects, members, linkFetchStrategy);

      if (objects.Count == 1)
      {
        // there is no easy way to get the only element except to iterate the collection...
        foreach (Locator loc in objects)
        {
          IObject o = ObjectRepresentationProvider.IObjectForLocator(loc);
          OnSingleFetch("Fetching " + o.UmlClass.Name);
        }
      }
    }

    public IObjectRepresentationProvider ObjectRepresentationProvider
    {
      get { return m_objRepProv; }
      set { m_objRepProv = value; }
    }

    public event FetchAlertDelegate OnSingleFetch;
  }
}

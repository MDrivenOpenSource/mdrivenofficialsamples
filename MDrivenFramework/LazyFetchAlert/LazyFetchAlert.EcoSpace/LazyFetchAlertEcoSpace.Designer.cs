namespace LazyFetchAlert
{
    public partial class LazyFetchAlertEcoSpace
    {
        /// <summary>
        /// Required designer variable
        /// </summary>
        private System.ComponentModel.Container components = null;

        private Eco.Persistence.PersistenceMapperSharer persistenceMapperSharer1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Active = false;
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {

            this.persistenceMapperSharer1 = new Eco.Persistence.PersistenceMapperSharer();
            this.persistenceMapperSharer1.MapperProviderTypeName = "LazyFetchAlert.LazyFetchAlertPMP";
            this.PersistenceMapper = this.persistenceMapperSharer1;
        }

        protected CoreClasses.CoreClassesPackage IncludeEcoPackage_CoreClasses_CoreClassesPackage;
    }
}

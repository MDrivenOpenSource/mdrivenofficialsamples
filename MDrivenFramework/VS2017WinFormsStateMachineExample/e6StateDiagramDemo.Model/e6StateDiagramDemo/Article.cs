// This file contains all user-written code for the class

namespace e6StateDiagramDemo {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;

  public partial class Article
  {


      [Obsolete("This method has been removed from the model"), UmlElement(Id = "b764e303-375a-431d-90fa-b5e5e864b3ab")]
      public void Method1()
      {
          this.AsIObject().Invoke("Publish", new object[0]);
      }

      [UmlElement(Id = "4d200800-453b-4b86-8998-637b6373b9ff")]
      public void CountRejections()
      {
          RejectionCount++;
      }

      [UmlElement(Id = "420af742-e8c7-4181-a318-f17527ad63b2")]
      public void TrackHistory(string RecentHistory)
      {
          History = History + " " + RecentHistory;
      }

  }
}

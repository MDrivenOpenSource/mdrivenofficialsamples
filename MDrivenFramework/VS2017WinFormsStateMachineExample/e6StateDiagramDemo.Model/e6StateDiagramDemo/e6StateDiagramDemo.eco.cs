//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace e6StateDiagramDemo {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.CodeDom.Compiler;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Services;
  using Eco.Subscription;
  using Eco.UmlCodeAttributes;
  using Eco.UmlRt;
  
  
  [UmlElement("Package", Id="e3070e99-871c-4c00-95ac-3f0197e12731")]
  [UmlTaggedValue("Eco.ModeledName", "e6StateDiagramDemo")]
  [GeneratedCodeAttribute("ECO", "6.0.0.0")]
  [UmlMetaAttribute("ownedElement", typeof(e6StateDiagramDemo.Article))]
  public abstract partial class e6StateDiagramDemoPackage {
  }
}

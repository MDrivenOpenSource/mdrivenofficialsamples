namespace e6StateDiagramDemo
{
    public partial class e6StateDiagramDemoEcoSpace
    {
        protected e6StateDiagramDemo.e6StateDiagramDemoPackage IncludeEcoPackage_e6StateDiagramDemo_e6StateDiagramDemoPackage;
        /// <summary>
        /// Required designer variable
        /// </summary>
        private System.ComponentModel.Container components = null;

        private Eco.Persistence.PersistenceMapperSharer persistenceMapperSharer1;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Active = false;
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {

            this.persistenceMapperSharer1 = new Eco.Persistence.PersistenceMapperSharer();
            this.persistenceMapperSharer1.MapperProviderTypeName = "e6StateDiagramDemo.e6StateDiagramDemoPMP";
            this.PersistenceMapper = this.persistenceMapperSharer1;
        }
    }
}

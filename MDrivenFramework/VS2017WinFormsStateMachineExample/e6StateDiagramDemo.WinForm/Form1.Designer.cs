namespace e6StateDiagramDemo
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Windows.Forms;
    using Eco.Handles;
    using Eco.ObjectRepresentation;
    using Eco.Services;
    using Eco.Windows.Forms;
    using Eco.WinForm;

    public partial class Form1
    {
        private Eco.Handles.ReferenceHandle rhRoot;
        private Eco.WinForm.EcoActionExtender ecoGlobalActions;
        private Eco.WinForm.EcoDragDropExtender ecoDragDrop;
        private Eco.WinForm.EcoAutoFormExtender ecoAutoForms;
        private Eco.WinForm.EcoListActionExtender ecoListActions;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rhRoot = new Eco.Handles.ReferenceHandle();
            this.ecoListActions = new Eco.WinForm.EcoListActionExtender();
            this.ecoDragDrop = new Eco.WinForm.EcoDragDropExtender();
            this.ecoGlobalActions = new Eco.WinForm.EcoActionExtender();
            this.btnShowDebugger = new System.Windows.Forms.Button();
            this.ecoAutoForms = new Eco.WinForm.EcoAutoFormExtender();
            this.viewModelUserControl1 = new Eco.ViewModel.WinForms.ViewModelUserControl();
            this.viewModelUserControl2 = new Eco.ViewModel.WinForms.ViewModelUserControl();
            this.dirtyObjectCatcher1 = new DroopyEyes.EcoExtensions.WinForm.DirtyObjectCatcher();
            ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).BeginInit();
            this.SuspendLayout();
            // 
            // rhRoot
            // 
            this.rhRoot.EcoSpaceTypeName = "e6StateDiagramDemo.e6StateDiagramDemoEcoSpace";
            // 
            // ecoListActions
            // 
            this.ecoListActions.DeleteQuestion = "Delete object(s)?";
            // 
            // ecoGlobalActions
            // 
            this.ecoGlobalActions.RootHandle = this.rhRoot;
            // 
            // btnShowDebugger
            // 
            this.ecoGlobalActions.SetEcoAction(this.btnShowDebugger, Eco.WinForm.EcoAction.ShowDebugger);
            this.btnShowDebugger.Location = new System.Drawing.Point(13, 13);
            this.btnShowDebugger.Name = "btnShowDebugger";
            this.btnShowDebugger.Size = new System.Drawing.Size(75, 23);
            this.btnShowDebugger.TabIndex = 0;
            this.btnShowDebugger.Text = "Debugger";
            this.btnShowDebugger.UseVisualStyleBackColor = true;
            // 
            // viewModelUserControl1
            // 
            this.viewModelUserControl1.DefaultDataSourceUpdateMode = System.Windows.Forms.DataSourceUpdateMode.OnValidation;
            this.viewModelUserControl1.EcoSpaceTypeName = "e6StateDiagramDemo.e6StateDiagramDemoEcoSpace";
            this.viewModelUserControl1.Location = new System.Drawing.Point(12, 62);
            this.viewModelUserControl1.Name = "viewModelUserControl1";
            this.viewModelUserControl1.Preview = false;
            this.viewModelUserControl1.ReadOnly = false;
            this.viewModelUserControl1.Size = new System.Drawing.Size(500, 625);
            this.viewModelUserControl1.TabIndex = 1;
            this.viewModelUserControl1.ViewModelName = "ViewModelArticle";
            // 
            // viewModelUserControl2
            // 
            this.viewModelUserControl2.DefaultDataSourceUpdateMode = System.Windows.Forms.DataSourceUpdateMode.OnValidation;
            this.viewModelUserControl2.EcoSpaceTypeName = "e6StateDiagramDemo.e6StateDiagramDemoEcoSpace";
            this.viewModelUserControl2.Location = new System.Drawing.Point(532, 62);
            this.viewModelUserControl2.Name = "viewModelUserControl2";
            this.viewModelUserControl2.Preview = false;
            this.viewModelUserControl2.ReadOnly = false;
            this.viewModelUserControl2.Size = new System.Drawing.Size(500, 625);
            this.viewModelUserControl2.TabIndex = 2;
            this.viewModelUserControl2.ViewModelName = "ViewModelArticle";
            // 
            // dirtyObjectCatcher1
            // 
            this.dirtyObjectCatcher1.Activated += new System.EventHandler(this.dirtyObjectCatcher1_Activated);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(1285, 697);
            this.Controls.Add(this.viewModelUserControl2);
            this.Controls.Add(this.viewModelUserControl1);
            this.Controls.Add(this.btnShowDebugger);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        private Button btnShowDebugger;
        private Eco.ViewModel.WinForms.ViewModelUserControl viewModelUserControl1;
        private Eco.ViewModel.WinForms.ViewModelUserControl viewModelUserControl2;
        private DroopyEyes.EcoExtensions.WinForm.DirtyObjectCatcher dirtyObjectCatcher1;
    }
}

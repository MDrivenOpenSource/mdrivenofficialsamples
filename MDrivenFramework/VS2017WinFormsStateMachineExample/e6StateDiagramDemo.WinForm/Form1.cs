namespace e6StateDiagramDemo
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;
    using Eco.Handles;
    using Eco.Linq;
    using Eco.ObjectRepresentation;
    using Eco.Services;
    using Eco.Windows.Forms;
    using Eco.WinForm;

    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public partial class Form1 : System.Windows.Forms.Form
    {
        public Form1(e6StateDiagramDemo.e6StateDiagramDemoEcoSpace ecoSpace)
        {
            // Required for Windows Form Designer support
            this.InitializeComponent();

            // Set EcoSpace (actually stores it in rhRoot.EcoSpace)
            // Note that you need to set the EcoSpace property for 
            // each ReferenceHandle and VariableHandle you add to 
            // the form.
            this.rhRoot.EcoSpace = ecoSpace;
           
            ecoAutoForms = new Eco.WinForm.EcoAutoFormExtender(); // optional

            dirtyObjectCatcher1.Activate(this, EcoSpace);

        }

        public e6StateDiagramDemo.e6StateDiagramDemoEcoSpace EcoSpace
        {
            get 
            { 
                return (e6StateDiagramDemo.e6StateDiagramDemoEcoSpace)this.rhRoot.EcoSpace; 
            }
        }

        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                //if ((dirtyObjectCatcher1.ContainsChanges)
                //      &&  HasDirtyObjects()  
                //   
                //    )
                {
                    DialogResult UpdateDB = MessageBox.Show(
                        "Save Changes? Yes(Save and Close),No(Close), Cancel(Return to form)",
                        "Unsaved changes exist",
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Warning,
                        MessageBoxDefaultButton.Button2);
                    switch (UpdateDB)
                    {
                        case DialogResult.Yes:
                            dirtyObjectCatcher1.UpdateDatabase();
                            // continue to close 
                            break;
                        case DialogResult.No:
                            // continue to close 
                            break;
                        case DialogResult.Cancel:
                            e.Cancel = true;
                            // cancel close 
                            break;

                    }
                }
                // continue to close              
            }
        }

        private void dirtyObjectCatcher1_Activated(object sender, EventArgs e)
        {
            viewModelUserControl1.SetEcoSpace(EcoSpace);
            Article A1 = new Article(EcoSpace);
            viewModelUserControl1.SetElement(A1);

            viewModelUserControl2.SetEcoSpace(EcoSpace);
            Article A2 = new Article(EcoSpace);
            viewModelUserControl2.SetElement(A2);

            A1.Title = "Article Nunber One";
            A1.PublishGlobally = true;
            A1.AutoPublishArticles = true;

            A2.Title = "Article Nunber Two";
            A2.PublishGlobally = false;
            A2.AutoPublishArticles = false;
        }


    }
}

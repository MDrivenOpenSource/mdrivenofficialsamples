namespace e6StateDiagramDemo
{
    using System;
    using System.Windows.Forms;
    using Eco.Windows.Forms;

    public static class Program
    {
        /// <summary>
        /// Stores the global EcoSpace instance.
        /// </summary>
        private static e6StateDiagramDemo.e6StateDiagramDemoEcoSpace ecoSpace;

        /// <summary>
        /// Gets the global EcoSpace instance.
        /// </summary>
        public static e6StateDiagramDemo.e6StateDiagramDemoEcoSpace EcoSpace
        {
            get { return ecoSpace; }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            WinFormDequeuer.Active = true;
            Form mainForm = null;
            try
            {
                ecoSpace = new e6StateDiagramDemo.e6StateDiagramDemoEcoSpace();
                ecoSpace.Active = true;
                mainForm = new Form1(ecoSpace);
            }
            catch (Exception e)
            {
                new ThreadExceptionDialog(e).ShowDialog();
                throw;
            }
            Application.ThreadException+=new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);

            Application.Run(mainForm);
            ecoSpace.Dispose();
        }
        
        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
          MessageBox.Show(e.Exception.ToString(), "Unhandled Exception");
        }
    }
}

namespace ViewmodelCustomControls_Gantt
{
    public partial class ViewmodelCustomControls_GanttEcoSpace
    {
        protected ViewmodelCustomControls_Gantt.ViewmodelCustomControls_GanttPackage IncludeEcoPackage_ViewmodelCustomControls_Gantt_ViewmodelCustomControls_GanttPackage;
        /// <summary>
        /// Required designer variable
        /// </summary>
        private System.ComponentModel.Container components = null;

        private Eco.Persistence.PersistenceMapperSharer persistenceMapperSharer1;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Active = false;
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {

            this.persistenceMapperSharer1 = new Eco.Persistence.PersistenceMapperSharer();
            this.persistenceMapperSharer1.MapperProviderTypeName = "ViewmodelCustomControls_Gantt.ViewmodelCustomControls_GanttPMP";
            this.PersistenceMapper = this.persistenceMapperSharer1;
        }
    }
}

namespace ViewmodelCustomControls_Gantt
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Eco.Handles;
    using Eco.Linq;
    using Eco.Services;
    using Eco.UmlCodeAttributes;
    
    [EcoSpace]
    [UmlTaggedValue("Eco.InitializeNullableStringsToNull", "true")]
    [UmlTaggedValue("Eco.GenerateMultiplicityConstraints", "true")]
    public partial class ViewmodelCustomControls_GanttEcoSpace : Eco.Handles.DefaultEcoSpace
    {
        #region Eco Managed code
        private static ITypeSystemService typeSystemProvider;
        #endregion Eco Managed code
        
        public ViewmodelCustomControls_GanttEcoSpace() : base()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Persist all changes to the domain objects.
        /// </summary>
        /// <remarks>
        /// This function persists all changes to the eco space, including object creation,
        /// object manipulation, changed associations and object deletions. After invoking this method
        /// all undo information is removed.
        /// If the application does not have any persistence layer defined the operation does nothing.
        /// </remarks>
        public void UpdateDatabase()
        {
            if ((Persistence != null) && (DirtyList != null))
            {
                Persistence.UpdateDatabaseWithList(DirtyList.AllDirtyObjects());
            }
        }

        #region Eco Managed code
        public static new ITypeSystemService GetTypeSystemService()
        {
            if (typeSystemProvider == null)
            {
                lock (typeof(ViewmodelCustomControls_GanttEcoSpace))
                {
                    if (typeSystemProvider == null)
                        typeSystemProvider = MakeTypeService(typeof(ViewmodelCustomControls_GanttEcoSpace));
                }
            }
            
            return typeSystemProvider;
        }
        
        protected override ITypeSystemService GetTypeSystemProvider()
        {
            return ViewmodelCustomControls_GanttEcoSpace.GetTypeSystemService();
        }
        #endregion

        // Add user written methods here
    }
}

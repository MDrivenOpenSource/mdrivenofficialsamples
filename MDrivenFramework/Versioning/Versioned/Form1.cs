using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using Eco.Handles;
using Eco.Linq;
using Eco.ObjectRepresentation;
using Eco.Services;
using Eco.Windows.Forms;
using Eco.WinForm;
using Eco.Persistence;

using Versioned;


namespace Versioned
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public partial class Form1 : System.Windows.Forms.Form
    {
#region Unimportant
        public Form1(Versioned.VersionedEcoSpace ecoSpace)
        {
            // Required for Windows Form Designer support
            this.InitializeComponent();

            // Set EcoSpace (actually stores it in rhRoot.EcoSpace)
            // Note that you need to set the EcoSpace property for 
            // each ReferenceHandle and VariableHandle you add to 
            // the form.
            this.rhRoot.EcoSpace = ecoSpace;
            this.rhPastPeople.EcoSpace = ecoSpace;
        }

        public Versioned.VersionedEcoSpace EcoSpace
        {
            get 
            { 
                return (Versioned.VersionedEcoSpace)this.rhRoot.EcoSpace; 
            }
        }
#endregion


        /// This method simply tries to make sure that we can retrieve a list of ECO Past Versions
        /// when data is changed.
        ///  
        /// This will call the appropriate services to retrieve a list of objects and then set that
        /// list into a ReferenceHandle for us to use in the right hand grids.
        private void cmhCurrentPerson_ListChanged(object sender, ListChangedEventArgs e)
        {
            Console.WriteLine("cmhCurrentPerson_ListChanged");

            if (EcoSpace != null)
            {
                IPersistenceService ps = EcoSpace.GetEcoService<IPersistenceService>();
                IVersionService vs = EcoSpace.GetEcoService<IVersionService>();

                if ((cmhCurrentPerson.Element != null) && (cmhCurrentPerson.Element.AsObject != null))
                {
                    Person person = cmhCurrentPerson.Element.GetValue<Person>();
                    bool bReadOnly = person.AsIObject().ReadOnly;
                    int iCount = person.AsIObject().Count;
                    string s = person.Name;

                    IObjectInstance oiPerson = person.AsIObject();
                    int currentVersion = vs.ElementVersion(oiPerson);

                    if (currentVersion > 1)
                    {
                        AbstractCondition condition = vs.GetChangePointCondition(oiPerson, 0, currentVersion - 1);

                        IObjectList allVersions = ps.GetAllWithCondition(condition);

                        /// This following section was intended to remove the instance representing the current version from the list of items
                        /// so that you could see a historic list of only the previous instances/revisions.
                        IModifiableVariableList mvl = EcoSpace.VariableFactory.CreateVariableList();
                        mvl.Add("vObj", oiPerson);

                        IElement el = EcoSpace.Ocl.Evaluate(allVersions, "self->excluding(vObj)", mvl);
                        rhPastPeople.SetElement(el); /// Set the rhPastPeople here, which should then satisfy things for the
                        /// evaluation of the DeriveValue event handler, surely?
                    }
                }
            }
        }

        private void cmhCurrentPerson_DeriveValue(object sender, DeriveEventArgs e)
        {
            Console.WriteLine("cmhCurrentPerson_DeriveValue");
        }

        /// This method is being used by both ehCurrentPeople and ehPastPeople to help identify the version
        /// of the database in which this instance exists or was in existance.
        /// 
        /// You may notice that Current Version's are always of value Int32.MaxValue
        private void ehPastPeople_DeriveValue(object sender, DeriveEventArgs e)
        {
            if (EcoSpace != null)
            {
                switch (e.Name)
                {
                    case "Version":
                        //ps = EcoSpace.GetEcoService<IPersistenceService>();
                        var vs = EcoSpace.GetEcoService<IVersionService>();
                        if (vs != null)
                        {
                            int version = vs.ElementVersion(e.RootElement);

                            e.ResultElement = EcoSpace.VariableFactory.CreateVariable(typeof(int));
                            e.ResultElement.AsObject = version;
                        }

                        break;

                    /// Just an example of how to return a string in a derived situation
                    case "New1":
                        e.ResultElement = EcoSpace.VariableFactory.CreateVariable(typeof(string));
                        e.ResultElement.AsObject = "Something";
                        break;
                }
            }
        }
    }
}
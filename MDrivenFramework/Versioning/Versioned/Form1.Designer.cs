namespace Versioned
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Windows.Forms;
    using Eco.Handles;
    using Eco.ObjectRepresentation;
    using Eco.Services;
    using Eco.Windows.Forms;
    using Eco.WinForm;

    public partial class Form1
    {
        private Eco.Handles.ReferenceHandle rhRoot;
        private Eco.WinForm.EcoActionExtender ecoGlobalActions;
        private Eco.WinForm.EcoDragDropExtender ecoDragDrop;
        private Eco.WinForm.EcoAutoFormExtender ecoAutoForms;
        private Eco.WinForm.EcoListActionExtender ecoListActions;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Eco.Handles.EventDerivedColumn eventDerivedColumn1 = new Eco.Handles.EventDerivedColumn();
            Eco.Handles.OclColumn oclColumn1 = new Eco.Handles.OclColumn();
            Eco.Handles.OclColumn oclColumn2 = new Eco.Handles.OclColumn();
            Eco.Handles.EventDerivedColumn eventDerivedColumn2 = new Eco.Handles.EventDerivedColumn();
            Eco.Handles.EventDerivedColumn eventDerivedColumn3 = new Eco.Handles.EventDerivedColumn();
            this.rhRoot = new Eco.Handles.ReferenceHandle();
            this.ecoListActions = new Eco.WinForm.EcoListActionExtender();
            this.btnChildrenAdd = new System.Windows.Forms.Button();
            this.ehCurrentChildren = new Eco.Handles.ExpressionHandle();
            this.cmhCurrentPerson = new Eco.Handles.CurrencyManagerHandle();
            this.dgvCurrentPeople = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCurrentPeopleNew1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ehCurrentPeople = new Eco.Handles.ExpressionHandle();
            this.btnChildrenDelete = new System.Windows.Forms.Button();
            this.cmhCurrentChild = new Eco.Handles.CurrencyManagerHandle();
            this.dgvCurrentChildren = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnPersonAdd = new System.Windows.Forms.Button();
            this.ecoDragDrop = new Eco.WinForm.EcoDragDropExtender();
            this.ecoGlobalActions = new Eco.WinForm.EcoActionExtender();
            this.btnShowDebugger = new System.Windows.Forms.Button();
            this.btnUpdateDb = new System.Windows.Forms.Button();
            this.ecoAutoForms = new Eco.WinForm.EcoAutoFormExtender();
            this.dgvPastPeople = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtbcPastPeopleVersionNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ehPastPeople = new Eco.Handles.ExpressionHandle();
            this.rhPastPeople = new Eco.Handles.ReferenceHandle();
            this.dgvPastChildren = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ehPastChildren = new Eco.Handles.ExpressionHandle();
            this.cmhCurrentPastPerson = new Eco.Handles.CurrencyManagerHandle();
            this.lbPastVersions = new System.Windows.Forms.Label();
            this.lbChildrenPastAssociations = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ehCurrentChildren)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmhCurrentPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrentPeople)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ehCurrentPeople)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmhCurrentChild)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrentChildren)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPastPeople)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ehPastPeople)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rhPastPeople)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPastChildren)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ehPastChildren)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmhCurrentPastPerson)).BeginInit();
            this.SuspendLayout();
            // 
            // rhRoot
            // 
            this.rhRoot.EcoSpaceTypeName = "Versioned.VersionedEcoSpace";
            this.rhRoot.RefName = null;
            // 
            // ecoListActions
            // 
            this.ecoListActions.DeleteQuestion = "Delete object(s)?";
            // 
            // btnChildrenAdd
            // 
            this.ecoListActions.SetEcoListAction(this.btnChildrenAdd, Eco.WinForm.ListAction.Add);
            this.btnChildrenAdd.ImageIndex = 1;
            this.btnChildrenAdd.Location = new System.Drawing.Point(13, 246);
            this.btnChildrenAdd.Name = "btnChildrenAdd";
            this.ecoListActions.SetRootHandle(this.btnChildrenAdd, this.ehCurrentChildren);
            this.btnChildrenAdd.Size = new System.Drawing.Size(75, 23);
            this.btnChildrenAdd.TabIndex = 7;
            this.btnChildrenAdd.Text = "Add Child";
            this.btnChildrenAdd.UseVisualStyleBackColor = true;
            // 
            // ehCurrentChildren
            // 
            this.ehCurrentChildren.Expression = "self.Children";
            this.ehCurrentChildren.RefName = null;
            this.ehCurrentChildren.RootHandle = this.cmhCurrentPerson;
            // 
            // cmhCurrentPerson
            // 
            this.cmhCurrentPerson.BindingContext = this.dgvCurrentPeople;
            this.cmhCurrentPerson.RefName = null;
            this.cmhCurrentPerson.RootHandle = this.ehCurrentPeople;
            this.cmhCurrentPerson.DeriveValue += new Eco.Handles.DeriveEventHandler(this.cmhCurrentPerson_DeriveValue);
            this.cmhCurrentPerson.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.cmhCurrentPerson_ListChanged);
            // 
            // dgvCurrentPeople
            // 
            this.dgvCurrentPeople.AutoGenerateColumns = false;
            this.dgvCurrentPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCurrentPeople.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.dgvCurrentPeopleNew1});
            this.dgvCurrentPeople.DataSource = this.ehCurrentPeople;
            this.dgvCurrentPeople.Location = new System.Drawing.Point(12, 41);
            this.dgvCurrentPeople.Name = "dgvCurrentPeople";
            this.dgvCurrentPeople.Size = new System.Drawing.Size(366, 195);
            this.dgvCurrentPeople.TabIndex = 1;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.Width = 200;
            // 
            // dgvCurrentPeopleNew1
            // 
            this.dgvCurrentPeopleNew1.DataPropertyName = "Version";
            this.dgvCurrentPeopleNew1.HeaderText = "Current Version";
            this.dgvCurrentPeopleNew1.Name = "dgvCurrentPeopleNew1";
            this.dgvCurrentPeopleNew1.ReadOnly = true;
            this.dgvCurrentPeopleNew1.Width = 120;
            // 
            // ehCurrentPeople
            // 
            eventDerivedColumn1.ForceColumnToSignalNotReadOnlyInOrderToWorkAroundWPFBindingLimitation = false;
            eventDerivedColumn1.Name = "Version";
            eventDerivedColumn1.TypeName = "System.Int32";
            this.ehCurrentPeople.Columns.AddRange(new Eco.Handles.AbstractColumn[] {
            eventDerivedColumn1});
            this.ehCurrentPeople.Expression = "Person.allInstances";
            this.ehCurrentPeople.RefName = null;
            this.ehCurrentPeople.RootHandle = this.rhRoot;
            this.ehCurrentPeople.DeriveValue += new Eco.Handles.DeriveEventHandler(this.ehPastPeople_DeriveValue);
            // 
            // btnChildrenDelete
            // 
            this.ecoListActions.SetEcoListAction(this.btnChildrenDelete, Eco.WinForm.ListAction.Delete);
            this.btnChildrenDelete.ImageIndex = 2;
            this.btnChildrenDelete.Location = new System.Drawing.Point(95, 246);
            this.btnChildrenDelete.Name = "btnChildrenDelete";
            this.ecoListActions.SetRootHandle(this.btnChildrenDelete, this.cmhCurrentChild);
            this.btnChildrenDelete.Size = new System.Drawing.Size(75, 23);
            this.btnChildrenDelete.TabIndex = 8;
            this.btnChildrenDelete.Text = "Delete";
            this.btnChildrenDelete.UseVisualStyleBackColor = true;
            // 
            // cmhCurrentChild
            // 
            this.cmhCurrentChild.BindingContext = this.dgvCurrentChildren;
            this.cmhCurrentChild.RefName = null;
            this.cmhCurrentChild.RootHandle = this.ehCurrentChildren;
            // 
            // dgvCurrentChildren
            // 
            this.dgvCurrentChildren.AutoGenerateColumns = false;
            this.dgvCurrentChildren.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCurrentChildren.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn1,
            this.personDataGridViewTextBoxColumn});
            this.dgvCurrentChildren.DataSource = this.ehCurrentChildren;
            this.dgvCurrentChildren.Location = new System.Drawing.Point(12, 275);
            this.dgvCurrentChildren.Name = "dgvCurrentChildren";
            this.dgvCurrentChildren.Size = new System.Drawing.Size(366, 217);
            this.dgvCurrentChildren.TabIndex = 2;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.Width = 180;
            // 
            // personDataGridViewTextBoxColumn
            // 
            this.personDataGridViewTextBoxColumn.DataPropertyName = "Person";
            this.personDataGridViewTextBoxColumn.HeaderText = "Person";
            this.personDataGridViewTextBoxColumn.Name = "personDataGridViewTextBoxColumn";
            this.personDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // btnPersonAdd
            // 
            this.ecoListActions.SetEcoListAction(this.btnPersonAdd, Eco.WinForm.ListAction.Add);
            this.btnPersonAdd.ImageIndex = 1;
            this.btnPersonAdd.Location = new System.Drawing.Point(95, 12);
            this.btnPersonAdd.Name = "btnPersonAdd";
            this.ecoListActions.SetRootHandle(this.btnPersonAdd, this.ehCurrentPeople);
            this.btnPersonAdd.Size = new System.Drawing.Size(75, 23);
            this.btnPersonAdd.TabIndex = 9;
            this.btnPersonAdd.Text = "Add Person";
            this.btnPersonAdd.UseVisualStyleBackColor = true;
            // 
            // ecoGlobalActions
            // 
            this.ecoGlobalActions.RootHandle = this.rhRoot;
            // 
            // btnShowDebugger
            // 
            this.ecoGlobalActions.SetEcoAction(this.btnShowDebugger, Eco.WinForm.EcoAction.ShowDebugger);
            this.btnShowDebugger.Location = new System.Drawing.Point(12, 12);
            this.btnShowDebugger.Name = "btnShowDebugger";
            this.btnShowDebugger.Size = new System.Drawing.Size(75, 23);
            this.btnShowDebugger.TabIndex = 0;
            this.btnShowDebugger.Text = "Debugger";
            this.btnShowDebugger.UseVisualStyleBackColor = true;
            // 
            // btnUpdateDb
            // 
            this.ecoGlobalActions.SetEcoAction(this.btnUpdateDb, Eco.WinForm.EcoAction.UpdateDatabase);
            this.btnUpdateDb.Location = new System.Drawing.Point(177, 12);
            this.btnUpdateDb.Name = "btnUpdateDb";
            this.btnUpdateDb.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateDb.TabIndex = 10;
            this.btnUpdateDb.Text = "Update Db";
            this.btnUpdateDb.UseVisualStyleBackColor = true;
            // 
            // dgvPastPeople
            // 
            this.dgvPastPeople.AutoGenerateColumns = false;
            this.dgvPastPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPastPeople.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn2,
            this.dgvtbcPastPeopleVersionNo});
            this.dgvPastPeople.DataSource = this.ehPastPeople;
            this.dgvPastPeople.Location = new System.Drawing.Point(384, 41);
            this.dgvPastPeople.Name = "dgvPastPeople";
            this.dgvPastPeople.Size = new System.Drawing.Size(382, 195);
            this.dgvPastPeople.TabIndex = 3;
            // 
            // nameDataGridViewTextBoxColumn2
            // 
            this.nameDataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn2.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn2.Name = "nameDataGridViewTextBoxColumn2";
            this.nameDataGridViewTextBoxColumn2.Width = 200;
            // 
            // dgvtbcPastPeopleVersionNo
            // 
            this.dgvtbcPastPeopleVersionNo.DataPropertyName = "Version";
            this.dgvtbcPastPeopleVersionNo.HeaderText = "Version No";
            this.dgvtbcPastPeopleVersionNo.Name = "dgvtbcPastPeopleVersionNo";
            this.dgvtbcPastPeopleVersionNo.ReadOnly = true;
            // 
            // ehPastPeople
            // 
            this.ehPastPeople.AddDefaultProperties = false;
            oclColumn1.Expression = "self.Name";
            oclColumn1.ForceColumnToSignalNotReadOnlyInOrderToWorkAroundWPFBindingLimitation = false;
            oclColumn1.Name = "Name";
            oclColumn1.TypeName = "n/a";
            oclColumn2.Expression = "self.Children";
            oclColumn2.ForceColumnToSignalNotReadOnlyInOrderToWorkAroundWPFBindingLimitation = false;
            oclColumn2.Name = "Children";
            oclColumn2.Nested = true;
            oclColumn2.TypeName = "n/a";
            eventDerivedColumn2.ForceColumnToSignalNotReadOnlyInOrderToWorkAroundWPFBindingLimitation = false;
            eventDerivedColumn2.IsReadOnly = true;
            eventDerivedColumn2.Name = "Version";
            eventDerivedColumn2.TypeName = "System.Int32";
            eventDerivedColumn3.ForceColumnToSignalNotReadOnlyInOrderToWorkAroundWPFBindingLimitation = false;
            eventDerivedColumn3.Name = "New1";
            this.ehPastPeople.Columns.AddRange(new Eco.Handles.AbstractColumn[] {
            oclColumn1,
            oclColumn2,
            eventDerivedColumn2,
            eventDerivedColumn3});
            this.ehPastPeople.Expression = "self";
            this.ehPastPeople.RefName = null;
            this.ehPastPeople.RootHandle = this.rhPastPeople;
            this.ehPastPeople.DeriveValue += new Eco.Handles.DeriveEventHandler(this.ehPastPeople_DeriveValue);
            // 
            // rhPastPeople
            // 
            this.rhPastPeople.EcoSpaceTypeName = "Versioned.VersionedEcoSpace";
            this.rhPastPeople.RefName = null;
            this.rhPastPeople.StaticValueTypeName = "Collection(Person)";
            // 
            // dgvPastChildren
            // 
            this.dgvPastChildren.AutoGenerateColumns = false;
            this.dgvPastChildren.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPastChildren.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn3,
            this.personDataGridViewTextBoxColumn1});
            this.dgvPastChildren.DataSource = this.ehPastChildren;
            this.dgvPastChildren.Location = new System.Drawing.Point(384, 275);
            this.dgvPastChildren.Name = "dgvPastChildren";
            this.dgvPastChildren.ReadOnly = true;
            this.dgvPastChildren.Size = new System.Drawing.Size(382, 217);
            this.dgvPastChildren.TabIndex = 4;
            // 
            // nameDataGridViewTextBoxColumn3
            // 
            this.nameDataGridViewTextBoxColumn3.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn3.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn3.Name = "nameDataGridViewTextBoxColumn3";
            this.nameDataGridViewTextBoxColumn3.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn3.Width = 180;
            // 
            // personDataGridViewTextBoxColumn1
            // 
            this.personDataGridViewTextBoxColumn1.DataPropertyName = "Person";
            this.personDataGridViewTextBoxColumn1.HeaderText = "Person";
            this.personDataGridViewTextBoxColumn1.Name = "personDataGridViewTextBoxColumn1";
            this.personDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // ehPastChildren
            // 
            this.ehPastChildren.Expression = "self.Children";
            this.ehPastChildren.RefName = null;
            this.ehPastChildren.RootHandle = this.cmhCurrentPastPerson;
            // 
            // cmhCurrentPastPerson
            // 
            this.cmhCurrentPastPerson.BindingContext = this.dgvPastPeople;
            this.cmhCurrentPastPerson.RefName = null;
            this.cmhCurrentPastPerson.RootHandle = this.ehPastPeople;
            // 
            // lbPastVersions
            // 
            this.lbPastVersions.AutoSize = true;
            this.lbPastVersions.Location = new System.Drawing.Point(384, 22);
            this.lbPastVersions.Name = "lbPastVersions";
            this.lbPastVersions.Size = new System.Drawing.Size(148, 13);
            this.lbPastVersions.TabIndex = 5;
            this.lbPastVersions.Text = "Current Object - Past Versions";
            // 
            // lbChildrenPastAssociations
            // 
            this.lbChildrenPastAssociations.AutoSize = true;
            this.lbChildrenPastAssociations.Location = new System.Drawing.Point(387, 256);
            this.lbChildrenPastAssociations.Name = "lbChildrenPastAssociations";
            this.lbChildrenPastAssociations.Size = new System.Drawing.Size(146, 13);
            this.lbChildrenPastAssociations.TabIndex = 6;
            this.lbChildrenPastAssociations.Text = "Current Object - Past Children";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "VersionNo";
            this.dataGridViewTextBoxColumn1.HeaderText = "Version No";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "New1";
            this.dataGridViewTextBoxColumn2.HeaderText = "New1";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "New1";
            this.dataGridViewTextBoxColumn3.HeaderText = "New1";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "New1";
            this.dataGridViewTextBoxColumn4.HeaderText = "New1";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "New1";
            this.dataGridViewTextBoxColumn5.HeaderText = "New1";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "New1";
            this.dataGridViewTextBoxColumn6.HeaderText = "New1";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(788, 504);
            this.Controls.Add(this.btnUpdateDb);
            this.Controls.Add(this.btnPersonAdd);
            this.Controls.Add(this.btnChildrenDelete);
            this.Controls.Add(this.btnChildrenAdd);
            this.Controls.Add(this.lbChildrenPastAssociations);
            this.Controls.Add(this.lbPastVersions);
            this.Controls.Add(this.dgvPastChildren);
            this.Controls.Add(this.dgvPastPeople);
            this.Controls.Add(this.dgvCurrentChildren);
            this.Controls.Add(this.dgvCurrentPeople);
            this.Controls.Add(this.btnShowDebugger);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ehCurrentChildren)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmhCurrentPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrentPeople)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ehCurrentPeople)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmhCurrentChild)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrentChildren)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPastPeople)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ehPastPeople)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rhPastPeople)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPastChildren)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ehPastChildren)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmhCurrentPastPerson)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private Button btnShowDebugger;
        private ExpressionHandle ehCurrentPeople;
        private DataGridView dgvCurrentPeople;
        private DataGridView dgvCurrentChildren;
        private CurrencyManagerHandle cmhCurrentPerson;
        private ExpressionHandle ehCurrentChildren;
        private DataGridView dgvPastPeople;
        private DataGridView dgvPastChildren;
        private Label lbPastVersions;
        private Label lbChildrenPastAssociations;
        private Button btnChildrenAdd;
        private Button btnChildrenDelete;
        private CurrencyManagerHandle cmhCurrentChild;
        private Button btnPersonAdd;
        private ReferenceHandle rhPastPeople;
        private CurrencyManagerHandle cmhCurrentPastPerson;
        private ExpressionHandle ehPastChildren;
        private DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn personDataGridViewTextBoxColumn;
        private Button btnUpdateDb;
        private DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn personDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private ExpressionHandle ehPastPeople;
        private DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dgvtbcPastPeopleVersionNo;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn dgvCurrentPeopleNew1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    }
}

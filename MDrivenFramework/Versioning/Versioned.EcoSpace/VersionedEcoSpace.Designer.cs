namespace Versioned
{
    public partial class VersionedEcoSpace
    {
        /// <summary>
        /// Required designer variable
        /// </summary>
        private System.ComponentModel.Container components = null;

        private Eco.Persistence.PersistenceMapperSharer persistenceMapperSharer1;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UpdateDatabase();

                Active = false;
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {

            this.persistenceMapperSharer1 = new Eco.Persistence.PersistenceMapperSharer();
            this.persistenceMapperSharer1.MapperProviderTypeName = "Versioned.VersionedPMP";
            this.PersistenceMapper = this.persistenceMapperSharer1;
        }

        protected VersionedPackage IncludeEcoPackage_Versioned_VersionedPackage;
    }
}

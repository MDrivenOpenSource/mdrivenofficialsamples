﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Eco.WPF;
using Eco.ViewModel.Runtime;
using Eco.Handles;
using System.IO;
using Eco.ObjectRepresentation;

namespace HockeyCont
{
  /// <summary>
  /// Interaction logic for Window2.xaml
  /// </summary>
  public partial class Window2 : Window
  {
    private EcoSpace _es;
    public Window2()
    {
      _es = new HockeyContEcoSpace { Active = true };
      ViewModelDefinitionsInApplication.Init(_es);
      DataContext = _es;
      WPFDequeuer.Active = true;
      InitializeComponent();
      (Resources["VM1"] as Eco.ViewModel.WPF.ViewModelContent).SetEcoSpace(_es);

      Game game = CreateSomeTestData();
      (Resources["VM1"] as Eco.ViewModel.WPF.ViewModelContent).RootObject = game;



      BaseForHockeySample.OnBringUpWindow += new EventHandler<OnBringUpWindowArgs>(BaseForHockeySample_OnBringUpWindow);

    }

    void BaseForHockeySample_OnBringUpWindow(object sender, OnBringUpWindowArgs e)
    {
      if (e.WindowToOpen == "GameDay")
      {
        WindowGameDay aWindowGameDay = new WindowGameDay();
        aWindowGameDay.SetEcoSpace(_es, (Resources["VM1"] as Eco.ViewModel.WPF.ViewModelContent).RootObject as Game);
        aWindowGameDay.Show();
      }
    }

    private Game CreateSomeTestData()
    {
      // Game types
      var gtboys15 = new GameType(_es) { Name = "15 years, Boys" };
      var gtboys16 = new GameType(_es) { Name = "16 years, Boys" };
      var gtgirls15 = new GameType(_es) { Name = "15 years, Girls" };

      // team types
      var ttb15 = new TeamType(_es) { Name = "Boys 15 years" };
      var ttb16 = new TeamType(_es) { Name = "Boys 16 years" };
      var ttg15 = new TeamType(_es) { Name = "Girls 15 years" };

      // Valid team-game combinations
      gtgirls15.TeamTypes.Add(ttg15);

      gtboys15.TeamTypes.Add(ttb15);
      gtboys15.TeamTypes.Add(ttg15); // girls can play in boys 15 year
      gtboys16.TeamTypes.Add(ttb15); // 15 year boys can enter 16 year games 
      gtboys16.TeamTypes.Add(ttb16);
      gtboys16.TeamTypes.Add(ttg15); // girls can play in boys 16 year



      Team t1 = new Team(_es) { Name = "Brynäs", Image = GetImage(imagebrynäs), TeamType = ttb15 };
      Team t2 = new Team(_es) { Name = "Brynäs", Image = GetImage(imagebrynäs), TeamType = ttb16 };
      Team t3 = new Team(_es) { Name = "Brynäs", Image = GetImage(imagebrynäs), TeamType = ttg15 };

      Team t4 = new Team(_es) { Name = "Luleå", Image = GetImage(imageluleå), TeamType = ttb15 };
      Team t5 = new Team(_es) { Name = "Luleå", Image = GetImage(imageluleå), TeamType = ttb16 };
      Team t6 = new Team(_es) { Name = "Luleå", Image = GetImage(imageluleå), TeamType = ttg15 };

      Team t7 = new Team(_es) { Name = "Djurgården", Image = GetImage(imagedjurgården), TeamType = ttb15 };
      Team t8 = new Team(_es) { Name = "Djurgården", Image = GetImage(imagedjurgården), TeamType = ttb16 };
      Team t9 = new Team(_es) { Name = "Djurgården", Image = GetImage(imagedjurgården), TeamType = ttg15 };

      DoRandomPlayers(t1);
      DoRandomPlayers(t2);
      DoRandomPlayers(t3);
      DoRandomPlayers(t4);
      DoRandomPlayers(t5);
      DoRandomPlayers(t6);
      DoRandomPlayers(t7);
      DoRandomPlayers(t8);
      DoRandomPlayers(t9);

      return new Game(_es)
      {
        ScheduledDate = DateTime.Now
      };

    }

    public string[] _playerlastname = new string[10] { "Sawchuk", "Shore", "Gretzky", "Orr", "Howe", "Lemieux", "Richard", "Harvey", "Beliveau", "Hull" };
    public string[] _playerfirstname = new string[10] { "Jordan", "Alexis", "Angel", "Riley", "Peyton", "Jean", "Taylor", "Terry", "Cameron", "Kim" };

    private void DoRandomPlayers(Team team)
    {
      Random r = new Random();


      for (int i = 0; i < 15; i++)
      {
        Player p = new Player(_es) { Name = _playerfirstname[r.Next(9)] + " " + _playerlastname[r.Next(9)] };
        team.Players.Add(p);
        System.Diagnostics.Trace.WriteLine(p.Name);
      }

      for (int i = 0; i < 3; i++)
      {
        LineUp lu = new LineUp(_es);
        team.LineUps.Add(lu);
        lu.Name = "Line up " + (i + 1).ToString();
        lu.CenterForward = team.Players[r.Next(15)];
        lu.RightForward = team.Players[r.Next(15)];
        lu.LeftForward = team.Players[r.Next(15)];
        lu.RightDefence = team.Players[r.Next(15)];
        lu.LeftDefence = team.Players[r.Next(15)];
      }

      team.Keeper = team.Players[r.Next(15)];

    }

    private byte[] GetImage(Image img)
    {
      MemoryStream memStream = new MemoryStream();
      JpegBitmapEncoder encoder = new JpegBitmapEncoder();
      encoder.Frames.Add(BitmapFrame.Create(img.Source as BitmapFrame));
      encoder.Save(memStream);
      return memStream.ToArray();
    }

    private void ButtonStartGame_Click(object sender, RoutedEventArgs e)
    {
      if ((Resources["VM1"] as Eco.ViewModel.WPF.ViewModelContent).RootObject.GetValue<Game>() != null)
        (Resources["VM1"] as Eco.ViewModel.WPF.ViewModelContent).RootObject.GetValue<Game>().StartGame();
    }


  }

  public class ImageBlobConverter : IValueConverter
  {

    #region IValueConverter Members

    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      byte[] bytes = (byte[])value;
      if (bytes.Length == 0)
        return null;
      MemoryStream stream = new MemoryStream(bytes);

      BitmapImage image = new BitmapImage();
      image.BeginInit();
      image.StreamSource = stream;
      image.EndInit();

      return image;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    #endregion
  }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Eco.Handles;
using System.Windows.Media.Animation;

namespace HockeyCont
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class WindowGameDay : Window
	{
		public WindowGameDay()
		{
			this.InitializeComponent();

			// Insert code required on object creation below this point.
		}

        private EcoSpace _es;
        internal void SetEcoSpace(EcoSpace es, Game aGame)
        {
            _es = es;
            (Resources["VM1"] as Eco.ViewModel.WPF.ViewModelContent).SetEcoSpace(_es);
            (Resources["VM1"] as Eco.ViewModel.WPF.ViewModelContent).RootObject = aGame;
            (Resources["VM1"] as Eco.ViewModel.WPF.ViewModelContent).GetCollectionView("LineUp").MoveCurrentToFirst();
            (Resources["VM1"] as Eco.ViewModel.WPF.ViewModelContent).GetCollectionView("LineUpVisitor").MoveCurrentToFirst();
            LayoutRoot.DataContext = Resources["VM1"];

        }

        private bool _firsttime = true;
        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Storyboard sb=(Resources["StoryboardAnimateHomeTeam"] as Storyboard);
            
            if (_firsttime)
            {
                _firsttime = false;
                sb.RepeatBehavior = RepeatBehavior.Forever;
                sb.AutoReverse = true;
                sb.Begin(this, true);
                
            }
            else
            {
               
            }
        }
	}
}
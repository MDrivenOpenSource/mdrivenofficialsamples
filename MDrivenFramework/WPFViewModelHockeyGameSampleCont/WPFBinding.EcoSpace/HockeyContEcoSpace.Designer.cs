namespace HockeyCont
{
  public partial class HockeyContEcoSpace
  {
    protected HockeyCont.HockeyContPackage IncludeEcoPackage_WPFBinding_WPFBindingPackage;
    /// <summary>
    /// Required designer variable
    /// </summary>
    private System.ComponentModel.Container components = null;
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        Active = false;
        if (this.components != null)
        {
          this.components.Dispose();
        }
      }

      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      var persistenceMapperMemory1 = new Eco.Persistence.PersistenceMapperMemory();
      // 
      // HockeyContPMP
      // 
      this.PersistenceMapper = persistenceMapperMemory1;

    }


  }
}

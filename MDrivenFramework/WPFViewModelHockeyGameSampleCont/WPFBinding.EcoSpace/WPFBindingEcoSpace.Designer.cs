namespace WPFBinding
{
    public partial class WPFBindingEcoSpace
    {
        protected WPFBinding.WPFBindingPackage IncludeEcoPackage_WPFBinding_WPFBindingPackage;
        /// <summary>
        /// Required designer variable
        /// </summary>
        private System.ComponentModel.Container components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Active = false;
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
					this.persistenceMapperSharer1 = new Eco.Persistence.PersistenceMapperSharer();
					// 
					// persistenceMapperSharer1
					// 
					this.persistenceMapperSharer1.MapperProviderTypeName = "WPFBinding.WPFBindingPMP";
					// 
					// WPFBindingEcoSpace
					// 
					this.PersistenceMapper = this.persistenceMapperSharer1;

        }

				private Eco.Persistence.PersistenceMapperSharer persistenceMapperSharer1;
    }
}

namespace WPFBinding
{
    public partial class WPFBindingPMP
    {
        #region Component Designer generated code
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        private Eco.Persistence.SyncHandler syncHandler1;
        private void InitializeComponent()
        {
					this.syncHandler1 = new Eco.Persistence.SyncHandler();
					this.persistenceMapperXml1 = new Eco.Persistence.PersistenceMapperXml();
					// 
					// syncHandler1
					// 
					this.syncHandler1.HistoryLength = 10000;
					// 
					// persistenceMapperXml1
					// 
					this.persistenceMapperXml1.FileName = "Data.xml";
					this.persistenceMapperXml1.SyncHandler = this.syncHandler1;
					// 
					// WPFBindingPMP
					// 
					this.EcoSpaceTypeName = "WPFBinding.WPFBindingEcoSpace";
					this.PersistenceMapper = this.persistenceMapperXml1;

        }
        #endregion
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
				}

				private Eco.Persistence.PersistenceMapperXml persistenceMapperXml1;
    }
}

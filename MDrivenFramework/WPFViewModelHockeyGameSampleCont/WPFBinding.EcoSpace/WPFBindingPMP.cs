namespace WPFBinding
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Handles;
    using Eco.Services;
    using Eco.Persistence;
    
    public partial class WPFBindingPMP : Eco.Persistence.PersistenceMapperProvider
    {
        public WPFBindingPMP() : base()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets the singleton instance of the PersistenceMapperProvider.
        /// </summary>
        public static WPFBindingPMP Instance
        {
            get
            {
                return GetInstance<WPFBindingPMP>();
            }
        }

        /// <summary>
        /// Regenerates the database schema, no questions asked.
        /// </summary>
        public static void GenerateDB()
        {
            Instance.DoGenerateDB();
        }
        #region Eco Managed Code
        private void DoGenerateDB()
        {
            if (PersistenceMapper is PersistenceMapperDb)
            {
                (PersistenceMapper as PersistenceMapperDb).CreateDatabaseSchema(GetTypeSystemService(true), new DefaultCleanPsConfig(true));
            }
            else
            {
                throw new InvalidOperationException("The PersistenceMapper is not a PersistenceMapperDb");
            }
        }
        #endregion

    }
}

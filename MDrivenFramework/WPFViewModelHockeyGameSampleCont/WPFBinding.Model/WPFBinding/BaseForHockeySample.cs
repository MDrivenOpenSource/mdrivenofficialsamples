namespace WPFBinding {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;


    public class OnBringUpWindowArgs : EventArgs
    {
        public string WindowToOpen;
    }

  public partial class BaseForHockeySample
	{
		public override string ToString()
		{
			return AsIObject().DefaultStringRepresentation;
		}

        public static event EventHandler<OnBringUpWindowArgs> OnBringUpWindow;

        [UmlElement(Id = "21355357-65fa-4256-94bb-1fa0bb7afef2"), UmlMetaAttribute("isQuery", true)]
        public void BringUpWindow(string name)
        {
            if (OnBringUpWindow!=null)
                OnBringUpWindow(this, new OnBringUpWindowArgs() { WindowToOpen=name});
        }

		
	}
}
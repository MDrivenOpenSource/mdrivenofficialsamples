// This file contains all user-written code for the class

using System.Linq;
using Eco.ObjectRepresentation;
using Eco.Services;
using Eco.UmlRt;

namespace HockeyCont
{
	public partial class Game : ISingleLinkCatcher
    {
			partial void HomeReverseDeriving(Team value)
			{
				AddTeam(value, true);
			}

			partial void VisitorReverseDeriving(Team value)
			{
				AddTeam(value, false);
			}

			private void AddTeam(Team team, bool isHome)
			{
				RemoveRole(isHome);
				RemoveTeam(team);

				if (team == null)
					return;

				Teams.Add(team);

				(from tgr in TeamGameRole where tgr.Teams == team select tgr).Single().IsHome = isHome;
			}

			private void RemoveRole(bool isHome)
			{
				foreach (var tgr in TeamGameRole)
				{
					if (tgr.IsHome == isHome)
					{
						tgr.AsIObject().Delete();
						break;
					}
				}
			}

			private void RemoveTeam(Team team)
			{
				if (team == null)
					return;

				foreach (var tgr in TeamGameRole)
				{
					if (tgr.Teams == team)
					{
						tgr.AsIObject().Delete();
						break;
					}
				}
			}

			public void SingleLinkModified(IAssociationEnd ae, IEcoObject oldValue, IEcoObject newValue)
			{
				if (ae.LoopbackIndex == Eco_LoopbackIndices.GameType)
				{
					if (newValue != oldValue)
					{
						if (oldValue == null)
							return;

						var newGameType = newValue as GameType;

						if (newGameType == null)
						{
							Home = null;
							Visitor = null;
						}
						else
						{
							if (Home != null && !newGameType.TeamTypes.Contains(Home.TeamType))
								Home = null;

							if (Visitor != null && !newGameType.TeamTypes.Contains(Visitor.TeamType))
								Visitor = null;
						}
					}
				}
			}
		}
}

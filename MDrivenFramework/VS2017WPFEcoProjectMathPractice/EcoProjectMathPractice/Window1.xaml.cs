using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Eco.ViewModel.Runtime;
using System.Globalization;

namespace EcoProjectMathPractice
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private EcoProjectMathPractice.EcoProjectMathPracticeEcoSpace ecoSpace;
//        private Eco.WinForm.EcoAutoFormExtender ecoAutoForms;
        public Window1()
        {
            ecoSpace = new EcoProjectMathPractice.EcoProjectMathPracticeEcoSpace();
            ecoSpace.Active = true;
            Eco.WPF.WPFDequeuer.Active = true;
            ViewDefinitionsInApplication.Init(ecoSpace);
            InitializeComponent();
  //          ecoAutoForms = new Eco.WinForm.EcoAutoFormExtender(); // optional
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Session session=new Session(ecoSpace);
            session.CreateSessionType1();
            MainListView.DataContext=session;
            MainListView.ItemsSource=session.Calculation;
            Main.DataContext = session;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Session session = new Session(ecoSpace);
            session.CreateSessionType2();
            MainListView.DataContext = session;
            MainListView.ItemsSource = session.Calculation;
            Main.DataContext = session;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Session session = new Session(ecoSpace);
            session.CreateSessionType3();
            MainListView.DataContext = session;
            MainListView.ItemsSource = session.Calculation;
            Main.DataContext=session;
        }
    }

    [ValueConversion(typeof(string), typeof(Brush))]
    public class MyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string backColor = (string)value;
            if (backColor=="red")
                return new LinearGradientBrush(Colors.Red,Colors.PaleVioletRed,new Point(0,0),new Point(1,1));
            if (backColor == "green")
                return new LinearGradientBrush(Colors.Green, Colors.GreenYellow, new Point(0, 0), new Point(1, 1));
            return new LinearGradientBrush(Colors.Gray, Colors.LightGray, new Point(0, 0), new Point(1, 1));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }



}

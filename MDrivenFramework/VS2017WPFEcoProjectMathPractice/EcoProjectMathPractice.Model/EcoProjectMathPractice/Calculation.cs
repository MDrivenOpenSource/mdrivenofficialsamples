namespace EcoProjectMathPractice {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;


    public abstract partial class Calculation
    {

        partial void ObjectCreated()
        {
            
        }
        [UmlElement(Id = "07549491-a0cd-4a3b-8ae6-0cc641a04f7c")]
        public abstract void InitValues();
    }
}
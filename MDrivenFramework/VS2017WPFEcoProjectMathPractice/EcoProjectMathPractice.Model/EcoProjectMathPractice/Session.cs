namespace EcoProjectMathPractice {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;
  
  
  public partial class Session {
      [UmlElement(Id = "f1ed557e-5a8a-487f-8075-2e0ea8dc68c3")]
      public void CreateSessionType1()
      {
            for(int i=0;i<10;i++)
                this.Calculation.Add(new Addition2(this.ServiceProvider()));

            InitAllValues();
      }

      private void InitAllValues()
      {
          foreach(Calculation x in Calculation)
              x.InitValues();
      }

      private Random _random=new Random(DateTime.Now.Millisecond);
      [UmlElement(Id = "a1100a4c-2fa3-4935-9d4b-a43ae2c1af4d")]
      public int GetRandom(int biggerthan, int smallerthan)
      {
        return _random.Next(smallerthan-biggerthan)+biggerthan;
      }

      [UmlElement(Id = "4413cc21-61d5-45d9-8c3a-a17e31c06d84")]
      public void CreateSessionType2()
      {
          for (int i = 0; i < 10; i++)
          {
              this.Calculation.Add(new Substraction2(this.ServiceProvider()));
          }
          InitAllValues();
      }

      [UmlElement(Id = "e814008b-fb1d-4bf2-acdb-98b9731d4efa")]
      public void CreateSessionType3()
      {
          for (int i = 0; i < 10; i++)
          {
              this.Calculation.Add(new Multiplication(this.ServiceProvider()));
          }
          InitAllValues();
      }
  }
}
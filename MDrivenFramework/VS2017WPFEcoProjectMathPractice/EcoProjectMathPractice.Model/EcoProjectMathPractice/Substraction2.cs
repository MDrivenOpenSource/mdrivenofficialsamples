namespace EcoProjectMathPractice {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;
  
  
  public partial class Substraction2 {
      [UmlElement(Id = "8cc198c3-41bc-45e3-ae9e-97c76d9d2b98")]
      public override void InitValues()
      {
          A = Session.GetRandom(10, 30);
          B = Session.GetRandom(0, A);
      }
  }
}
// This file contains all user-written code for the class

namespace EcoProjectMathPractice {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;

  public partial class Addition2
  {
      [UmlElement(Id = "98f453fb-ff7a-4ceb-991b-958940ef0ea8")]
      public override void InitValues()
      {
        A = Session.GetRandom(0,30);
        B = Session.GetRandom(0, A);
      }
  }
}

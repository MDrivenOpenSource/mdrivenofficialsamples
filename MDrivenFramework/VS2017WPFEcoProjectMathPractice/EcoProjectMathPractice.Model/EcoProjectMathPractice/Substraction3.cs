namespace EcoProjectMathPractice {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;
  
  
  public partial class Substraction3 {
      [UmlElement(Id = "fef485d6-d140-4243-8c42-243e753f9a0a")]
      public override void InitValues()
      {
          A = Session.GetRandom(10, 30);
          B = Session.GetRandom(0, (int)Math.Truncate((double)A / 2));
          C = Session.GetRandom(0, (int)Math.Truncate((double)A / 2));
      }
  }
}
namespace EcoProjectMathPractice {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;
  
  
  public partial class Multiplication {
      [UmlElement(Id = "bc318fd7-4b0b-44c3-993d-c0452fa440ca")]
      public override void InitValues()
      {
          A = Session.GetRandom(2, 4);
          B = Session.GetRandom(0, 11);

      }
  }
}
namespace PluginRoseFileRead
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Windows.Forms;
    using Eco.Handles;
    using Eco.ObjectRepresentation;
    using Eco.Services;
    using Eco.Windows.Forms;
    using Eco.WinForm;

    public partial class Form1
    {
        private Eco.Handles.ReferenceHandle rhRoot;
        private Eco.WinForm.EcoActionExtender ecoGlobalActions;
        private Eco.WinForm.EcoDragDropExtender ecoDragDrop;
        private Eco.WinForm.EcoAutoFormExtender ecoAutoForms;
        private Eco.WinForm.EcoListActionExtender ecoListActions;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rhRoot = new Eco.Handles.ReferenceHandle();
            this.ecoListActions = new Eco.WinForm.EcoListActionExtender();
            this.ecoDragDrop = new Eco.WinForm.EcoDragDropExtender();
            this.ecoGlobalActions = new Eco.WinForm.EcoActionExtender();
            this.ecoAutoForms = new Eco.WinForm.EcoAutoFormExtender();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.labelInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).BeginInit();
            this.SuspendLayout();
            // 
            // rhRoot
            // 
            this.rhRoot.EcoSpaceTypeName = "PluginRoseFileRead.PluginRoseFileReadEcoSpace";
            // 
            // ecoListActions
            // 
            this.ecoListActions.DeleteQuestion = "Delete object(s)?";
            // 
            // ecoGlobalActions
            // 
            this.ecoGlobalActions.RootHandle = this.rhRoot;
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Location = new System.Drawing.Point(119, 33);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(202, 20);
            this.textBoxFileName.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "File to load";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(327, 33);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(36, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "*.mdl";
            this.openFileDialog1.Filter = "(rose file)|*.mdl";
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(369, 34);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(65, 21);
            this.buttonLoad.TabIndex = 6;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo.Location = new System.Drawing.Point(44, 103);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(0, 25);
            this.labelInfo.TabIndex = 7;
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(464, 186);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxFileName);
            this.Name = "Form1";
            this.Text = "Load a rose mdl file";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rhRoot)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private TextBox textBoxFileName;
        private Label label1;
        private Button button4;
        private OpenFileDialog openFileDialog1;
        private Button buttonLoad;
        private Label labelInfo;
    }
}

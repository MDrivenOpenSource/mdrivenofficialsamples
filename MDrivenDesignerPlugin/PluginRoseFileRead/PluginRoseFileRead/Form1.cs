namespace PluginRoseFileRead
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;
    using Eco.Handles;
    using Eco.Linq;
    using Eco.ObjectRepresentation;
    using Eco.Services;
    using Eco.Windows.Forms;
    using Eco.WinForm;
    using PlugInRoseFileReadAssembly;
    using System.Xml.Linq;
    using System.IO;
    using Eco.UmlRt;
    using RoseModel;

    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public partial class Form1 : System.Windows.Forms.Form
    {
        public Form1(PluginRoseFileRead.PluginRoseFileReadEcoSpace ecoSpace)
        {
            // Required for Windows Form Designer support
            this.InitializeComponent();

            // Set EcoSpace (actually stores it in rhRoot.EcoSpace)
            // Note that you need to set the EcoSpace property for 
            // each ReferenceHandle and VariableHandle you add to 
            // the form.
            this.rhRoot.EcoSpace = ecoSpace;
            ecoSpace.AllowDeactivateDirty = true;
        }

        public PluginRoseFileRead.PluginRoseFileReadEcoSpace EcoSpace
        {
            get
            {
                return (PluginRoseFileRead.PluginRoseFileReadEcoSpace)this.rhRoot.EcoSpace;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {


        }

        /*
        private void button1_Click(object sender, EventArgs e)
        {
            RoseImport ri = new RoseImport();
            ri.OpenFile(@"C:\CapableObjects\trunk\demos\NewDemos\PluginRoseFileRead\SampleModel1.mdl", true);
            ri.XDocument.Save(@"c:\temp\rosexml.xml");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RoseImport ri = new RoseImport();
            ri.OpenFile(@"C:\Pexlain_WF\Pexlain\Riks\RiksdagenSourceSafe070601\Gemensamt\Modellen\Distribution\DistributionsModell.mdl", true);


            ri.XDocument.Save(@"c:\temp\rosexml.xml");

            int i = 0;
            foreach (XDocument xd in ri.CatFiles)
            {
                xd.Save(@"c:\temp\rosexml_cat" + i.ToString() + ".xml");
                i++;
            }
        }

        */

        private void button4_Click(object sender, EventArgs e)
        {
            openFileDialog1.AddExtension = true;
            openFileDialog1.DefaultExt = "*.mdl";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxFileName.Text = openFileDialog1.FileName;
            }


        }

        private PluginRoseFileReadEcoSpace _es;
        private Dictionary<string, IEcoObject> _createdobjects;
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            if (File.Exists(textBoxFileName.Text))
            {

                RoseImport ri = new RoseImport();  // This reads the mdl and turns it into XML
                SetInfo(Path.GetFileName(textBoxFileName.Text));
                ri.OpenFile(textBoxFileName.Text, true);
                _es = EcoSpace;
                _es.AllowDeactivateDirty = true;
                _es.Active = false;
                _es.Active = true; // Clear before new load
                _createdobjects = new Dictionary<string, IEcoObject>();


                InstansiateObjects(ri.XDocument);       // This should interpret the xml content and match it with the model I have
                foreach (XDocument xd in ri.CatFiles)   // RoseImport also find CAT file references and reads those too
                {
                    SetInfo(Path.GetFileName(xd.Root.Attribute("filename").Value));

                    InstansiateObjects(xd);
                }

                TransformRoseToModlr();
                SetInfo("Done...");
            }
        }

        private void SetInfo(string p)
        {
            labelInfo.Text = p;
            labelInfo.Refresh();
            Refresh();
        }

        private void TransformRoseToModlr()
        {
            if (_modlrESP == null)
                return;
            Dictionary<ro_Class_Category, Eco.ModelLayer.Package> packages = new Dictionary<ro_Class_Category, Eco.ModelLayer.Package>();
            Dictionary<ro_Class, Eco.ModelLayer.Class> classes = new Dictionary<ro_Class, Eco.ModelLayer.Class>();
            Dictionary<string, Eco.ModelLayer.Class> classesForAssociation = new Dictionary<string, Eco.ModelLayer.Class>();
            Dictionary<ro_Role, Eco.ModelLayer.AssociationEnd> roles = new Dictionary<ro_Role, Eco.ModelLayer.AssociationEnd>();
            foreach (RoseModel.ro_Class_Category cat in _es.Extents.AllInstances<ro_Class_Category>())
            {
                if (cat.logical_models.Count == 0)
                    continue;
                Eco.ModelLayer.Package mPack = new Eco.ModelLayer.Package(_modlrESP);
                packages.Add(cat, mPack);
                mPack.Name = cat.name;
                SetInfo("Repository " + cat.name);

                foreach (var lm in cat.logical_models)
                {

                    if (lm is RoseModel.ro_Class)
                    {
                        RoseModel.ro_Class cl = lm as RoseModel.ro_Class;
                        Eco.ModelLayer.Class mCl = new Eco.ModelLayer.Class(_modlrESP);
                        classes.Add(cl, mCl);
                        mCl.Name = cl.name;
                        mCl.Package_ = mPack;
                        classesForAssociation.Add(mPack.Name + "::" + mCl.Name, mCl);

                        foreach (var attr in cl.class_attributes)
                        {
                            Eco.ModelLayer.Attribute mA = new Eco.ModelLayer.Attribute(_modlrESP);
                            mA.Name = attr.name;
                            mA.AttributeType = StripQuotes(attr.type);
                            mCl.Feature.Add(mA);
                        }
                        foreach (var op in cl.operations)
                        {
                            Eco.ModelLayer.Method mM = new Eco.ModelLayer.Method(_modlrESP);
                            mM.Name = op.name;
                            mCl.Feature.Add(mM);
                            if (op.result != "")
                            {
                                mM.ReturnParameter = new Eco.ModelLayer.Parameter(_modlrESP);
                                mM.ReturnParameter.SetParameterType(StripQuotes(op.result));
                            }
                            foreach (var param in op.parameters)
                            {
                                Eco.ModelLayer.Parameter mP = new Eco.ModelLayer.Parameter(_modlrESP);
                                mM.Parameter.Add(mP);
                                mP.Name = param.name;
                                mP.SetParameterType(StripQuotes(param.type));
                            }
                        }
                    }
                }
            }

            foreach (ro_Class c in classes.Keys)
            {
                if (c.superclasses.Count > 0)
                {

                    classes[c].Superclass = classes[c.superclasses[0].ro_Class];

                }
            }

            SetInfo("Repository Associations");

            foreach (ro_Association assoc in _es.Extents.AllInstances<ro_Association>())
            {
                if (assoc.roles.Count == 2 && assoc.roles[0].ro_Class != null && assoc.roles[1].ro_Class!=null)
                {
                    Eco.ModelLayer.Association mAss = new Eco.ModelLayer.Association(_modlrESP);

                    mAss.Name = CheckUnNamed(assoc.name);
                    Eco.ModelLayer.AssociationEnd ae1 = new Eco.ModelLayer.AssociationEnd(_modlrESP);
                    Eco.ModelLayer.AssociationEnd ae2 = new Eco.ModelLayer.AssociationEnd(_modlrESP);

                    ae1.Name = CheckUnNamed(assoc.roles[0].name);
                    ae2.Name = CheckUnNamed(assoc.roles[1].name);
                    mAss.AssociationEnd.Add(ae1);
                    mAss.AssociationEnd.Add(ae2);

                    roles.Add(assoc.roles[0], ae1);
                    roles.Add(assoc.roles[1], ae2);

                    ae1.Participant = classes[assoc.roles[0].ro_Class];
                    ae2.Participant = classes[assoc.roles[1].ro_Class];
                    mAss.Package_ = ae1.Participant.Package_;
                    ae1.IsNavigable = assoc.roles[0].is_navigable == "TRUE";
                    if (assoc.roles[0].is_aggregate == "TRUE")
                        ae2.Aggregation = AggregationKind.Aggregate; // looks wrong (ae2 not ae1) but verified
                    ae2.IsNavigable = assoc.roles[1].is_navigable == "TRUE";
                    if (assoc.roles[1].is_aggregate == "TRUE")
                        ae1.Aggregation = AggregationKind.Aggregate; // looks wrong (ae1 not ae2) but verified
                    if (assoc.AssociationClass != "")
                    {
                        string key = StripQuotes(assoc.AssociationClass);
                        if (classesForAssociation.ContainsKey(key))
                            mAss.Class = classesForAssociation[key];
                        else if (key.Contains("Logical View::"))
                        {
                            key = key.Replace("Logical View::", "");
                            if (classesForAssociation.ContainsKey(key))
                                mAss.Class = classesForAssociation[key];
                        }
                    }
                    if (assoc.roles[0].client_cardinality != "")
                        ae1.Multiplicity = assoc.roles[0].client_cardinality.Split('"')[1];
                    if (assoc.roles[1].client_cardinality != "")
                        ae2.Multiplicity = assoc.roles[1].client_cardinality.Split('"')[1];


                }


            }

            /////////////////////// DIAGRAMS BELOW
            var rosediags = _es.Extents.AllInstances<ro_ClassDiagram>();
            foreach (ro_ClassDiagram diag in rosediags)
            {
                ModlrDiagrams.Diagram mDiag;
                mDiag = new ModlrDiagrams.Diagram(_modlrESP);
                mDiag.Name = diag.name;
                SetInfo("Diagrams " + diag.name);


                Dictionary<ro_ClassView, ModlrDiagrams.PlacedClass> placedclasses = new Dictionary<ro_ClassView, ModlrDiagrams.PlacedClass>();
                Dictionary<string, ro_ClassView> placedclassesbykey = new Dictionary<string, ro_ClassView>();

                foreach (RoseModel.View v in diag.items)
                {
                    if (v is ro_ClassView)
                    {
                        ro_ClassView roc = (v as ro_ClassView);
                        if (roc.ro_Class == null)
                            continue;
                        ModlrDiagrams.PlacedClass pc = new ModlrDiagrams.PlacedClass(_modlrESP);
                        placedclasses.Add(roc, pc);
                        placedclassesbykey.Add(roc.key, roc);
                        pc.Class = classes[roc.ro_Class];
                        Point p = ToPoint(roc.location);
                        pc.x = p.X / 4;
                        pc.y = p.Y / 4;
                        pc.Size = 1;
                        int red = (int.Parse(roc.fill_color) & 0xFF0000) / (256 * 256);
                        int green = (int.Parse(roc.fill_color) - red * 256 * 256) / 256;
                        int blue = int.Parse(roc.fill_color) - red * 256 * 256 - green * 256;
                        pc.Color = blue * 256 * 256 + green * 256 + red + unchecked((int)0x0B0000000); //;
                        
                        if (roc.SuppressAttribute == "TRUE")
                            pc.PickedFeatureVisibility = ModlrDiagrams.PickedFeatureVisibilityEnum.HideAll;
                        else
                            pc.PickedFeatureVisibility = ModlrDiagrams.PickedFeatureVisibilityEnum.ShowAll;


                        mDiag.PlacedClass.Add(pc);

                    }
                }


                // Associations
                Dictionary<string, ModlrDiagrams.AssociationEndConnection> associationEndConnectionByRoseKey = new Dictionary<string, ModlrDiagrams.AssociationEndConnection>();
                foreach (RoseModel.View v in diag.items)
                {
                    if (v is ro_AssociationViewNew)
                    {
                        ro_AssociationViewNew assocView = v as ro_AssociationViewNew;

                        if (assocView.roleview_list.Count == 2)
                        {
                            ro_RoleView rv1 = assocView.roleview_list[0];
                            ro_RoleView rv2 = assocView.roleview_list[1];

                            if (rv1.ro_Role != null && rv2.ro_Role != null && roles.ContainsKey(rv1.ro_Role) && roles.ContainsKey(rv2.ro_Role))
                            {

                                Eco.ModelLayer.AssociationEnd ae1 = roles[rv1.ro_Role];
                                Eco.ModelLayer.AssociationEnd ae2 = roles[rv2.ro_Role];

                                ModlrDiagrams.AssociationEndConnection aec1 = new ModlrDiagrams.AssociationEndConnection(_modlrESP);
                                ModlrDiagrams.AssociationEndConnection aec2 = new ModlrDiagrams.AssociationEndConnection(_modlrESP);
                                associationEndConnectionByRoseKey.Add(assocView.key, aec1);

                                aec1.AssociationEnd = ae1;
                                aec1.ViaPointsStartsHere = true;
                                aec1.next = aec2;
                                aec2.AssociationEnd = ae2;
                                aec2.prev = aec1;

                                ro_ClassView roc1 = placedclassesbykey[rv1.supplier];
                                ro_ClassView roc2 = placedclassesbykey[rv2.supplier];

                                aec1.PlacedClass = placedclasses[roc1];
                                aec2.PlacedClass = placedclasses[roc2];

                                Point p1 = ToPoint(roc1.location);
                                Point p1d = ToPoint(rv1.location);

                                //aec1.x = p1.X + p1d.X;
                                //aec1.y = p1.Y + p1d.Y;
                                aec1.SideToUse = 4;

                                Point p2 = ToPoint(roc2.location);
                                Point p2d = ToPoint(rv2.location);

                                //aec2.x = p2.X + p2d.X;
                                //aec2.y = p2.Y + p2d.Y;
                                aec2.SideToUse = 4;

                                if (roc1 == roc2)
                                {
                                    //Relation to self
                                    aec1.SideToUse = 0;
                                    aec2.SideToUse = 1;
                                    aec1.NextLegDrawKind = 1;
                                }


                                //aec1.PlacedClass= placedclasses[xxx] // hur fan vet man vilken placedclass den g�r mot?

                                //classes[rv1.ro_Role.ro_Class]

                                //rv1.ro_Role.ro_Class .client_cardinality
                            }
                        }
                        else
                        {
                            //??
                        }
                    }
                    else if (v is ro_InheritView)
                    {
                        ro_InheritView inh = v as ro_InheritView;
                        if (placedclassesbykey.ContainsKey(inh.supplier) && placedclassesbykey.ContainsKey(inh.client))
                        {
                            ro_ClassView roc = placedclassesbykey[inh.client];
                            ro_ClassView roc2 = placedclassesbykey[inh.supplier];

                            ModlrDiagrams.SpecializationConnection near = new ModlrDiagrams.SpecializationConnection(_modlrESP);
                            ModlrDiagrams.GeneralizationConnection far = new ModlrDiagrams.GeneralizationConnection(_modlrESP);

                            near.PlacedClass = placedclasses[roc];
                            far.PlacedClass = placedclasses[roc2];
                            near.Class = far.PlacedClass.Class;
                            far.Class = near.PlacedClass.Class;

                            near.next=far;
                            far.prev =near;
                            near.SideToUse = 4;
                            far.SideToUse = 4;
                            near.ViaPointsStartsHere = true;

                            near.PlacedClass.SpecializationConnection.Add(near);
                            far.PlacedClass.GeneralizationConnection.Add(far);



                        }
                    }
                }

                // AssociationClass connection
                foreach (RoseModel.View v in diag.items)
                {
                    if (v is ro_AssocAttachView)
                    {
                        ro_AssocAttachView attachview = v as ro_AssocAttachView;

                        ModlrDiagrams.AssociationEndConnection aec1 = null;
                        ro_ClassView roc1 = null;
                        if (associationEndConnectionByRoseKey.ContainsKey(attachview.supplier) && placedclassesbykey.ContainsKey(attachview.client))
                        {
                            aec1 = associationEndConnectionByRoseKey[attachview.supplier];
                            roc1 = placedclassesbykey[attachview.client];
                        }
                        else if (associationEndConnectionByRoseKey.ContainsKey(attachview.client) && placedclassesbykey.ContainsKey(attachview.supplier))
                        {
                            aec1 = associationEndConnectionByRoseKey[attachview.client];
                            roc1 = placedclassesbykey[attachview.supplier];
                        }
                        if (aec1 != null && roc1!=null)
                        {

                            ModlrDiagrams.PlacedClass pc = placedclasses[roc1];



                            ModlrDiagrams.AssociationConnection ac = new ModlrDiagrams.AssociationConnection(_modlrESP);
                            ModlrDiagrams.LinkClassConnectViaPoint lcc = new ModlrDiagrams.LinkClassConnectViaPoint(_modlrESP);
                            lcc.PercentFromStart = 50;
                            lcc.AssociationEndConnection = aec1;
                            pc.AssociationConnection = ac;
                            ac.SideToUse = 4;
                            ac.Association = aec1.AssociationEnd.Association;
                            ac.ViaPointsStartsHere = true;
                            ac.next = lcc;
                        }



                    }

                }

            }
        }

        private Point ToPoint(string p)
        {
            Point result = new Point();
            if (p.Length > 2)
            {
                string[] cords = p.Substring(1, p.Length - 2).Split(',');
                result.X = int.Parse(cords[0]);
                result.Y = int.Parse(cords[1]);
            }
            return result;
        }

        private string CheckUnNamed(string p)
        {
            if (p.StartsWith("$UNNAMED$"))
                return "";
            return p;
        }

        private string StripQuotes(string p)
        {
            if (p.Length > 1 && p[0] == '\"' && p[p.Length - 1] == '\"')
                return p.Substring(1, p.Length - 2);
            return p;
        }

        private void InstansiateObjects(XDocument xDocument)
        {
            foreach (XElement xe in xDocument.Root.Elements())
            {
                InstansiateObjects(xe);
            }
        }

        private IEcoObject InstansiateObjects(XElement xe)
        {
            IEcoObject existing = null;
            XElement keyelement = xe.Elements("key") as XElement;
            if (keyelement != null && keyelement.Value != null)
            {
                string Key = keyelement.Value;
                if (_createdobjects.ContainsKey(keyelement.Value))
                    existing = _createdobjects[keyelement.Value];
            }
            if (existing == null)
            {
                IClass iclass = EcoServiceHelper.GetTypeSystemService(_es).TypeSystem.
                        GetClassifierByName("ro_" + xe.Name.LocalName) as IClass; // My model prefixed all rose classes with ro_
                if (iclass == null)
                    return null;
                existing = EcoServiceHelper.GetObjectFactoryService(_es).CreateNewObject(iclass);
                if (keyelement != null && keyelement.Value != null)
                {
                    _createdobjects.Add(keyelement.Value, existing);
                }

            }

            foreach (XElement child in xe.Elements())
            {
                if (!child.HasElements)
                {
                    try
                    {
                        IElement elem = existing.AsIObject().Properties[child.Name.LocalName];
                        if (elem != null && !(elem is IObjectList))
                            elem.SetValue<string>(child.Value);
                    }
                    catch
                    { 
                        // if trying to get a Property we do not have...
                    }
                }
                else
                {
                    // load association
                    try
                    {
                        IElement elem = existing.AsIObject().Properties[child.Name.LocalName];
                        if (elem is IObjectList)
                        {
                            foreach (XElement associationobject in child.Elements())
                            {
                                IEcoObject associaionInstansiatedObject = InstansiateObjects(associationobject);
                                if (associaionInstansiatedObject == null)
                                    continue;
                                if (associaionInstansiatedObject.AsIObject().UmlClass.ConformsTo((elem as IObjectList).UmlClass))
                                    (elem as IObjectList).Add(associaionInstansiatedObject);
                                else
                                    System.Diagnostics.Debug.WriteLine(string.Format("Tried to add a {0} to a list of {1}, association was {2}", associaionInstansiatedObject.AsIObject().UmlClass.Name, (elem as IObjectList).UmlClass.Name, child.Name.LocalName));
                            }
                        }
                    }
                    catch
                    {
                        // if trying to get a Property we do not have...
                    }
                }
            }

            return existing;
        }

        private IEcoServiceProvider _modlrESP;
        internal void InstallModlrESP(IEcoServiceProvider esp)
        {
            _modlrESP = esp;
        }
    }
}

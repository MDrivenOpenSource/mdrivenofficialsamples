﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modlr.Plugins;

namespace PluginRoseFileRead
{
    class PluginCode:IModlrPlugin
    {
        #region IModlrPlugin Members

        public string GetName()
        {
            return "RationalRose";
        }

        public List<IModlrPluginMenuOperation> ProvideOperations()
        {
            List<IModlrPluginMenuOperation> result = new List<IModlrPluginMenuOperation>();
            result.Add(new Operation());
            return result;
        }

        #endregion
    }

    class Operation : IModlrPluginMenuOperation
    {

        #region IModlrPluginMenuOperation Members

        public void CheckEnableOrExecute(Eco.ObjectRepresentation.IEcoServiceProvider esp, bool execute, Eco.ObjectRepresentation.IEcoObject maincontext, Eco.ObjectRepresentation.IEcoObject subcontext, out bool enabled)
        {
            enabled=true;
            if (execute)
            {
                PluginRoseFileReadEcoSpace ecoSpace = new PluginRoseFileRead.PluginRoseFileReadEcoSpace();
                ecoSpace.Active = true;
                Form1 mainForm = new Form1(ecoSpace);
                mainForm.InstallModlrESP(esp);
                mainForm.ShowDialog();
                ecoSpace.AllowDeactivateDirty = true;
                ecoSpace.Active = false;
                ecoSpace.Dispose();
            }
        }

        public string GetName()
        {
            return "Import Mdl file";
        }

        #endregion
    }
}

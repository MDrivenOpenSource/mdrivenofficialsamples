//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RoseModel {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using Eco.Services;
  using Eco.ObjectRepresentation;
  using Eco.ObjectImplementation;
  using Eco.Subscription;
  using Eco.UmlRt;
  using Eco.UmlCodeAttributes;
  
  
  [UmlElement(Id="a9460f42-b36d-40fa-921c-5fe780ef7ef4")]
  public partial class ro_Compartment : Eco.ObjectImplementation.ILoopBack2, System.ComponentModel.INotifyPropertyChanged {
    
    #region *** Constructors ***
    
    public ro_Compartment(Eco.ObjectRepresentation.IEcoServiceProvider serviceProvider) {
      this.Initialize(serviceProvider);
      try {
        this.ObjectCreated();
      }
      catch (System.Exception ) {
        this.Deinitialize(serviceProvider);
        throw;
      }
    }
    
    // For framework internal use only
    protected ro_Compartment(Eco.ObjectImplementation.IContent content) {
      this.eco_Content = content;
      content.AssertLoopbackUnassigned();
    }
    
    /// <summary>This method is called when the object has been created the first time (not when loaded from a db)</summary>
    partial void ObjectCreated();
    
    /// <summary>This method is called before the object is deleted. You can perform custom clean up or prevent the deletion by returning false or throw an exception (preferably EcoObjectDeleteException)</summary>
    partial void PreDelete(ref System.Boolean canDelete);
    
    #endregion *** Constructors ***
    
    #region *** ILoopback implementation ***
    
    public virtual void set_MemberByIndex(int index, object value) {
      throw new System.IndexOutOfRangeException();
    }
    
    public virtual object get_MemberByIndex(int index) {
      throw new System.IndexOutOfRangeException();
    }
    
    Eco.ObjectRepresentation.IObject Eco.ObjectRepresentation.IObjectProvider.AsIObject() {
      return this.eco_Content.AsIObject();
    }
    
    void Eco.ObjectImplementation.ILoopBack2.SetContent(Eco.ObjectImplementation.IContent content) {
      if ((this.eco_Content != null)) {
        throw new System.InvalidOperationException();
      }
      this.eco_Content = content;
    }
    
    #endregion *** ILoopback implementation ***
    
    #region *** LoopbackIndex declarations ***
    
    public class Eco_LoopbackIndices {
      
      public const int Eco_FirstMember = 0;
      
      public const int Parent_View = Eco_FirstMember;
      
      public const int anchor = (Parent_View + 1);
      
      public const int icon_style = (anchor + 1);
      
      public const int fill_color = (icon_style + 1);
      
      public const int key = (fill_color + 1);
      
      public const int location = (key + 1);
      
      public const int justify = (location + 1);
      
      public const int name = (justify + 1);
      
      public const int nlines = (name + 1);
      
      public const int max_width = (nlines + 1);
      
      public const int font = (max_width + 1);
      
      public const int Eco_MemberCount = (font + 1);
    }
    
    #endregion *** LoopbackIndex declarations ***
    
    #region *** IObjectProvider implementation ***
    
    public virtual Eco.ObjectRepresentation.IObjectInstance AsIObject() {
      return this.eco_Content.AsIObject();
    }
    
    #endregion *** IObjectProvider implementation ***
    
    #region *** INotifyPropertyChanged implementation ***
    
    event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged {
    
      add { eco_Content.PropertyChanged += value; }
    
      remove { eco_Content.PropertyChanged -= value; }
    
    }
    
    #endregion *** INotifyPropertyChanged implementation ***
    
    #region *** ECO framework implementations ***
    
    protected Eco.ObjectImplementation.IContent eco_Content;
    
    protected virtual void Initialize(Eco.ObjectRepresentation.IEcoServiceProvider serviceProvider) {
      if ((this.eco_Content == null)) {
        Eco.ObjectImplementation.IInternalObjectContentFactory factory = serviceProvider.GetEcoService<Eco.ObjectImplementation.IInternalObjectContentFactory>();
        factory.CreateContent(this);
        this.eco_Content.LoopbackValid();
      }
    }
    
    protected virtual void Deinitialize(Eco.ObjectRepresentation.IEcoServiceProvider serviceProvider) {
      if ((this.eco_Content == null)) {
        Eco.ObjectImplementation.IInternalObjectContentFactory factory = serviceProvider.GetEcoService<Eco.ObjectImplementation.IInternalObjectContentFactory>();
        factory.CreateContentFailed(this.eco_Content, this);
        this.eco_Content = null;
      }
    }
    
    #endregion *** ECO framework implementations ***
    
    [UmlElement(Id="2234abfa-0079-4202-9ffc-5f74048a90ba", Index=Eco_LoopbackIndices.Parent_View)]
    public string Parent_View {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.Parent_View)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.Parent_View, ((object)(value)));
      }
    }
    
    [UmlElement(Id="37f48079-5e7b-42a1-b85a-fbe6778509a9", Index=Eco_LoopbackIndices.anchor)]
    public string anchor {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.anchor)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.anchor, ((object)(value)));
      }
    }
    
    [UmlElement(Id="4765f853-0e4b-49b1-9029-3175472ec09e", Index=Eco_LoopbackIndices.icon_style)]
    public string icon_style {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.icon_style)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.icon_style, ((object)(value)));
      }
    }
    
    [UmlElement(Id="51689090-fd22-492b-b57a-3bc784585ef9", Index=Eco_LoopbackIndices.fill_color)]
    public string fill_color {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.fill_color)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.fill_color, ((object)(value)));
      }
    }
    
    [UmlElement(Id="5b435f65-8b01-49f3-a613-8a968abb5ea8", Index=Eco_LoopbackIndices.key)]
    public string key {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.key)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.key, ((object)(value)));
      }
    }
    
    [UmlElement(Id="66a4c453-dffa-44a8-9156-2a586bebb789", Index=Eco_LoopbackIndices.location)]
    public string location {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.location)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.location, ((object)(value)));
      }
    }
    
    [UmlElement(Id="69f53f62-0169-4f1d-95c2-1c9393ebfeaa", Index=Eco_LoopbackIndices.justify)]
    public string justify {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.justify)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.justify, ((object)(value)));
      }
    }
    
    [UmlElement(Id="b3434555-f095-4a28-b1ca-b761af91d51c", Index=Eco_LoopbackIndices.name)]
    public string name {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.name)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.name, ((object)(value)));
      }
    }
    
    [UmlElement(Id="c9b8d71f-3b2f-4da1-b594-79899be9b971", Index=Eco_LoopbackIndices.nlines)]
    public string nlines {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.nlines)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.nlines, ((object)(value)));
      }
    }
    
    [UmlElement(Id="e8e7f259-f517-4403-be55-08e29f83ce3a", Index=Eco_LoopbackIndices.max_width)]
    public string max_width {
      get {
        return ((string)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.max_width)));
      }
      set {
        this.eco_Content.set_MemberByIndex(Eco_LoopbackIndices.max_width, ((object)(value)));
      }
    }
    
    [UmlElement("AssociationEnd", Id="7cd11171-bbdf-47cb-87ad-09d7214e316a", Index=Eco_LoopbackIndices.font)]
    [UmlMetaAttribute("association", typeof(RoseModelPackage.ro_Compartmentro_Compartmentfontro_Fontfont), Index=1)]
    [UmlMetaAttribute("multiplicity", "0..*")]
    public IEcoList<ro_Font> font {
      get {
        return ((IEcoList<ro_Font>)(this.eco_Content.get_MemberByIndex(Eco_LoopbackIndices.font)));
      }
    }
  }
}

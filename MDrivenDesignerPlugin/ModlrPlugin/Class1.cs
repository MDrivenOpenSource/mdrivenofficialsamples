﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modlr.Plugins;
using Eco.ObjectRepresentation;

namespace ModlrPlugin
{
    public class Class1 : IModlrPlugin
    {
        #region IModlrPlugin Members

        public string GetName()
        {
            return "Demo plugin";
        }


        public List<IModlrPluginMenuOperation> ProvideOperations()
        {
            List<IModlrPluginMenuOperation> ops = new List<IModlrPluginMenuOperation>();
            ops.Add(new OperationShowDialog());
            ops.Add(new OperationActOnClass());

            return ops;
        }   

        #endregion
    }

    public class OperationShowDialog : IModlrPluginMenuOperation
    {
        #region IModlrPluginMenuAction Members

        public void CheckEnableOrExecute(IEcoServiceProvider esp, bool execute, Eco.ObjectRepresentation.IEcoObject maincontext, Eco.ObjectRepresentation.IEcoObject subcontext, out bool enabled)
        {
            enabled = false;
            if (maincontext != null)
            {
                enabled = true;
                if (execute)
                {
                    PlugInDemoDialog d = new PlugInDemoDialog();
                    d.ShowDialog();
                }
            }
        }

        public string GetName()
        {
            return "Show a dialog";
        }

        #endregion
    }

    public class OperationActOnClass : IModlrPluginMenuOperation
    {
        #region IModlrPluginMenuAction Members

        public void CheckEnableOrExecute(IEcoServiceProvider esp,bool execute, Eco.ObjectRepresentation.IEcoObject maincontext, Eco.ObjectRepresentation.IEcoObject subcontext, out bool enabled)
        {
            enabled = false;
            if (maincontext != null && maincontext.AsIObject().AsObject is Eco.ModelLayer.Class)
            {
                enabled = true;
                if (execute)
                {
                    (maincontext.AsIObject().AsObject as Eco.ModelLayer.Class).Name += "X";
                }
            }
        }

        public string GetName()
        {
            return "Operation on class";
        }

        #endregion
    }


}

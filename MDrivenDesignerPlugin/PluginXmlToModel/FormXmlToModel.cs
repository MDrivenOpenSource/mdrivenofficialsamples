﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Eco.Services;
using Eco.ModelLayer;

namespace PluginXmlToModel
{
    public partial class FormXmlToModel : Form
    {
        public FormXmlToModel()
        {
            InitializeComponent();
        }


        private void buttonbrowse_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxFileName.Text = openFileDialog1.FileName;
            }
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            XmlToModel xm=new XmlToModel();
            if (comboBoxStrategy.SelectedIndex==0)
                xm.Load(_esp, textBoxFileName.Text, textBoxPrefix.Text.Trim(), comboBox1.Text);
            else
            if (comboBoxStrategy.SelectedIndex==1)
                xm.Load(_esp, textBoxFileName.Text, textBoxPrefix.Text.Trim(), comboBox1.Text,textBoxAttribName.Text);

            MessageBox.Show(string.Format("{0} new classes added to model.", xm.Classescreated));
        }

        Eco.ObjectRepresentation.IEcoServiceProvider _esp;
        internal void SetESP(Eco.ObjectRepresentation.IEcoServiceProvider esp)
        {
            _esp = esp;

            foreach (Package p in _esp.GetEcoService<IExtentService>().AllInstances<Package>())
            {
                comboBox1.Items.Add(p.Name);
            }

            if (comboBox1.Items.Count == 0)
                comboBox1.Items.Add("NewPackage");
            comboBox1.SelectedIndex = 0;
        }

        private void FormXmlToModel_Load(object sender, EventArgs e)
        {
            comboBoxStrategy.SelectedIndex = 0;
        }
    }
}

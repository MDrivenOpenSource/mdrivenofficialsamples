﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Modlr.Plugins;
using Eco.ObjectRepresentation;
using Eco.Services;
using Eco.Linq;
using Eco.ModelLayer;

namespace PluginXmlToModel
{
    public class Plugin : IModlrPlugin
    {
        public string GetName()
        {
            return "Karlsen X2M";
        }

        public List<IModlrPluginMenuOperation> ProvideOperations()
        {
            List<IModlrPluginMenuOperation> list = new List<IModlrPluginMenuOperation>();
            list.Add(new PluginOperation());
            return list;
        }
    }

    public class PluginOperation : IModlrPluginMenuOperation
    {
        public void CheckEnableOrExecute(Eco.ObjectRepresentation.IEcoServiceProvider esp, bool execute, Eco.ObjectRepresentation.IEcoObject maincontext, Eco.ObjectRepresentation.IEcoObject subcontext, out bool enabled)
        {
            enabled = true;
            if (execute)
            {
                FormXmlToModel form = new FormXmlToModel();
                form.SetESP(esp);
                form.ShowDialog();
            }
        }

        public string GetName()
        {
            return "Xml to model";
        }


    }

    public class XmlToModel
    {
        Package _package;
        string _prefix;
        string[] _takeClassNameFromAttrib;
        Eco.ObjectRepresentation.IEcoServiceProvider _esp;
        public void Load(Eco.ObjectRepresentation.IEcoServiceProvider esp, string file, string classprefix, string packageName,string takeClassNameFromAttrib)
        {
            _takeClassNameFromAttrib = takeClassNameFromAttrib.Split(';');
            Load(esp, file, classprefix, packageName);
        }

        public void Load(Eco.ObjectRepresentation.IEcoServiceProvider esp, string file, string classprefix,string packageName)
        {
            _esp = esp;
            _prefix = classprefix;

            if (_esp != null)
            {
                var result = (from x in _esp.GetEcoService<IExtentService>().AllInstances<Package>() where x.Name == packageName select x).ToArray<Package>();
                if (result.Count() == 0)
                {
                    Package p = new Package(_esp);
                    p.Name = packageName;
                    _package = p;
                }
                else
                {
                    _package = result[0];
                }
            }

            XDocument xd = XDocument.Load(file);
            foreach (XElement xe in xd.Root.Elements())
            {
                Analyze(xe);
            }

        }
        private Eco.ModelLayer.Class Analyze(XElement xe)
        {
            Eco.ModelLayer.Class c = EnsureClass(GetNameFromXE(xe));
            foreach (XAttribute xa in xe.Attributes())
            {
                EnsureAttribute(c, xa.Name.ToString());
            }
            foreach (XElement subxe in xe.Elements())
            {

                if (subxe.FirstNode is XText || subxe.FirstNode==null)
                {
                    // treat as model attribute
                    EnsureAttribute(c, GetNameFromXE(subxe));
                }
                else
                {
                    // treat as model association
                    /*
                        We treat to types of linking:
                         single link may look like this:
                             obj1
                                attr1
                                link
                                    attr1
                     
                     *   multilink often looks like this:
                             obj1
                                attr1
                                link
                                    obj2
                                       attr1
                     */
                    if (subxe.Elements().Count() > 0 && (subxe.Elements().First().FirstNode is XText || subxe.Elements().First().FirstNode == null))
                    {
                        Eco.ModelLayer.Class targetclass = Analyze(subxe);
                        AssociationEnd ae = EnsureAssociation(c, targetclass, GetNameFromXE(subxe));
                        if (ae!=null)
                            ae.Multiplicity = "0..1";
                    }
                    else
                    {
                        List<Eco.ModelLayer.Class> classesfoundinrelation = new List<Eco.ModelLayer.Class>();
                        foreach (XElement xeObjectInAssoc in subxe.Elements())
                        {
                            Eco.ModelLayer.Class targetclass = Analyze(xeObjectInAssoc);
                            if (classesfoundinrelation.IndexOf(targetclass) == -1)
                                classesfoundinrelation.Add(targetclass);
                        }

                        if (classesfoundinrelation.Count > 0)
                        {
                            // if classesfoundinrelation.Count>1 the association is most likely pointing to a super class of the classes found, but we dont have enough info - pick first
                            AssociationEnd ae = EnsureAssociation(c, classesfoundinrelation[0], GetNameFromXE(subxe));
                            if (ae!=null)
                                ae.Multiplicity = "0..*";
                        }
                    }
                }
            }
            return c;
        }

        private string GetNameFromXE(XElement xe)
        {
            if (_takeClassNameFromAttrib == null || _takeClassNameFromAttrib.Length==0)
                return xe.Name.ToString();
            else
            {
                foreach (XAttribute xa in xe.Attributes())
                {
                    if (_takeClassNameFromAttrib.Contains(xa.Name.ToString()))
                        return xa.Value;
                }
                // Fallback
                return xe.Name.ToString();
            }
        }

        private AssociationEnd EnsureAssociation(Eco.ModelLayer.Class cfrom, Eco.ModelLayer.Class cto, string xName)
        {

            if (_esp != null)
            {
                var res = (from x in cfrom.AssociationEnd where x.OtherEnd != null && x.OtherEnd.Name == xName select x);
                if (res.Count() == 0)
                {
                    AssociationEnd ae1=new AssociationEnd(_esp);
                    AssociationEnd ae2 = new AssociationEnd(_esp);
                    Association a = new Association(_esp);
                    a.AssociationEnd.Add(ae1);
                    a.AssociationEnd.Add(ae2);
                    a.Package_ = _package;

                    ae1.Multiplicity = "0..1"; // So it is not left as zero.
                    ae2.Multiplicity = "0..1"; // So it is not left as zero.

                    ae2.Name = xName;
                    ae2.IsNavigable = true;
                    cfrom.AssociationEnd.Add(ae1);
                    cto.AssociationEnd.Add(ae2);
                    return ae2;
                }
                if (res.First().Multiplicity == "0")
                    res.First().Multiplicity = "1..0";
                if (res.First().OtherEnd.Multiplicity == "0")
                    res.First().OtherEnd.Multiplicity = "1..0";
                return res.First().OtherEnd;
            }
            return null;
        }

        Datatype _stringDataType;
        private void EnsureAttribute(Eco.ModelLayer.Class c, string xName)
        {
            FixUnvalidName(ref xName);
            if (_esp != null)
            {
                foreach (ModelElement me in c.AllFeatures)
                {
                    if (me is Eco.ModelLayer.Attribute)
                    {
                        if (xName == (me as Eco.ModelLayer.Attribute).Name)
                        {
                            return;
                        }
                    }
                }
                // not found

                Eco.ModelLayer.Attribute a=new Eco.ModelLayer.Attribute(_esp);
                a.Name = xName;
                if (_stringDataType == null)
                {
                     var types= (from x in _esp.GetEcoService<IExtentService>().AllInstances<Datatype>() where x.Name.ToLower() == "string" select x).ToArray();
                     if (types.Count() == 0)
                     {
                         _stringDataType = new Datatype(_esp);
                         _stringDataType.Package_ = _package;
                         _stringDataType.Name = "String";

                     }
                     else
                         _stringDataType = types[0];
                }
                a.Type = _stringDataType;
                c.Feature.Add(a);

            }
        }

        private int _classescreated;
        public int Classescreated
        {
            get { return _classescreated; }
        }

        private Eco.ModelLayer.Class EnsureClass(string xName)
        {
            FixUnvalidName(ref xName);
            if (_esp != null)
            {
                string completename = _prefix + xName;
                var result = (from x in _esp.GetEcoService<IExtentService>().AllInstances<Eco.ModelLayer.Class>() 
                              where (x.Name == completename && x.OwningPackage == _package) select x).ToArray<Eco.ModelLayer.Class>();
                if (result.Count() == 0)
                {
                    Eco.ModelLayer.Class c = new Eco.ModelLayer.Class(_esp);
                    _classescreated++;
                    c.Name = completename;
                    c.Package_=_package;
                    return c;
                }
                else
                    return result[0];                
            }
            return null;
        }

        private void FixUnvalidName(ref string xName)
        {
            if (xName == "value")
                xName = "TheValue";
            else if (xName == "link")
                xName = "TheLink";
            else if (xName == "class")
                xName = "TheClass";
        }
    }

}

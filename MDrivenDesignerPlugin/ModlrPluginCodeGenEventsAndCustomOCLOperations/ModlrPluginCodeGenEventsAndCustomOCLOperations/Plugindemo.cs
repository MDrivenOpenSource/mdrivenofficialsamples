﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modlr.Plugins;
using Eco.Services;
using Eco.Ocl.Support;

namespace ModlrPluginCodeGenEventsAndCustomOCLOperations
{
  public class Plugindemo : IModlrPlugin, IModlrPluginMenuOperation, IModlrPluginModelAccessOclType, IModlrPluginImportantEventCallbackHandler
  {
    public string GetName()
    {
      return "AnotherPlugin";
    }

    public List<IModlrPluginMenuOperation> ProvideOperations()
    {
      List<IModlrPluginMenuOperation>  result = new List<IModlrPluginMenuOperation>();
      result.Add(new MyOperation());
      return result;
    }

    public void CheckEnableOrExecute(Eco.ObjectRepresentation.IEcoServiceProvider esp, bool execute, Eco.ObjectRepresentation.IEcoObject maincontext, Eco.ObjectRepresentation.IEcoObject subcontext, out bool enabled)
    {
      enabled = true;
    }

    public void RuntimeModelAccess(IOclTypeService ocl)
    {
      ocl.InstallOperation(new CustomOCL_SplitAtSemi());
    }


    public void ImportantEventAboutToHappen(Eco.ObjectRepresentation.IEcoServiceProvider esp, CallbackEventKind eventkind, ref bool stop, ref string message)
    {
      // Here you can perform checks before codegen, and optionally stop the codegen
      System.Windows.Forms.MessageBox.Show("You are updating the code, this annoying message came from plugin ModlrPluginCodeGenEventsAndCustomOCLOperations");
    }

    public void ImportantEventHasHappend(Eco.ObjectRepresentation.IEcoServiceProvider esp, CallbackEventKind eventkind)
    {
      // Here you can do stuff after codegen
    }
  }

  public class CustomOCL_SplitAtSemi : OclOperationBase
  {
    public CustomOCL_SplitAtSemi()
    { 
    
    }

    protected override void Init()
    {
      InternalInit("SplitAtSemi", new IOclType[] { Support.StringType }, Support.StringType);

    }

    public override void Evaluate(IOclOperationParameters __Params)
    {
      string s = Support.GetAsString(__Params.Values[0]);
      string[] ss=s.Split(';');
      string first = "";
      if (ss.Length > 0)
        first = ss[0];
      Support.MakeNewString(__Params.Result, first);
    }
  }

  public class MyOperation : IModlrPluginMenuOperation
  {

    public void CheckEnableOrExecute(Eco.ObjectRepresentation.IEcoServiceProvider esp, bool execute, Eco.ObjectRepresentation.IEcoObject maincontext, Eco.ObjectRepresentation.IEcoObject subcontext, out bool enabled)
    {
      enabled = true;
      if (execute)
        System.Windows.Forms.MessageBox.Show("The op was executed (1)");
    }

    public string GetName()
    {
      return "MyDemoOperation";
    }
  }

}

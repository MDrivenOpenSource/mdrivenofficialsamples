
### What is this repository for? ###

Samples for MDriven

### How do I get set up? ###

Visit us at CapableObjects.com or MDriven.net to download: 
MDriven Designer - design model driven declarative systems with UML and OCL
MDriven Framework for Visual Studio - mix in c# in your model driven declarative systems 
MDriven Turnkey - execute your model driven declarative systems in the cloud or on prem

### Contribution guidelines ###

You can send us working samples that show something important.
Like if you have spotted a bug - a repeating sample is actually the BEST way to have us fix it FAST

### Who do I talk to? ###

Visit us at CapableObjects.com or MDriven.net

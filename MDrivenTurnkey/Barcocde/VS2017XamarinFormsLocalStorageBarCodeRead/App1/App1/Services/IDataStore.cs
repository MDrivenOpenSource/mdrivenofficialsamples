﻿using ProjectTemplate11;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App1.Services
{
  public interface IDataStore<T>
  {
   
   
    void SetEcoSpace(EcoProject1EcoSpace es);
    EcoProject1EcoSpace GetEcoSpace();
  }
}

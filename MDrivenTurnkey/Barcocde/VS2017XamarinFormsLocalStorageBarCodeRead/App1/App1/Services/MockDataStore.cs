﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



using Xamarin.Forms;
using EcoProject1;
using ProjectTemplate11;
using Eco.Services;

[assembly: Dependency(typeof(App1.Services.MockDataStore))]
namespace App1.Services
{
  public class MockDataStore : IDataStore<ItemClass>
  {

    private EcoProject1EcoSpace _es;
    public void SetEcoSpace(EcoProject1EcoSpace es)
    {
      _es = es;
    }
    public EcoProject1EcoSpace GetEcoSpace()
    {
      return _es;
    }


  }
}

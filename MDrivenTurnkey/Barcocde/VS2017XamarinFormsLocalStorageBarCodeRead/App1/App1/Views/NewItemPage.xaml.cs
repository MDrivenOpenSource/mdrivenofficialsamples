﻿using System;
using Xamarin.Forms;
using EcoProject1;
using App1.ViewModels;
using Eco.Services;

namespace App1.Views
{
  public partial class NewItemPage : ContentPage
  {
    public ItemClass Item { get; set; }
    private bool _RevertOnDisappear = true;
    public NewItemPage():this("New Item")
    {
    }
    public NewItemPage(string initText)
    {
      InitializeComponent();

      Item = new ItemClass(BaseViewModel.DataStore.GetEcoSpace())
      {
        Text = initText,
        Description = "Scanned " + DateTime.Now.ToString("yyyy-MM-dd HH:mm")
      };

      BindingContext = this;
    }

    async void Save_Clicked(object sender, EventArgs e)
    {
      _RevertOnDisappear = false;
      BaseViewModel.DataStore.GetEcoSpace().UpdateDatabase();
      await Navigation.PopToRootAsync();
    }


    protected override void OnDisappearing()
    {
      base.OnDisappearing();
      if (_RevertOnDisappear)
      {
        EcoServiceHelper.GetPersistenceService(Item).DiscardChanges(Item);
      }
    }
   
  }
}
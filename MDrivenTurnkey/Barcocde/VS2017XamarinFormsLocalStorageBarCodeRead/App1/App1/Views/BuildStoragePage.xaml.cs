﻿using System;

using App1.ViewModels;

using Xamarin.Forms;
using EcoProject1;
using PortableWithMDriven.ViewModelCodeGen_DMItemsViewModel;
using App1.Services;
using ZXing.Net.Mobile.Forms;
using PortableWithMDriven.ViewModelCodeGen_BuildStorage;
using Eco.Handles;

namespace App1.Views
{
  public partial class BuildStoragePage : ContentPage
  {
    BuildStorage _viewModel;
    EcoSpace _es;
    public BuildStoragePage()
    {
      InitializeComponent();
      _es = BaseViewModel.DataStore.GetEcoSpace();
      _viewModel = BuildStorage.Create(_es, null);
      _viewModel.VM_Variables.vServerAddress = "demo20170123.azurewebsites.net";
      MDriven.Net.Http.MDrivenRestLogic.Install();
    }

    private void AssignBindindContext()
    {
      Dispatcher.CurrentDispatcher.BeginInvoke(() =>
      {
        BindingContext = _viewModel;
      });

    }

  
    protected override async void OnAppearing()
    {
      base.OnAppearing();
      AssignBindindContext();

    }

    protected override void OnDisappearing()
    {
      base.OnDisappearing();
      BindingContext = null;
    }


    private async void ScanShelf_Clicked(object sender, EventArgs e)
    {

      var scanPage = new ZXingScannerPage();
      scanPage.AutoFocus();
      // Navigate to our scanner page
      await Navigation.PushAsync(scanPage);


      scanPage.OnScanResult += (result) =>
      {
        // Stop scanning
        scanPage.IsScanning = false;

        // Pop the page and show the result
        Device.BeginInvokeOnMainThread(async () =>
        {
          await Navigation.PopAsync();
          var theScanResult = result.Text;
          _viewModel.VM_Variables.vFoundShelf = new FoundShelf(_es) { Barcode= theScanResult };
          int ld;
          _viewModel.EnsureMembersAreUpToDate(out ld);

          //          await Navigation.PushAsync(new NewItemPage(theScanResult));
          //await DisplayAlert("Scanned Barcode", result.Text, "OK");
        });
      };
    }
    private async void ScanItem_Clicked(object sender, EventArgs e)
    {

      var scanPage = new ZXingScannerPage();
      scanPage.AutoFocus();
      // Navigate to our scanner page
      await Navigation.PushAsync(scanPage);


      scanPage.OnScanResult += (result) =>
      {
        // Stop scanning
        scanPage.IsScanning = false;

        // Pop the page and show the result
        Device.BeginInvokeOnMainThread(async () =>
        {
          await Navigation.PopAsync();
          var theScanResult = result.Text;

          if (_viewModel.VM_Variables.vFoundShelf != null)
          {
            _viewModel.VM_Variables.vFoundShelf.FoundShelfableItems.Add(new FoundShelfableItem(_es) { Barcode = theScanResult });
            int ld;
            _viewModel.EnsureMembersAreUpToDate(out ld);
          }

        });
      };
    }

    private async void Sync_Clicked(object sender, EventArgs e)
    {
      try
      {
        _viewModel.ExecuteNamedAction("SyncThis");
        int ld;
        _viewModel.EnsureMembersAreUpToDate(out ld);
      }
      catch (Exception ex)
      {
        //
      }
    }

  }
  }

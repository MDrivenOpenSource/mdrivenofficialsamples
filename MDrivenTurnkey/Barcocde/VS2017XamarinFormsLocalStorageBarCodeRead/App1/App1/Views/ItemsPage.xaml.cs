﻿using System;

using App1.ViewModels;

using Xamarin.Forms;
using EcoProject1;
using PortableWithMDriven.ViewModelCodeGen_DMItemsViewModel;
using App1.Services;
using ZXing.Net.Mobile.Forms;

namespace App1.Views
{
  public partial class ItemsPage : ContentPage
  {
    DMItemsViewModel viewModel;

    public ItemsPage()
    {
      InitializeComponent();
      viewModel = DMItemsViewModel.Create(BaseViewModel.DataStore.GetEcoSpace(), null);
    }

    private void AssignBindindContext()
    {
      Dispatcher.CurrentDispatcher.BeginInvoke(() =>
      {
        BindingContext = viewModel;
      });

    }

    async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
    {
      var item = args.SelectedItem as Item;
      if (item == null)
        return;

      await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(item.Element.AsObject as ItemClass)));

      // Manually deselect item
      ItemsListView.SelectedItem = null;
    }

    async void AddItem_Clicked(object sender, EventArgs e)
    {
      await Navigation.PushAsync(new NewItemPage());
    }

    protected override async void OnAppearing()
    {
      base.OnAppearing();
      AssignBindindContext();

    }

    protected override void OnDisappearing()
    {
      base.OnDisappearing();
      BindingContext = null;
    }


    private async void Scan_Clicked(object sender, EventArgs e)
    {

      var scanPage = new ZXingScannerPage();
      scanPage.AutoFocus();
      // Navigate to our scanner page
      await Navigation.PushAsync(scanPage);


      scanPage.OnScanResult += (result) =>
      {
        // Stop scanning
        scanPage.IsScanning = false;

        // Pop the page and show the result
        Device.BeginInvokeOnMainThread(async () =>
        {
          await Navigation.PopAsync();
          var theScanResult = result.Text;
          await Navigation.PushAsync(new NewItemPage(theScanResult));
          //await DisplayAlert("Scanned Barcode", result.Text, "OK");
        });
      };
    }
  }
}
